//
//  AppDelegate.swift
//  Tamuro
//
//  Created by Mobile on 9/6/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import CoreData
import Fabric
import Crashlytics
import Firebase
import FirebaseAuth
import IQKeyboardManagerSwift
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import OneSignal
import SideMenu
import ProgressHUD
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        IQKeyboardManager.shared.enable = true
        
        FirebaseApp.configure()
        
        Fabric.with([Crashlytics.self])
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        configureOnesignal(launchOptions)
        
        configureRealmShare()
        
        configureRootView()
        
        appearanceChage() 
        
        return true
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication!, annotation: annotation)
    }
    func appearanceChage() {
        UITabBar.appearance().layer.borderWidth = 0
        UITabBar.appearance().clipsToBounds = true
        UITabBar.appearance().backgroundColor = .clear

//        SideMenuManager.default.menuPresentMode = .menuSlideIn
    }
    func configureOnesignal(_ launchOptions :[UIApplication.LaunchOptionsKey: Any]?)
    {
        OneSignal.initWithLaunchOptions(launchOptions,
                                        appId: Constant.AppKey.ONESIGNAL,
                                        handleNotificationReceived: nil,
                                        handleNotificationAction: nil,
                                        settings: [kOSSettingsKeyInAppAlerts:false])
        OneSignal.setLogLevel(ONE_S_LOG_LEVEL.LL_NONE, visualLevel: ONE_S_LOG_LEVEL.LL_NONE)
    }
    
    func configureRealmShare() {
        Users.shared.initialize()
        Recents.shared.initialize()
//        Groups.shared.initialize()
        Trips.shared.initialize()
        TripContents.shared.initialize()
        Connection.shared()
    }
    
    func configureRootView(){
        let storyboard = UIStoryboard.init(name: "Auth", bundle: nil)
        var vc = storyboard.instantiateInitialViewController()
        let currentUser: [String : Any]? = UserDefaults.standard.object(forKey: "CurrentUser") as? [String : Any]
        if let _ = Auth.auth().currentUser {
            if currentUser != nil{
                vc = storyboard.instantiateViewController(withIdentifier: "SplashVC")
            }else{
                let firebaseAuth = Auth.auth()
                do {
                    try firebaseAuth.signOut()
                    UserDefaults.standard.removeObject(forKey: "CurrentUser")
                    UserDefaults.standard.synchronize()
                } catch let signOutError as NSError {
                    print ("Error signing out: %@", signOutError)                    
                }
            }
        }
        window?.rootViewController = vc
        window?.makeKeyAndVisible()
    }

    func applicationWillResignActive(_ application: UIApplication) {

    }

    func applicationDidEnterBackground(_ application: UIApplication) {
 
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
  
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
        OneSignal.idsAvailable { (oneSignal, pushToken) in
            if pushToken != nil{
                UserDefaults.standard.set(oneSignal, forKey: FConst.FUser.ONESIGNALID)
                UserDefaults.standard.synchronize()
            }else{
                UserDefaults.standard.removeObject(forKey:FConst.FUser.ONESIGNALID)
                UserDefaults.standard.synchronize()
            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Tamuro")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}

