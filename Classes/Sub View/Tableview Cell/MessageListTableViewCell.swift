//
//  MessageListTableViewCell.swift
//  Tamuro
//
//  Created by Mobile on 9/15/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class MessageListTableViewCell: UITableViewCell {
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var viewUnread: UIView!
    @IBOutlet weak var viewPromotion: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgUser.setRadius(radius: imgUser.frame.size.width / 2)
        viewUnread.setRadius(radius: viewUnread.frame.size.width / 2)
        viewPromotion.setRadius(radius: viewPromotion.frame.size.width / 2)
        if #available(iOS 11.0, *) {
            viewPromotion.setBorder(width: 4, color: UIColor(named: "color_green_promotion")!)
        } else {
            // Fallback on earlier versions
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(dbtrip : DBTrip) {
        
        lblTitle.text = dbtrip.title
//        lblPrice.text = "$\(dbtrip.cost)"
//        imgTrip.sd_setImage(with: URL(string: dbtrip.picture), placeholderImage: nil, options: .refreshCached)
//        lblDay.text = String(dbtrip.day)
//        lblTripDayNight.text = "\(dbtrip.day)D, " + "\(dbtrip.night)N Trip"
//        
//        let startDate = formatter.date(from: dbtrip.startDate)
//        let startMonth = formatter1.string(from: startDate!)
//        let startDay = formatter2.string(from: startDate!)
//        lblDay.text = startDay
//        lblMonth.text = startMonth
//        
//        var userImage = ""
//        if dbtrip.userId != CurrentUser.shared.objectId{
//            let dbuser: DBUser = Users.getObjectfromId(objectId: dbtrip.userId)!
//            userImage = dbuser.thumbnail
//        }else{
//            userImage = CurrentUser.shared.thumbnail
//        }
//        btnProfile.sd_setImage(with: URL(string: userImage), for: .normal, placeholderImage: nil, options: .refreshCached)
    }
}
