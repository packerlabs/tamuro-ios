//
//  TripListTableViewCell.swift
//  Tamuro
//
//  Created by Mobile on 9/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import SDWebImage

class TripListTableViewCell: UITableViewCell {
    @IBOutlet weak var lblTripDayNight: UILabel!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var imgTrip: UIImageView!
    @IBOutlet weak var btnProfile: UIButton!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    private let formatter = DateFormatter()
    private let formatter1 = DateFormatter()
    private let formatter2 = DateFormatter()
    override func awakeFromNib() {
        super.awakeFromNib()
        formatter.dateFormat = "dd MMMM yyyy"
        formatter1.dateFormat = "MMM"
        formatter2.dateFormat = "dd"
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCell(dbtrip : DBTrip) {
        lblTripDayNight.dropShadowEffect(shadowRadius: 3)
        viewMain.setRadius(radius: 22)
        imgTrip.setRadius(radius: 22)
        btnProfile.setRadius(radius: btnProfile.frame.size.height / 2)
        
        lblTitle.text = dbtrip.title
        lblPrice.text = "$\(dbtrip.cost)"  
        imgTrip.sd_setImage(with: URL(string: dbtrip.picture), placeholderImage: nil, options: .refreshCached)
        lblDay.text = String(dbtrip.day)
        lblTripDayNight.text = "\(dbtrip.day)D, " + "\(dbtrip.night)N Trip"
        
        let startDate = formatter.date(from: dbtrip.startDate)
        let startMonth = formatter1.string(from: startDate!)
        let startDay = formatter2.string(from: startDate!)
        lblDay.text = startDay
        lblMonth.text = startMonth
        
        var userImage = ""
        if dbtrip.userId != CurrentUser.shared.objectId{
            let dbuser: DBUser = Users.getObjectfromId(objectId: dbtrip.userId)!
            userImage = dbuser.thumbnail
        }else{
            userImage = CurrentUser.shared.thumbnail
        }
        btnProfile.sd_setImage(with: URL(string: userImage), for: .normal, placeholderImage: nil, options: .refreshCached)
    }
}
