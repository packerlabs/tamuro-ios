//
//  Push.swift
//  Tamuro
//
//  Created by Mobile on 11/30/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
import OneSignal

class Push: NSObject {

    func sendPushNotification1(message: FObject) {

        let type: String = message.dictionary[FConst.FMessage.TYPE] as! String
        var text: String = message.dictionary[FConst.FMessage.SENDERNAME] as! String
        let groupId: String = message.dictionary[FConst.FMessage.GROUPID] as! String
        if (type == Constant.Chat.MESSAGE_TEXT) {
            text = text + (" sent you a text message.")
        }

        if (type == MESSAGE_EMOJI) {
            text = text + (" sent you an emoji.")
        }
        if (type == MESSAGE_PICTURE) {
            text = text + (" sent you a picture.")
        }
        if (type == MESSAGE_VIDEO) {
            text = text + (" sent you a video.")
        }
        if (type == MESSAGE_AUDIO) {
            text = text + (" sent you an audio.")
        }
        if (type == MESSAGE_LOCATION) {
            text = text + (" sent you a location.")
        }
    
        Recent.fetchMembers(groupId) { userIds in
            if var userids = userIds{
                userids.removeAll(where: { element in element == FUser.currentId() })
                self.sendPushNotification2(userIds: userids, text: text)
            }
        }
    }
    
    func sendPushNotification2(userIds: [String], text: String) {
        var oneSignalIds: [String] = []
        let predicate = NSPredicate(format: "objectId IN %@", userIds)
        let dbusers = DBUser.objects(with: predicate) as! RLMResults<DBUser>
        for index in 0..<Int(dbusers.count) - 1 {
            let dbuser: DBUser = dbusers[UInt(index)]
            if dbuser.oneSignalId.count != 0 {
                oneSignalIds.append(dbuser.oneSignalId)
            }
        }
        
        OneSignal.postNotification(["contents": ["en": text], "include_player_ids": oneSignalIds])
//        [Global pushNotificationWithText:text getUserIds:oneSignalIds];
    }

}
