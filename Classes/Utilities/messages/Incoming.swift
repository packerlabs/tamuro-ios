//
//  Incoming.swift
//  Tamuro
//
//  Created by Mobile on 11/1/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
import JSQMessagesViewController

class Incoming: NSObject {
    
    var senderId = ""
    var senderName = ""
    var date: Date?
    var maskOutgoing = false
    var dbmessage: DBMessage?
    var collectionView: JSQMessagesCollectionView?
    init(_ dbmessage_: DBMessage?, collectionView collectionView_: JSQMessagesCollectionView?) {
        super.init()
        dbmessage = dbmessage_
        collectionView = collectionView_
    }

    func createMessage() -> JSQMessage? {
        senderId = dbmessage?.senderId ?? ""
        senderName = dbmessage?.senderName ?? ""
   
        date = Date.dateWithNumberTimestamp(number: dbmessage?.createdAt ?? 0)
        maskOutgoing = senderId == FUser.currentId()

        if (dbmessage!.type == Constant.Chat.MESSAGE_TEXT) {
            return createTextMessage()
        }
        if (dbmessage!.type == Constant.Chat.MESSAGE_EMOJI) {
            return createEmojiMessage()
        }
        if (dbmessage!.type == Constant.Chat.MESSAGE_PICTURE) {
            return createPictureMessage()
        }
        if (dbmessage!.type == Constant.Chat.MESSAGE_VIDEO) {
            return createVideoMessage()
        }
        if (dbmessage!.type == Constant.Chat.MESSAGE_AUDIO) {
            return createAudioMessage()
        }
        if (dbmessage!.type == Constant.Chat.MESSAGE_LOCATION) {
            return createLocationMessage()
        }

        return nil;
    }
    
    // MARK: - Text message
    
    func createTextMessage() -> JSQMessage? {
        return JSQMessage(senderId: senderId, senderDisplayName: senderName, date: date, text: dbmessage?.text)
    }
    
    // MARK: - Emoji message
    
    func createEmojiMessage() -> JSQMessage? {
        let mediaItem = EmojiMediaItem(text: dbmessage!.text)
        mediaItem!.appliesMediaViewMaskAsOutgoing = maskOutgoing
        
        return JSQMessage(senderId: senderId, senderDisplayName: senderName, date: date, media: mediaItem)
    }

    // MARK: - Picture message
    
    func createPictureMessage() -> JSQMessage? {
        let mediaItem = PhotoMediaItem(image: nil, width: NSNumber(value: dbmessage?.picture_width ?? 0) , height: NSNumber(value: dbmessage?.picture_height ?? 0))
        mediaItem!.appliesMediaViewMaskAsOutgoing = maskOutgoing
        
//        MediaManager.loadPicture(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
        
        return JSQMessage(senderId: senderId, senderDisplayName: senderName, date: date, media: mediaItem)
    }


    // MARK: - Video message

    func createVideoMessage() -> JSQMessage? {
        let mediaItem = VideoMediaItem(maskAsOutgoing: maskOutgoing)
        
        MediaManager.loadVideo(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
        
        return JSQMessage(senderId: senderId, senderDisplayName: senderName, date: date, media: mediaItem)
    }

    // MARK: - Audio message
    
    func createAudioMessage() -> JSQMessage?
    {
        let mediaItem = AudioMediaItem(data:nil)
        mediaItem.appliesMediaViewMaskAsOutgoing = maskOutgoing
        MediaManager.loadAudio(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
        return JSQMessage(senderId: senderId, senderDisplayName: senderName, date: date, media: mediaItem)
    }
    
    // MARK: - Location message
    func createLocationMessage() -> JSQMessage? {
        let mediaItem = JSQLocationMediaItem(location: nil)
        mediaItem?.appliesMediaViewMaskAsOutgoing = maskOutgoing
        
        let location = CLLocation(latitude: CLLocationDegrees(dbmessage!.latitude), longitude: CLLocationDegrees(dbmessage!.longitude))
        mediaItem!.setLocation(location, withCompletionHandler: {
            self.collectionView!.reloadData()
        })
        
        return JSQMessage(senderId: senderId, senderDisplayName: senderName, date: date, media: mediaItem)
    }
}
