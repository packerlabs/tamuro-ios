//
//  MessageSend1.swift
//  Tamuro
//
//  Created by Mobile on 11/5/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import ProgressHUD
class MessageSend1: NSObject {
    var groupId = ""
    var view: UIView?
    
    init(_ groupId_: String, view view_: UIView) {
        super.init()
        groupId = groupId_
        view = view_
    }
    
    func send(_ text: String?, video: URL?, picture: UIImage?, audio: String?)
    {
        let message = FObject.object(path: FConst.FMessage.PATH, subpath: groupId)
        message.dictionary[FConst.FMessage.GROUPID] = groupId
        message.dictionary[FConst.FMessage.SENDERID] = FUser.currentId()
        message.dictionary[FConst.FMessage.SENDERNAME] = CurrentUser.shared.fullname
        message.dictionary[FConst.FMessage.SENDERINITIALS] = FUser.initials()
        
        message.dictionary[FConst.FMessage.PICTURE] = ""
        message.dictionary[FConst.FMessage.PICTURE_WIDTH] = 0
        message.dictionary[FConst.FMessage.PICTURE_HEIGHT] = 0
        message.dictionary[FConst.FMessage.PICTURE_MD5] = ""
        
        message.dictionary[FConst.FMessage.VIDEO] = ""
        message.dictionary[FConst.FMessage.VIDEO_DURATION] = 0
        message.dictionary[FConst.FMessage.VIDEO_MD5] = ""
        
        message.dictionary[FConst.FMessage.AUDIO] = ""
        message.dictionary[FConst.FMessage.AUDIO_DURATION] = 0
        message.dictionary[FConst.FMessage.AUDIO_MD5] = ""
        
        message.dictionary[FConst.FMessage.LATITUDE] = 0
        message.dictionary[FConst.FMessage.LONGITUDE] = 0
        
        message.dictionary[FConst.FMessage.STATUS] = Constant.Chat.TEXT_SENT
        message.dictionary[FConst.FMessage.ISDELETED] = false
        
        if text != nil {
            sendTextMessage(message, text: text!)
        }
        if picture != nil {
            sendPictureMessage(message, picture: picture!)
        }
        if video != nil {
            sendVideoMessage(message, video: video!)
        }
        if audio != nil {
            sendAudioMessage(message, audio: audio!)
        }
        if (text == nil) && (picture == nil) && (video == nil) && (audio == nil) {
            sendLoactionMessage(message)
        }
    }

    func sendTextMessage(_ message: FObject, text: String) {
        message.dictionary[FConst.FMessage.TEXT] = text
        message.dictionary[FConst.FMessage.TYPE] = Emoji.isEmoji(text) ? Constant.Chat.MESSAGE_EMOJI : Constant.Chat.MESSAGE_TEXT
        sendMessage(message)
    }

    func sendPictureMessage(_ message: FObject, picture: UIImage) {
//        NSData *dataPicture = UIImageJPEGRepresentation(picture, 0.6);
//        NSData *cryptedPicture = [Cryptor encryptData:dataPicture groupId:groupId];
//        NSString *md5Picture = [Checksum md5HashOfData:cryptedPicture];
//
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//        hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//
//        FIRStorage *storage = [FIRStorage storage];
//        FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(@"message_image", @"jpg")];
//        FIRStorageUploadTask *task = [reference putData:cryptedPicture metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
//            {
//            [hud hideAnimated:YES];
//            [task removeAllObservers];
//            if (error == nil)
//            {
//            NSString *link = metadata.downloadURL.absoluteString;
//            NSString *file = [DownloadManager fileImage:link];
//            [dataPicture writeToFile:[Dir document:file] atomically:NO];
//
//            message[FMESSAGE_PICTURE] = link;
//            message[FMESSAGE_PICTURE_WIDTH] = @((NSInteger) picture.size.width);
//            message[FMESSAGE_PICTURE_HEIGHT] = @((NSInteger) picture.size.height);
//            message[FMESSAGE_PICTURE_MD5] = md5Picture;
//            message[FMESSAGE_TEXT] = @"[Picture message]";
//            message[FMESSAGE_TYPE] = MESSAGE_PICTURE;
//            [self sendMessage:message];
//            }
//            else [ProgressHUD showError:@"Message sending failed."];
//            }];
//
//        [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
//            {
//            hud.progress = (float) snapshot.progress.completedUnitCount / (float) snapshot.progress.totalUnitCount;
//            }];
    }
    func sendVideoMessage(_ message: FObject, video: URL) {
//        NSNumber *duration = [Video duration:video.path];
//
//        NSData *dataVideo = [NSData dataWithContentsOfFile:video.path];
//        NSData *cryptedVideo = [Cryptor encryptData:dataVideo groupId:groupId];
//        NSString *md5Video = [Checksum md5HashOfData:cryptedVideo];
//
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//        hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//
//        FIRStorage *storage = [FIRStorage storage];
//        FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(@"message_video", @"mp4")];
//        FIRStorageUploadTask *task = [reference putData:cryptedVideo metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
//            {
//            [hud hideAnimated:YES];
//            [task removeAllObservers];
//            if (error == nil)
//            {
//            NSString *link = metadata.downloadURL.absoluteString;
//            NSString *file = [DownloadManager fileVideo:link];
//            [dataVideo writeToFile:[Dir document:file] atomically:NO];
//
//            message[FMESSAGE_VIDEO] = link;
//            message[FMESSAGE_VIDEO_DURATION] = duration;
//            message[FMESSAGE_VIDEO_MD5] = md5Video;
//            message[FMESSAGE_TEXT] = @"[Video message]";
//            message[FMESSAGE_TYPE] = MESSAGE_VIDEO;
//            [self sendMessage:message];
//            }
//            else [ProgressHUD showError:@"Message sending failed."];
//            }];
//
//        [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
//            {
//            hud.progress = (float) snapshot.progress.completedUnitCount / (float) snapshot.progress.totalUnitCount;
//            }];
    }
    
    func sendAudioMessage(_ message: FObject, audio: String) {
//        NSNumber *duration = [Audio duration:audio];
//
//        NSData *dataAudio = [NSData dataWithContentsOfFile:audio];
//        NSData *cryptedAudio = [Cryptor encryptData:dataAudio groupId:groupId];
//        NSString *md5Audio = [Checksum md5HashOfData:cryptedAudio];
//
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:view animated:YES];
//        hud.mode = MBProgressHUDModeDeterminateHorizontalBar;
//
//        FIRStorage *storage = [FIRStorage storage];
//        FIRStorageReference *reference = [[storage referenceForURL:FIREBASE_STORAGE] child:Filename(@"message_audio", @"m4a")];
//        FIRStorageUploadTask *task = [reference putData:cryptedAudio metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error)
//            {
//            [hud hideAnimated:YES];
//            [task removeAllObservers];
//            if (error == nil)
//            {
//            NSString *link = metadata.downloadURL.absoluteString;
//            NSString *file = [DownloadManager fileAudio:link];
//            [dataAudio writeToFile:[Dir document:file] atomically:NO];
//
//            message[FMESSAGE_AUDIO] = link;
//            message[FMESSAGE_AUDIO_DURATION] = duration;
//            message[FMESSAGE_AUDIO_MD5] = md5Audio;
//            message[FMESSAGE_TEXT] = @"[Audio message]";
//            message[FMESSAGE_TYPE] = MESSAGE_AUDIO;
//            [self sendMessage:message];
//            }
//            else [ProgressHUD showError:@"Message sending failed."];
//            }];
//
//        [task observeStatus:FIRStorageTaskStatusProgress handler:^(FIRStorageTaskSnapshot *snapshot)
//            {
//            hud.progress = (float) snapshot.progress.completedUnitCount / (float) snapshot.progress.totalUnitCount;
//            }];
    }

    func sendLoactionMessage(_ message: FObject) {
        message.dictionary[FConst.FMessage.LATITUDE] = Location.latitude()
        message.dictionary[FConst.FMessage.LONGITUDE] = Location.longitude()
        message.dictionary[FConst.FMessage.TEXT] = "[Location message]"
        message.dictionary[FConst.FMessage.TYPE] = Constant.Chat.MESSAGE_LOCATION
        sendMessage(message)
    }

    func sendMessage(_ message: FObject)
    {
        message.dictionary[FConst.FMessage.TEXT] = Cryptor.encryptText((message.dictionary[FConst.FMessage.TEXT] as! String), groupId: groupId)
        message.saveInBackground { (error) in
            if error == nil {
                Recent.updateLastMessage(message)
                Push().sendPushNotification1(message: message)
//                SendPushNotification1(message)
            } else {
                ProgressHUD.showError("Message sending failed.")
            }
        }
    }

}
