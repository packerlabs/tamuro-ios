
#pragma mark - Device Params
#define		SYSTEM_VERSION								[[UIDevice currentDevice] systemVersion]
#define		SYSTEM_VERSION_EQUAL_TO(v)					([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedSame)
#define		SYSTEM_VERSION_GREATER_THAN(v)				([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedDescending)
#define		SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)	([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedAscending)
#define		SYSTEM_VERSION_LESS_THAN(v)					([SYSTEM_VERSION compare:v options:NSNumericSearch] == NSOrderedAscending)
#define		SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)		([SYSTEM_VERSION compare:v options:NSNumericSearch] != NSOrderedDescending)

#define     IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define     Fontcell (IS_IPAD ? 70 : 45)

#pragma mark - COLORs
#define        HEXCOLOR(c) [UIColor colorWithRed:((c>>24)&0xFF)/255.0 green:((c>>16)&0xFF)/255.0 blue:((c>>8)&0xFF)/255.0 alpha:((c)&0xFF)/255.0]
#define COLOR_HOME_BG        [UIColor colorWithRed:(235.0/255.0) green:(235.0/255) blue:(235.0/255) alpha:1.0]
#define COLOR_NAV_BLUE       [UIColor colorWithRed:(0.0/255.0) green:(120.0/255) blue:(255.0/255) alpha:1.0]
#define COLOR_NAV_WHITE      [UIColor colorWithRed:(255.0/255.0) green:(255.0/255) blue:(255.0/255) alpha:1.0]
#define COLOR_SLIDER_BG      [UIColor colorWithRed:(41.0/255.0) green:(41.0/255) blue:(41.0/255) alpha:1.0]
#define COLOR_BTN_BACKGROUND [UIColor colorWithRed:126/225.0 green:201/255.0 blue:55/255.0 alpha:1.0]

#define COLOR_NAVIGATIONBAR  HEXCOLOR(0xF5605CFF)
#define COLOR_OUTGOING       HEXCOLOR(0xFFFFFFFF)
#define COLOR_INCOMING       HEXCOLOR(0x1476D1FF)


#pragma mark - App Constants
//---------------------------------------------------------------------------------
#define		DEFAULT_TAB							0
#define		VIDEO_LENGTH						5
#define		AUDIO_LENGTH						5
#define		INSERT_MESSAGES						10
#define		PASSWORD_LENGTH						20
#define		DOWNLOAD_TIMEOUT					300
//---------------------------------------------------------------------------------
#define		STATUS_LOADING						1
#define		STATUS_SUCCEED						2
#define		STATUS_MANUAL						3
//---------------------------------------------------------------------------------
#define		MEDIA_IMAGE							1
#define		MEDIA_VIDEO							2
#define		MEDIA_AUDIO							3
//---------------------------------------------------------------------------------
#define		NETWORK_MANUAL						1
#define		NETWORK_WIFI						2
#define		NETWORK_ALL							3
//---------------------------------------------------------------------------------
#define		KEEPMEDIA_WEEK						1
#define		KEEPMEDIA_MONTH						2
#define		KEEPMEDIA_FOREVER					3
//---------------------------------------------------------------------------------
#define		DEL_ACCOUNT_NONE					1
#define		DEL_ACCOUNT_ONE						2
#define		DEL_ACCOUNT_ALL						3
//---------------------------------------------------------------------------------
#define     MESSAGE_TEXT                        @"text"
#define     MESSAGE_EMOJI                       @"emoji"
#define     MESSAGE_PICTURE                     @"picture"
#define     MESSAGE_VIDEO                       @"video"
#define     MESSAGE_AUDIO                       @"audio"
#define     MESSAGE_LOCATION                    @"location"
//---------------------------------------------------------------------------------

#define		LOGIN_EMAIL							@"Email"
#define		LOGIN_FACEBOOK						@"Facebook"
#define		LOGIN_GOOGLE						@"Google"
//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
#define		TEXT_QUEUED							@"Queued"
#define		TEXT_SENT							@"Sent"
#define		TEXT_READ							@"Read"
//---------------------------------------------------------------------------------
#define		PHOTOS_ALBUM_TITLE					@"Chat"
//---------------------------------------------------------------------------------
#define		LINK_PREMIUM						@"http://www.relatedcode.com/premium"
//---------------------------------------------------------------------------------
#define		SCREEN_WIDTH						[UIScreen mainScreen].bounds.size.width
#define		SCREEN_HEIGHT						[UIScreen mainScreen].bounds.size.height
//---------------------------------------------------------------------------------
#define		TEXT_INVITE_SMS						@"Check out PremiumChat for your smartphone. Download it today."
//---------------------------------------------------------------------------------

#define     ONESIGNALID                         @"OneSignalId"
#define     USER_ACCOUNTS                       @"UserAccounts"
#define     REACHABILITY_CHANGED                @"ReachabilityChanged"
//---------------------------------------------------------------------------------

#pragma mark - App Keys

#pragma mark - App Keys
#define     DEFAULT_ADDRESS                     @"SAN FRANCISCO CALIFONIA"
#define     DEFAULT_LATITUDE1                   31.949725
#define     DEFAULT_LONGITUDE1                  -83.112558
#define     DEFAULT_LATITUDE2                   47.626908
#define     DEFAULT_LONGITUDE2                  -123.743622
#define     DEFAULT_PASSWORD                    @"sec12345"
#define     MARKER_ID                           @"markerId"

// dispatch_async(dispatch_queue_create(nil, DISPATCH_QUEUE_SERIAL), ^{
//
// });

//dispatch_async(dispatch_get_main_queue(), ^{
//
//});

//dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//    dispatch_async(dispatch_get_main_queue(), ^{
//    });
//});

//dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC);
//dispatch_after(time, dispatch_get_main_queue(), ^(void){
//
//});

//static dispatch_once_t once;
//dispatch_once(&once, ^{
//});

//[self performSelector:@selector(delayedReload) withObject:nil afterDelay:0.25];
