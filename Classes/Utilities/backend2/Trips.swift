//
//  Users.swift
//  WeddingProject
//
//  Created by Mobile on 8/31/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Realm
class Trips: NSObject {
    
    static let shared = Trips()
    
    var timer = Timer()
    var refreshInterface : Bool = false
    var firebase : DatabaseReference? = nil
    
    func initialize()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.APP_STARTED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_IN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionCleanup), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_OUT), object: nil)
        timer = Timer(timeInterval: 0.25, target: self, selector: #selector(refreshUserInterface), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
    
    // MARK: - Backend methods
    
    @objc func initObservers()
    {
        if (FUser.currentId() != nil)
        {
            if (firebase == nil) {
                self.createObservers()
            }
        }
    }
    
    func createObservers()
    {
        let lastUpdatedAt : Int64 = DBTrip.lastUpdatedAt();
        firebase = Database.database().reference().child(FConst.FTrip.PATH)
        let query : DatabaseQuery = firebase!.queryOrdered(byChild: FConst.FTrip.UPDATEDAT).queryStarting(atValue: lastUpdatedAt + 1)
        query.observe(.childAdded) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
        }
        
        query.observe(.childChanged) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
        }
    }
    
    func updateRealm(dic: [String:Any])
    {
        var temp = dic
        if dic[FConst.FTrip.MEMBERS] != nil {
            temp[FConst.FTrip.MEMBERS] = (dic[FConst.FTrip.MEMBERS] as! Array).joined(separator: ",")
        }
        if dic[FConst.FTrip.TRIPCONTENTS] != nil {
            temp[FConst.FTrip.TRIPCONTENTS] = (dic[FConst.FTrip.TRIPCONTENTS] as! Array).joined(separator: ",")
        }
        if dic[FConst.FTrip.FOLLOWERS] != nil {
            temp[FConst.FTrip.FOLLOWERS] = (dic[FConst.FTrip.FOLLOWERS] as! Array).joined(separator: ",")
        }
        
        let realm = RLMRealm.default()
        realm.beginWriteTransaction()
        DBTrip.createOrUpdate(in: realm, withValue: temp)
        do{
            try realm.commitWriteTransaction()
        }catch let error as NSError{
            print(error.description)
        }
    }
    // MARK: - Cleanup methods
    
    
    @objc func actionCleanup()
    {
        firebase?.removeAllObservers()
        firebase = nil
    }
    
    // MARK : - Notification methods
    
    @objc func refreshUserInterface()
    {
        if (refreshInterface)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_TRIPS), object: nil)
            refreshInterface = false;
        }
    }
}
