//
//  Users.swift
//  WeddingProject
//
//  Created by Mobile on 8/31/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Realm
class Users: NSObject {
    
    static let shared = Users()
    
    var timer = Timer()
    var refreshInterface : Bool = false
    var firebase : DatabaseReference? = nil
    
    func initialize()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.APP_STARTED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_IN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionCleanup), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_OUT), object: nil)
        timer = Timer(timeInterval: 0.25, target: self, selector: #selector(refreshUserInterface), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
    
    // MARK: - Backend methods
    
    @objc func initObservers()
    {
        if (FUser.currentId() != nil)
        {
            if (firebase == nil) {
                self.createObservers()
            }
        }
    }
    func createObservers()
    {
        let lastUpdatedAt : Int64 = DBUser.lastUpdatedAt();
        firebase = Database.database().reference().child(FConst.FUser.PATH)
        let query : DatabaseQuery = firebase!.queryOrdered(byChild: FConst.FUser.UPDATEDAT).queryStarting(atValue: lastUpdatedAt + 1)
        query.observe(.childAdded) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
            
        }
        query.observe(.childChanged) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
        }
    }
    
    func updateRealm(dic: [String:Any])
    {
        if dic[FConst.FUser.OBJECTID] as! String == CurrentUser.shared.objectId {
            return
        }
        let realm = RLMRealm.default()
        realm.beginWriteTransaction()
        DBUser.createOrUpdate(in: realm, withValue: dic)
        do{
            try realm.commitWriteTransaction()
        }catch let error as NSError{
            print(error.description)
        }        
    }
    // MARK: - Cleanup methods
    
    
    @objc func actionCleanup()
    {
        firebase!.removeAllObservers()
        firebase = nil
    }
    
    // MARK : - Notification methods
    
    @objc func refreshUserInterface()
    {
        if (refreshInterface)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_USERS), object: nil)
            refreshInterface = false;
        }
    }
    static func getObjectfromId(objectId:String) ->DBUser?{
        let predicate = NSPredicate(format: "objectId == %@", objectId)
        let dbuser = DBUser.objects(with: predicate).firstObject() as? DBUser
        return dbuser
    }
}
