//
//  Groups.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Realm
class Groups: NSObject {
    static let shared = Groups()
    
    var timer = Timer()
    var refreshInterface : Bool = false
    var firebase : DatabaseReference? = nil
    
    func initialize()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.APP_STARTED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_IN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionCleanup), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_OUT), object: nil)
        timer = Timer(timeInterval: 0.25, target: self, selector: #selector(refreshUserInterface), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
    
    // MARK: - Backend methods
    
    @objc func initObservers()
    {
        if (FUser.currentId() != nil)
        {
            if (firebase == nil) {
                self.createObservers()
            }
        }
    }
    func createObservers()
    {
        let lastUpdatedAt : Int64 = DBGroup.lastUpdatedAt();
        firebase = Database.database().reference().child(FConst.FGroup.PATH)
        let query : DatabaseQuery = firebase!.queryOrdered(byChild: FConst.FGroup.UPDATEDAT).queryStarting(atValue: lastUpdatedAt + 1)
        query.observe(.childAdded) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
            
        }
        query.observe(.childChanged) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
        }
    }
    
    func updateRealm(dic: [String:Any])
    {
        var temp = dic
        if dic[FConst.FGroup.MEMBERS] != nil {
            temp[FConst.FGroup.MEMBERS] = (dic[FConst.FGroup.MEMBERS] as! Array).joined(separator: ",")
        }
        let realm = RLMRealm.default()
        realm.beginWriteTransaction()
        DBGroup.createOrUpdate(in: realm, withValue: dic)
        do{
            try realm.commitWriteTransaction()
        }catch let error as NSError{
            print(error.description)
        }
    }
    
    // MARK: - Cleanup methods
    
    @objc func actionCleanup()
    {
        firebase!.removeAllObservers()
        firebase = nil
    }
    
    // MARK : - Notification methods
    
    @objc func refreshUserInterface()
    {
        if (refreshInterface)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_GROUPS), object: nil)
            refreshInterface = false;
        }
    }
    static func getObjectfromId(objectId:String) ->DBGroup?{
        let predicate = NSPredicate(format: "objectId == %@", objectId)
        let dbgroup = DBGroup.objects(with: predicate).firstObject() as? DBGroup
        return dbgroup
    }
}
