//
//  Messages.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import UIKit
import FirebaseDatabase
import Realm
import JSQMessagesViewController
class Messages: NSObject { 
    
    var groupId = ""
    var timer = Timer()
    var refreshUserInterface1 = false
    var refreshUserInterface2 = false
    var playMessageReceivedSound = false
    var firebase : DatabaseReference? = nil
    
    init(_ groupId_: String) {
        super.init()
        groupId = groupId_
        timer = Timer.scheduledTimer(timeInterval: 0.25, target: self, selector: #selector(self.refreshUserInterface), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
        createObservers()
    }
    
    // MARK: - Backend methods

    func createObservers()
    {
        let lastUpdatedAt: Int64 = DBMessage.lastUpdatedAt()
        firebase = Database.database().reference().child(FConst.FMessage.PATH).child(groupId)
        firebase?.observe(.childAdded) { (snapshot) in
            var message = snapshot.value as! [String:Any]
            if (message[FConst.FMessage.UPDATEDAT] as! Int64) > lastUpdatedAt {
                self.updateIncoming(message)
                DispatchQueue(label: "queuename").async(execute: {
                    self.updateRealm(dic: message)
                    self.refreshUserInterface1 = true
                })
            }
        }
        firebase?.observe(.childChanged) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshUserInterface2 = true
            }
        }
    }
    
    func updateRealm(dic: [String:Any])
    {
        var temp = dic
        if dic[FConst.FMessage.GROUPID] != nil {
            temp[FConst.FMessage.TEXT] = Cryptor.decryptText(dic[FConst.FMessage.TEXT] as? String, groupId: dic[FConst.FMessage.GROUPID] as? String)

        }
        let realm = RLMRealm.default()
        realm.beginWriteTransaction()
        DBMessage.createOrUpdate(in: realm, withValue: temp)
        do{
            try realm.commitWriteTransaction()
        }catch let error as NSError{
            print(error.description)
        }
    }
    
    func updateIncoming(_ message: [String : Any]) {
        if ((message[FConst.FMessage.SENDERID] as! String) == FUser.currentId()) == false {
            if ((message[FConst.FMessage.STATUS] as! String) == Constant.Chat.TEXT_SENT) {
                let messageId = message[FConst.FMessage.OBJECTID] as! String
                Message.updateStatus(groupId, messageId: messageId)
                playMessageReceivedSound = true
            }
        }
    }

    // MARK: - Cleanup methods    
    
    @objc func actionCleanup()
    {
        firebase!.removeAllObservers()
        firebase = nil
    }
    
    // MARK : - Notification methods
    @objc func refreshUserInterface() {
        if refreshUserInterface1 {
            DispatchQueue.main.async(execute: {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_MESSAGES1), object: nil)
                self.refreshUserInterface1 = false
            })
        }

        if refreshUserInterface2 {
            DispatchQueue.main.async(execute: {
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_MESSAGES2), object: nil)
                self.refreshUserInterface2 = false
            })
        }
        if playMessageReceivedSound {
            JSQSystemSoundPlayer.jsq_playMessageReceivedSound()
            playMessageReceivedSound = false
        }
    }

}
