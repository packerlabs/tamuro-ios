//
//  Recents.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import UIKit
import FirebaseDatabase
import Realm
class Recents: NSObject {
    
    static let shared = Recents()
    
    var timer = Timer()
    var refreshInterface : Bool = false
    var firebase : DatabaseReference? = nil
    
    func initialize()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.APP_STARTED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.initObservers), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_IN), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionCleanup), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_OUT), object: nil)
        
        timer = Timer(timeInterval: 0.25, target: self, selector: #selector(refreshUserInterface), userInfo: nil, repeats: true)
        RunLoop.main.add(timer, forMode: RunLoop.Mode.common)
    }
    
    // MARK: - Backend methods
    
    @objc func initObservers()
    {
        if (FUser.currentId() != nil)
        {
            if (firebase == nil) {
                self.createObservers()
            }
        }
    }
    func createObservers()
    {
        firebase = Database.database().reference().child(FConst.FRecent.PATH)
        let query: DatabaseQuery = firebase!.queryOrdered(byChild: FConst.FRecent.USERID).queryEqual(toValue: FUser.currentId())
        query.observe(.childAdded) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            Password.set(dic[FConst.FRecent.PASSWORD] as? String, groupId: dic[FConst.FRecent.GROUPID] as? String)
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
            
        }
        query.observe(.childChanged) { (snapshot) in
            let dic : [String:Any] = snapshot.value as! [String:Any]
            let serialQueue = DispatchQueue(label: "queuename")
            serialQueue.sync {
                self.updateRealm(dic: dic)
                self.refreshInterface = true
            }
        }
    }
    
    func updateRealm(dic: [String:Any])
    {
        var temp = dic
        if dic[FConst.FRecent.MEMBERS] != nil {
            temp[FConst.FRecent.MEMBERS] = (dic[FConst.FRecent.MEMBERS] as! Array).joined(separator: ",")
        }
        
        if dic[FConst.FRecent.GROUPID] != nil {
            temp[FConst.FRecent.LASTMESSAGE] = Cryptor.decryptText(dic[FConst.FRecent.LASTMESSAGE] as? String, groupId: dic[FConst.FRecent.GROUPID] as? String)
        }
        
        let realm = RLMRealm.default()
        realm.beginWriteTransaction()
        DBRecent.createOrUpdate(in: realm, withValue: temp)
        do{
            try realm.commitWriteTransaction()
        }catch let error as NSError{
            print(error.description)
        }
    }
    // MARK: - Cleanup methods
    
    
    @objc func actionCleanup()
    {
        firebase!.removeAllObservers()
        firebase = nil
    }
    
    // MARK : - Notification methods
    
    @objc func refreshUserInterface()
    {
        if (refreshInterface)
        {
            NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_RECENTS), object: nil)
            refreshInterface = false;
        }
    }
    static func getObjectfromId(objectId:String) ->DBUser?{
        let predicate = NSPredicate(format: "objectId == %@", objectId)
        let dbuser = DBUser.objects(with: predicate).firstObject() as? DBUser
        return dbuser
    }
}
