
#import "Location.h"
@implementation Location

+ (Location *)shared

{
	static dispatch_once_t once;
	static Location *location;
    
	
	dispatch_once(&once, ^{ location = [[Location alloc] init]; });
	
	return location;
}


+ (void)start

{
	[[self shared].locationManager startUpdatingLocation];
}


+ (void)stop

{
	[[self shared].locationManager stopUpdatingLocation];
}


+ (CLLocationDegrees)latitude

{
	return [self shared].coordinate.latitude;
}


+ (CLLocationDegrees)longitude

{
	return [self shared].coordinate.longitude;
}
//+ (NSString*)address{
//    return GLOBALINS.meAddress;
//}


#pragma mark - Instance methods


- (id)init

{
	self = [super init];
	
	self.locationManager = [[CLLocationManager alloc] init];
	[self.locationManager setDelegate:self];
	[self.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
	[self.locationManager requestWhenInUseAuthorization];
	return self;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    self.coordinate = locations[0].coordinate;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	
}


@end

