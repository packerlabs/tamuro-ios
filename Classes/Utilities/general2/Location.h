

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Location : NSObject <CLLocationManagerDelegate>


@property (strong, nonatomic) CLLocationManager *locationManager;

@property (nonatomic) CLLocationCoordinate2D coordinate;
+ (Location *)shared;

+ (void)start;
+ (void)stop;

+ (CLLocationDegrees)latitude;
+ (CLLocationDegrees)longitude;

@end

