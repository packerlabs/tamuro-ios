

#import <Foundation/Foundation.h>
#import <Reachability/Reachability.h>

@interface Connection : NSObject


@property (strong, nonatomic) Reachability *reachability;

+ (Connection *)shared;

+ (BOOL)isReachable;
+ (BOOL)isReachableViaWWAN;
+ (BOOL)isReachableViaWiFi;

@end

