//
//  CurrentUser.swift
//  WeddingProject
//
//  Created by Mobile on 8/28/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import FirebaseAuth
import Realm

class CurrentUser: NSObject {
    static let shared = CurrentUser()
    var objectId = ""
    var email = ""
    var onesignalId = ""
    var phone = ""
    var fullname = ""
    var firstname = ""
    var lastname = ""
    var picture = ""
    var thumbnail = ""
    var registerModel : [String:Any]?
    var userImage : Any?
    var savedtrips = [String]()
    var trips = [String]()
    override init()
    {
        super.init()
        NotificationCenter.default.addObserver(self, selector: #selector(self.logout), name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_OUT), object: nil)
    }

    static func saveUserInfoWithKey(key: String, value:Any){
        let user = FUser.currentUser()
        user?.dictionary[key] = value
        user?.saveInBackground()        
    }

    static func saveUserInfoWithKey(key:String,value : Any, block:((Error?) ->())?)
    {
        let user = FUser.currentUser()
        user?.dictionary[key] = value
        user?.saveInBackground(block: { (error) in
            block!(error)
        })
    }
    func getUserStatus(dic:[String:Any])
    {
        self.objectId = dic[FConst.FUser.OBJECTID] as! String
        self.email = dic[FConst.FUser.EMAIL] as! String
        self.onesignalId = dic[FConst.FUser.ONESIGNALID] as! String
        self.phone = dic[FConst.FUser.PHONE] as! String
        self.fullname = dic[FConst.FUser.FULLNAME] as! String
        self.firstname = dic[FConst.FUser.FIRSTNAME] as! String
        self.lastname = dic[FConst.FUser.LASTNAME] as! String
        self.picture = dic[FConst.FUser.PICTURE] as! String
        self.thumbnail = dic[FConst.FUser.THUMBNAIL] as! String
        self.savedtrips = dic[FConst.FUser.SAVEDTRIPS] as? Array ?? []
        self.trips = dic[FConst.FUser.TRIPS] as? Array ?? []
    }

    @objc func logout()
    {
        UserDefaults.standard.removeObject(forKey: "CurrentUser")
        UserDefaults.standard.synchronize()
        
        self.clearRealmCashe()
    }
    func clearRealmCashe(){
        let realm = RLMRealm.default()
        realm.beginWriteTransaction()
        realm.deleteAllObjects()
        do {
            try realm.commitWriteTransaction()
        }catch let error as NSError{
            print ("Error realm.commitWrite: %@", error)
        }
    }

}
