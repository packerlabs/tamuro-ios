//
//  FObject.swift
//  WeddingProject
//
//  Created by Mobile on 8/28/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth
class FObject: NSObject {
    
    var path = ""
    var subpath: String?
    var dictionary : [String : Any] = [:]
    
    //MARK: - Class methods
    static func object(path : String) ->FObject
    {
        return FObject(path: path)
    }
    
    static func object(path : String, dictionary:[String:Any]) ->FObject
    {
        return FObject(path: path,dictionary: dictionary)
    }

    static func object(path:String, subpath:String) ->FObject
    {
        return FObject(path: path,subpath: subpath)
    }

    static func object(path: String, subpath :String, dictionary:[String:Any]) -> FObject
    {
        return FObject(path: path,subpath: subpath,dictionary: dictionary)
    }

    // MARK: - Instance methods

    init(path: String){
        super.init()
        self.path = path
        self.subpath = nil
        self.dictionary = [:]
    }

    init(path: String, dictionary: [String : Any]){
        super.init()
        self.path = path
        self.subpath = nil
        self.dictionary = dictionary
    }

    init(path : String, subpath : String){
        super.init()
        self.path = path;
        self.subpath = subpath;
        self.dictionary = [:];
        
    }

    init(path: String,subpath: String,dictionary: [String : Any]){
        super.init()
        self.path = path;
        self.subpath = subpath;
        for (key, obj) in dictionary {
            self.dictionary[key] = obj
        }
    }

    func objectId() -> String
    {
        return dictionary["objectId"] as! String
    }


    func objectIdInit() -> String
    {
        if (dictionary["objectId"] == nil)
        {
            let reference = self.databaseReference()
            dictionary["objectId"] = reference.key
        }
        return dictionary["objectId"] as! String
    }

    //MARK:- Save methods
    
    func saveInBackground()
    {
        self.saveInBackground(block:nil)
    }

    func saveInBackground(block:((Error?)->())?)
    {
        let reference = self.databaseReference()
        if (dictionary["objectId"] == nil){
            dictionary["objectId"] = reference.key
        }

        if (dictionary["createdAt"] == nil){
            dictionary["createdAt"] = ServerValue.timestamp()
        }

        dictionary["updatedAt"] = ServerValue.timestamp()

        if let block = block
        {
            reference.updateChildValues(dictionary) { (error, ref) in
                block(error)
            }
        }else{
            reference.updateChildValues(dictionary)
        }
    }

    //MARK:- Update methods
    
    func updateInBackground()
    {
        self.updateInBackground(block:nil)
    }

    func updateInBackground(block:((Error?) ->())?)
    {
        let reference = self.databaseReference()
        if (dictionary["objectId"] != nil)
        {
            dictionary["updatedAt"] = ServerValue.timestamp()
            if let block = block
            {
                reference.updateChildValues(dictionary) { (error, ref) in
                    if let error = error{
                        block(error)
                    }
                }
            }else {
                reference.updateChildValues(dictionary)
            }
        }else if let block = block{
            block(NSError(domain:"Object cannot be updated.", code:101, userInfo:nil))
        }
    }

    //MARK:- Delete methods
    
    func deleteInBackground()
    {
        self.deleteInBackground(block: nil)
    }

    func deleteInBackground(block: ((Error?) -> ())?)
    {
        let reference = self.databaseReference()
        if (dictionary["objectId"] != nil)
        {
            if let myblock = block
            {
                reference.removeValue { (error, ref) in
                    if let error = error{
                        myblock(error)
                    }
                }
            }else{
                reference.removeValue()
            }
        }else if let myBlock = block{
            myBlock(NSError(domain:"Object cannot be deleted.", code:102, userInfo:nil))
        }
    }

    //MARK:- Fetch methods

    func fetchInBackground()
    {
        self.fetchInBackground(block: nil)
    }

    func fetchInBackground(block: ((Error?) -> ())? )  {
        let reference = self.databaseReference()
        reference.observeSingleEvent(of: DataEventType.value) { (snapshot:DataSnapshot) in
            if(snapshot.exists()){
                self.dictionary = snapshot.value as! [String:Any]
                block!(nil)
            }else if let myBlock = block{
                myBlock(NSError(domain:"Object not found.", code:103, userInfo:nil))
            }
        }
    }

    //MARK: - Private methods
    func databaseReference() -> DatabaseReference
    {
        var reference = DatabaseReference()
        if (subpath == nil){
            reference = Database.database().reference(withPath: path)
        }else {
            reference = Database.database().reference(withPath: path).child(subpath!)
        }
        
        if (dictionary.count<1 && Auth.auth().currentUser?.uid != nil) {
            return reference.child((Auth.auth().currentUser?.uid)!)//[reference child:[FIRAuth auth].currentUser.uid];
        }
        if (dictionary["objectId"] == nil){
            return reference.childByAutoId();
        }else {
            return reference.child(dictionary["objectId"] as! String);
        }
    }

}

