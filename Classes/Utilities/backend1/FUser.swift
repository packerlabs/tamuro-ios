//
//  FUser.swift
//  WeddingProject
//
//  Created by Mobile on 8/28/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import FirebaseAuth
import FBSDKLoginKit
import ProgressHUD

class FUser: FObject {
    //MARK: - Class methods
    
    static func currentId() ->String?
    {
        return (Auth.auth().currentUser?.uid)
    }
    
    static func currentUser() ->FUser?
    {
        if Auth.auth().currentUser != nil{
            let dictionary = UserDefaults.standard.object(forKey: "CurrentUser") as! [String : Any]
            CurrentUser.shared.getUserStatus(dic: dictionary)
            return FUser(path: FConst.FUser.PATH, dictionary:dictionary)
        }
        return nil
    }
    
    //MARK: - Initialization methods

    static func user(userId:String) -> FUser
    {
        let user = FUser(path: FConst.FUser.PATH)
        user.dictionary["objectId"] = userId;
        return user;
    }

    // MARK: - Email methods
    static func signIn(email:String, password:String, completion:((FUser?,Error?) ->())?)
    {
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            if error == nil && authResult != nil{
                FUser.load(firuser: authResult!.user,isFacebook:false,  completion: { (fuser, error) in
                    if error == nil{
                        completion!(fuser,nil)
                    }else{
                        self.logOut()
                        completion!(nil,error)
                    }
                })
            }else{
                completion!(nil,error)
            }
        }
    }

    static func createUser(email:String!, password:String!, completion:((FUser?,Error?) ->())?)
    {
        Auth.auth().createUser(withEmail: email, password: password, completion: { (authResult, error) in
            if error == nil{
                FUser.create(uid: authResult!.user.uid, email: email, isFacebook: false, completion: { (fuser, error) in
                    if error == nil{
                        if completion !=  nil{
                            completion!(fuser,nil)
                        }
                    }else{
                        authResult!.user.delete(completion: { (error) in
                            if error != nil{
                                self.logOut()
                            }
                        })
                        if completion != nil{
                            completion!(nil, error)
                        }
                    }
                })
            }else{
                if completion != nil{
                    completion!(nil, error)
                }
            }
        })
    }

    //MARK: - Facebook methods

    static func signInWithFacebook(viewController:UIViewController, completion:((FUser?,Error?) ->())?)
    {
        var accessToken : String?
        if FBSDKAccessToken.current() != nil{
            accessToken = FBSDKAccessToken.current().tokenString
        }
    
        if let accessToken = accessToken{
            ProgressHUD.show()
            let credential : AuthCredential = FacebookAuthProvider.credential(withAccessToken: accessToken)
            signIn(credential: credential, completion: completion)
            return
        }
        let login : FBSDKLoginManager = FBSDKLoginManager()
        let permissions = ["public_profile", "email"];
        login.logIn(withReadPermissions: permissions, from: viewController) { (result, error) in
            ProgressHUD.show()
            if error == nil{
                if result?.isCancelled == false{
                    let accessToken = FBSDKAccessToken.current().tokenString
                    let credential : AuthCredential = FacebookAuthProvider.credential(withAccessToken: accessToken!)
                    signIn(credential: credential, completion: completion)
                }else{
                    ProgressHUD.dismiss()
                    if (completion != nil){
                        completion!(nil, nil)
                    }
                }
            }else{
                if (completion != nil){
                    completion!(nil, nil)
                }
            }
        }
    }

    //MARK: - Credential methods

    static func signIn(credential:AuthCredential, completion:((FUser?,Error?) ->())?)
    {
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error == nil{
                FUser.load(firuser: authResult!.user,isFacebook: true, completion: { (fuser, error) in
                    if error == nil{
                        if completion != nil{
                            completion!(fuser, nil)
                        }
                    }else{
                        self.logOut()
                        if completion != nil{
                            completion!(nil, error)
                        }
                    }
                })
            }else{
                completion!(nil, error)
            }
        }
    }


    //MARK: - Logut methods

    static func logOut() ->Bool
    {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
            if FBSDKAccessToken.current() != nil{
                FBSDKLoginManager().logOut()
            }
            return true
        } catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
            return false
        }
    }
//
    //MARK: - Private methods


    static func load(firuser:User, isFacebook:Bool, completion:((FUser?, Error?)->())?)
    {
        let user = FUser.user(userId: firuser.uid)
        user.fetchInBackground { (error) in
            if error == nil{
                if completion != nil{
                    completion!(user,nil)
                }
            }else{
                self.create(uid: firuser.uid, email: firuser.email!,isFacebook: isFacebook, completion: completion)
            }
        }
    }

    static func create(uid:String, email:String, isFacebook:Bool, completion:((FUser?,Error?) ->())?)
    {
        let user = FUser.user(userId: uid)
        if (email.count>0){
            user.dictionary["email"] = email
        }
        let onesignalId = UserDefaults.standard.object(forKey: FConst.FUser.ONESIGNALID) as? String
        if (onesignalId == nil){
            user.dictionary[FConst.FUser.ONESIGNALID] = ""
        }else {
            user.dictionary[FConst.FUser.ONESIGNALID] = onesignalId
        }

  
        user.dictionary[FConst.FUser.PHONE] = "";
        user.dictionary[FConst.FUser.PICTURE] = "";
        user.dictionary[FConst.FUser.THUMBNAIL] = "";
        
        if isFacebook == true{
            let parameters = "picture.type(large), email, name,first_name,last_name"
            let request = FBSDKGraphRequest.init(graphPath: "me", parameters: ["fields": parameters])
            request?.start(completionHandler: { (connection, result, error) in
                if error == nil{
                    let results = result as! [String : Any]
                    let firstname : String? = results["first_name"] as? String
                    let lastname : String? = results["last_name"] as? String
                    let name : String? = results["name"] as? String
                    user.dictionary[FConst.FUser.FIRSTNAME] = firstname
                    user.dictionary[FConst.FUser.LASTNAME] = lastname
                    user.dictionary[FConst.FUser.FULLNAME] = name
                    let pictureUrl: String? = ((results["picture"] as! [String:Any])["data"] as! [String:Any])["url"] as? String
                    CurrentUser.shared.userImage = pictureUrl
                    if pictureUrl != nil{
                        user.dictionary[FConst.FUser.PICTURE] = pictureUrl
                        user.dictionary[FConst.FUser.THUMBNAIL] = pictureUrl
                    }else{
                        user.dictionary[FConst.FUser.PICTURE] = ""
                        user.dictionary[FConst.FUser.THUMBNAIL] = ""
                    }
                    user.saveInBackground(block: { (error) in
                        if error == nil{
                            if completion != nil{
                                completion!(user,nil)
                            }
                        }else{
                            if completion != nil{
                                completion!(nil, error)
                            }
                        }
                    })
                }
            })
        }else{
            if CurrentUser.shared.registerModel != nil{
                for (key,value) in CurrentUser.shared.registerModel!{
                    user.dictionary[key] = value
                }
                user.saveInBackground(block: { (error) in
                    if error == nil{
                        if completion != nil{
                            completion!(user,nil)
                        }
                    }else{
                        if completion != nil{
                            completion!(nil, error)
                        }
                    }
                })
            }
        }
    }


    //MARK: - Current user methods


    func isCurrent() ->Bool
    {
        return (dictionary["objectId"] as? String == FUser.currentId())
    }

    func saveLocalIfCurrent()
    {
        if (self.isCurrent())
        {
            UserDefaults.standard.set(dictionary, forKey: "CurrentUser")
            UserDefaults.standard.synchronize()
            CurrentUser.shared.getUserStatus(dic: dictionary)
        }
    }

    //MARK: - Save methods

    override func saveInBackground()
    {
        self.saveLocalIfCurrent()
        super.saveInBackground()
    }

    override func saveInBackground(block:((Error?) ->())?){
        self.saveLocalIfCurrent()
        super.saveInBackground { (error) in
            if error == nil{
                self.saveLocalIfCurrent()
            }
            if (block != nil) {
                block!(error)
            }
        }
    }
    
//    MARK: - Fetch methods

    override func fetchInBackground()
    {
        super.fetchInBackground {(error) in
            if(error != nil){
                self.saveLocalIfCurrent()
            }
        }
    }

    override func fetchInBackground(block:((Error?) ->())?)
    {
        super.fetchInBackground { (error) in
            if error == nil {
                self.saveLocalIfCurrent()
            }
            if (block != nil){
                block!(error)
            }
        }
    }
    
    class func initials()-> String
    {
        if (CurrentUser.shared.firstname.count != 0) && (CurrentUser.shared.lastname.count != 0) {
            return "\(CurrentUser.shared.firstname.prefix(1))\(CurrentUser.shared.lastname.prefix(1))"
        }
        return ""

    }
}
