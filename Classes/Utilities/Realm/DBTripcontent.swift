//
//  DBShootCode.swift
//  WeddingProject
//
//  Created by Mobile on 9/3/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import Realm
class DBTripcontent: RLMObject {
    @objc dynamic var objectId : String = ""
    @objc dynamic var name : String = ""
    @objc dynamic var picture: String = ""
    @objc dynamic var isDeleted = false
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var updatedAt: Int64 = 0
    
    override static func primaryKey() ->String
    {
        return FConst.FTripContent.OBJECTID;
    }
    
    static func lastUpdatedAt() -> (Int64)
    {
        print("Total TripContent Count: %d", DBTripcontent.allObjects().count)
        let dbtripcontent = DBTripcontent.allObjects().sortedResults(usingKeyPath: FConst.FTripContent.UPDATEDAT, ascending: true).lastObject() as? DBTripcontent
        if dbtripcontent == nil {
            return 0
        }
        return dbtripcontent!.updatedAt
    }
}
