//
//  DBShootCode.swift
//  WeddingProject
//
//  Created by Mobile on 9/3/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import Realm
class DBMessage: RLMObject {
    @objc dynamic var objectId : String = ""
    @objc dynamic var groupId : String = ""
    @objc dynamic var senderId : String = ""
    @objc dynamic var senderName : String = ""

    @objc dynamic var senderInitials: String = ""
    
    @objc dynamic var type: String = ""
    @objc dynamic var text: String = ""
    
    @objc dynamic var picture: String = ""
    @objc dynamic var picture_width:Int = 0
    @objc dynamic var picture_height:Int = 0
    @objc dynamic var picture_md5: String = ""
    
    @objc dynamic var video: String = ""
    @objc dynamic var video_duration:Int = 0
    @objc dynamic var video_md5: String = ""
    
    @objc dynamic var audio: String = ""
    @objc dynamic var audio_duration:Int = 0
    @objc dynamic var audio_md5: String = ""
    
    @objc dynamic var latitude: Float = 0
    @objc dynamic var longitude: Float = 0

    
    @objc dynamic var status: String = ""
    @objc dynamic var isDeleted: Bool = false
    
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var updatedAt: Int64 = 0
    
    override static func primaryKey() ->String
    {
        return FConst.FMessage.OBJECTID;
    }
    
    static func lastUpdatedAt() -> (Int64)
    {
        print(DBMessage.allObjects().count)
        let dbmessage = DBMessage.allObjects().sortedResults(usingKeyPath: FConst.FMessage.UPDATEDAT, ascending: true).lastObject() as? DBMessage
        if dbmessage == nil {
            return 0
        }
        return dbmessage!.updatedAt
    }
}
