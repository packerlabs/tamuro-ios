//
//  DBTrip.swift
//  Tamuro
//
//  Created by Mobile on 11/20/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
class DBTrip: RLMObject {
    @objc dynamic var objectId = ""
    @objc dynamic var tripId = ""
    @objc dynamic var title = ""
    @objc dynamic var about = ""
    @objc dynamic var picture = ""
    @objc dynamic var userId = ""
    
    @objc dynamic var cost = 0
    @objc dynamic var raised = 0  
    @objc dynamic var paymentType = ""
    
    @objc dynamic var tripContents = ""
    @objc dynamic var members = ""
    @objc dynamic var followers = ""
    @objc dynamic var startDate = ""
    @objc dynamic var maxMemberNum = 0
    @objc dynamic var day = 0
    @objc dynamic var night = 0
    
    @objc dynamic var popularity = 0
    
    @objc dynamic var isPrivate = false
    @objc dynamic var isPaid = false
    @objc dynamic var isDeleted = false
    @objc dynamic var isAchieved = false
    
    @objc dynamic var address = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    
    @objc dynamic var startDateStamp : Int64 = 0
    @objc dynamic var endDateStamp : Int64 = 0
    @objc dynamic var createdAt : Int64 = 0
    @objc dynamic var updatedAt : Int64 = 0
    
    override static func primaryKey() ->String
    {
        return FConst.FTrip.OBJECTID;
    }
    
    
    static func lastUpdatedAt() -> (Int64)
    {
        print("Total Trips Count: %d", DBTrip.allObjects().count)
        let dbtrip = DBTrip.allObjects().sortedResults(usingKeyPath: FConst.FTrip.UPDATEDAT, ascending: true).lastObject() as? DBTrip
        if dbtrip == nil {
            return 0
        }
        return dbtrip!.updatedAt
    }
}
