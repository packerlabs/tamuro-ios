//
//  DBGroup.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
class DBGroup: RLMObject {
    @objc dynamic var objectId = ""
    @objc dynamic var userId = ""
    @objc dynamic var name = ""
    @objc dynamic var picture = ""
    @objc dynamic var members = ""

    @objc dynamic var isDeleted = false
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var updatedAt: Int64 = 0
    
    
    override static func primaryKey() ->String
    {
        return FConst.FGroup.OBJECTID;
    }
    
    
    static func lastUpdatedAt() -> (Int64)
    {
        print(DBGroup.allObjects().count)
        let dbgroup = DBGroup.allObjects().sortedResults(usingKeyPath: FConst.FGroup.UPDATEDAT, ascending: true).lastObject() as? DBGroup
        if dbgroup == nil {
            return 0
        }
        return dbgroup!.updatedAt
    }
}
