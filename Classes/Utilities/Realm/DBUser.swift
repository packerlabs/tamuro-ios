//
//  DBUser.swift
//  WeddingProject
//
//  Created by Mobile on 8/31/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import Realm
class DBUser: RLMObject {
    @objc dynamic var objectId : String = ""
    @objc dynamic var email = ""
    @objc dynamic var phone = ""
    
    @objc dynamic var firstname = ""
    @objc dynamic var lastname = ""
    @objc dynamic var fullname = ""

    @objc dynamic var status = ""

    
    @objc dynamic var picture = ""
    @objc dynamic var thumbnail = ""

    @objc dynamic var oneSignalId = ""

    @objc dynamic var address = ""
    @objc dynamic var latitude = 0.0
    @objc dynamic var longitude = 0.0
    
    @objc dynamic var createdAt : Int64 = 0
    @objc dynamic var updatedAt : Int64 = 0
    
    override static func primaryKey() ->String
    {
        return FConst.FUser.OBJECTID;
    }    
    
    static func lastUpdatedAt() -> (Int64)
    {
        print("Total Users Count: %d", DBUser.allObjects().count)
        let dbuser = DBUser.allObjects().sortedResults(usingKeyPath: FConst.FUser.UPDATEDAT, ascending: true).lastObject() as? DBUser
        if dbuser == nil {
            return 0
        }
        return dbuser!.updatedAt
    }
    func initials() -> String? {
        if (self.firstname.count != 0) && (self.lastname.count == 0) {
            return (self.firstname as NSString).substring(to: 1)
        }
        if (self.firstname.count == 0) && (self.lastname.count != 0) {
            return (self.lastname as NSString).substring(to: 1)
        }
        if (self.firstname.count != 0) && (self.lastname.count != 0) {
            return "\((self.firstname as NSString).substring(to: 1))\((self.lastname as NSString).substring(to: 1) )"
        }        
        return nil
    }

}

