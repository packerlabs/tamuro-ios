//
//  DBMedia.swift
//  WeddingProject
//
//  Created by Mobile on 8/28/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import Realm
class DBRecent: RLMObject {
    @objc dynamic var objectId : String = ""
    @objc dynamic var userId = ""
    @objc dynamic var groupId = ""
    @objc dynamic var initials = ""
    @objc dynamic var picture = ""
    @objc dynamic var desc = ""
    @objc dynamic var members = ""
    @objc dynamic var password = ""
    @objc dynamic var type = ""
    @objc dynamic var counter: Int = 0
    @objc dynamic var lastMessage = ""
    @objc dynamic var lastMessageDate: Int64 = 0
    @objc dynamic var isArchived = false
    @objc dynamic var isDeleted = false
    @objc dynamic var createdAt: Int64 = 0
    @objc dynamic var updatedAt: Int64 = 0

    
    override static func primaryKey() ->String
    {
        return FConst.FRecent.OBJECTID;
    }
    
    
    static func lastUpdatedAt() -> (Int64)
    {
        print(DBRecent.allObjects().count)
        let dbrecent = DBRecent.allObjects().sortedResults(usingKeyPath: FConst.FRecent.UPDATEDAT, ascending: true).lastObject() as? DBRecent
        if dbrecent == nil {
            return 0
        }
        return dbrecent!.updatedAt
    }
}
