

#import <JSQMessagesViewController/JSQMediaItem.h>

@interface EmojiMediaItem : JSQMediaItem <JSQMessageMediaData, NSCoding, NSCopying>


@property (nonatomic, strong) NSString *text;

- (instancetype)initWithText:(NSString *)text;

@end

