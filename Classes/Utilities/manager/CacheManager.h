

#import <Foundation/Foundation.h>


@interface CacheManager : NSObject


+ (void)cleanupExpired;

+ (void)cleanupManual;

+ (long long)total;

@end

