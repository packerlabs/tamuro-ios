//
//  MediaManager.swift
//  Tamuro
//
//  Created by Mobile on 11/1/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class MediaManager: NSObject {
    class func loadPicture(_ mediaItem: PhotoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        let path = DownloadManager.pathImage(dbmessage?.picture)
//        let localIdentifier = UserDefaults.standard.string(forKey: dbmessage?.objectId ?? "")
//
//        if (path == nil) && (localIdentifier == nil) {
//            self.loadPictureMedia(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
//        } else if path != nil {
//            self.showPictureFile(mediaItem, path: path, collectionView: collectionView)
//        } else if localIdentifier != nil {
//            self.loadPictureAlbum(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
//        }
    }

    
    
    class func loadPictureManual(_ mediaItem: PhotoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
    }

    // MARK: - Picture private


    class func loadPictureMedia(_ mediaItem: PhotoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//            [self downloadPictureMedia:mediaItem dbmessage:dbmessage collectionView:collectionView];
    }


    class func downloadPictureMedia(_ mediaItem: PhotoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        mediaItem.status = STATUS_LOADING;
//        [DownloadManager image:dbmessage.picture md5:dbmessage.picture_md5 completion:^(NSString *path, NSError *error, BOOL network)
//            {
//            if (error == nil)
//            {
//            if (network) [Cryptor decryptFile:path groupId:dbmessage.groupId];
//
//            [self showPictureFile:mediaItem Path:path collectionView:collectionView];
//
//            if ([FUser autoSaveMedia]) [self savePictureAlbum:mediaItem dbmessage:dbmessage path:path];
//            }
//            else mediaItem.status = STATUS_MANUAL;
//            [collectionView reloadData];
//            }];
    }


    class func loadPictureAlbum(_ mediaItem: PhotoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
    }

    class func savePictureAlbum(_ mediaItem: PhotoMediaItem?, dbmessage: DBMessage?, path: String?) {
    }



    class func showPictureImage(_ mediaItem: PhotoMediaItem?, image: UIImage?, collectionView: UICollectionView?) {
//        mediaItem.image = image;
//        mediaItem.status = STATUS_SUCCEED;
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [collectionView reloadData];
//            });
    }

    class func showPictureFile(_ mediaItem: PhotoMediaItem?, path: String?, collectionView: UICollectionView?) {
//        mediaItem.image = [[UIImage alloc] initWithContentsOfFile:path];
//        mediaItem.status = STATUS_SUCCEED;
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [collectionView reloadData];
//            });
    }


    // MARK: - Video public

    class func loadVideo(_ mediaItem: VideoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        NSString *path = [DownloadManager pathVideo:dbmessage.video];
//        NSString *localIdentifier = [UserDefaults stringForKey:dbmessage.objectId];
//
//        if ((path == nil) && (localIdentifier == nil))
//        {
//            [self loadVideoMedia:mediaItem dbmessage:dbmessage collectionView:collectionView];
//        }
//        else if (path != nil)
//        {
//            [self showVideoFile:mediaItem Path:path collectionView:collectionView];
//        }
//        else if (localIdentifier != nil)
//        {
//            [self loadVideoAlbum:mediaItem dbmessage:dbmessage collectionView:collectionView];
//        }
    }

    class func loadVideoManual(_ mediaItem: VideoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?)
    {
        
    }

    // MARK: - Video private
    class func loadVideoMedia(_ mediaItem: VideoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?)
    {
//        [self downloadVideoMedia:mediaItem dbmessage:dbmessage collectionView:collectionView];
    }



    class func downloadVideoMedia(_ mediaItem: VideoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        mediaItem.status = STATUS_LOADING;        
//        [DownloadManager video:dbmessage.video md5:dbmessage.video_md5 completion:^(NSString *path, NSError *error, BOOL network)
//            {
//            if (error == nil)
//            {
//            if (network) [Cryptor decryptFile:path groupId:dbmessage.groupId];
//
//            [self showVideoFile:mediaItem Path:path collectionView:collectionView];
//
//            if ([FUser autoSaveMedia]) [self saveVideoAlbum:mediaItem dbmessage:dbmessage path:path collectionView:collectionView];
//            }
//            else mediaItem.status = STATUS_MANUAL;
//            [collectionView reloadData];
//            }];
    }
    
    class func loadVideoAlbum(_ mediaItem: VideoMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
    }
    
    class func saveVideoAlbum(_ mediaItem: VideoMediaItem?, dbmessage: DBMessage?, path path1: String?, collectionView: UICollectionView?) {
    }


    class func showVideoFile(_ mediaItem: VideoMediaItem?, path: String?, collectionView: UICollectionView?) {
//        UIImage *picture = [Video thumbnail:path];
//        mediaItem.image = [Image square:picture size:320];
//        mediaItem.fileURL = [NSURL fileURLWithPath:path];
//        mediaItem.status = STATUS_SUCCEED;
//
//        dispatch_async(dispatch_get_main_queue(), ^{
//            [collectionView reloadData];
//            });
    }
    
    // MARK: - Audio public
    class func loadAudio(_ mediaItem: AudioMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        NSString *path = [DownloadManager pathAudio:dbmessage.audio];
//        NSString *localIdentifier = [UserDefaults stringForKey:dbmessage.objectId];
//
//        if ((path == nil) && (localIdentifier == nil))
//        {
//            [self loadAudioMedia:mediaItem dbmessage:dbmessage collectionView:collectionView];
//        }
//        else if (path != nil)
//        {
//            [self showAudioFile:mediaItem Path:path collectionView:collectionView];
//        }
//        else if (localIdentifier != nil)
//        {
//            [self loadAudioAlbum:mediaItem dbmessage:dbmessage collectionView:collectionView];
//        }
    }
    
    class func loadAudioManual(_ mediaItem: AudioMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
    }
    
    // MARK: - Audio private

    class func loadAudioMedia(_ mediaItem: AudioMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        [self downloadAudioMedia:mediaItem dbmessage:dbmessage collectionView:collectionView];
    }



    class func downloadAudioMedia(_ mediaItem: AudioMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
//        mediaItem.status = STATUS_LOADING;
//
//        [DownloadManager audio:dbmessage.audio md5:dbmessage.audio_md5 completion:^(NSString *path, NSError *error, BOOL network)
//            {
//            if (error == nil)
//            {
//            if (network) [Cryptor decryptFile:path groupId:dbmessage.groupId];
//
//            [self showAudioFile:mediaItem Path:path collectionView:collectionView];
//
//            if ([FUser autoSaveMedia]) [self saveAudioAlbum:mediaItem dbmessage:dbmessage path:path collectionView:collectionView];
//            }
//            else mediaItem.status = STATUS_MANUAL;
//            [collectionView reloadData];
//            }];
    }
    
    class func loadAudioAlbum(_ mediaItem: AudioMediaItem?, dbmessage: DBMessage?, collectionView: UICollectionView?) {
    }
    
    class func saveAudioAlbum(_ mediaItem: AudioMediaItem?, dbmessage: DBMessage?, path path1: String?, collectionView: UICollectionView?) {
    }



    class func showAudioFile(_ mediaItem: AudioMediaItem?, path: String?, collectionView: UICollectionView?) {
//        mediaItem?.audioDataWithUrl = URL(fileURLWithPath: path ?? "")
//        mediaItem?.status = STATUS_SUCCEED
//
//        DispatchQueue.main.async(execute: {
//            collectionView?.reloadData()
//        })
    }
}
