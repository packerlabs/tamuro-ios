

#import "RealmManager.h"
#import <Realm/Realm.h>
@implementation RealmManager

+ (void)cleanupDatabase
{
	RLMRealm *realm = [RLMRealm defaultRealm];
	[realm beginWriteTransaction];
	[realm deleteAllObjects];
	[realm commitWriteTransaction];
}

@end

