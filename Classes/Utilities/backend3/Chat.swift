//
//  Chat.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Realm

class Chat: NSObject {
    
    class func groupId(_ members: [String]) -> String? {
        let sorted = members.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
        return Checksum.md5Hash(of: sorted.joined(separator: ""))
    }


    // MARK: - Private Chat methods

    class func startPrivate(_ dbuser2: DBUser) -> [String : Any]?
    {
        let userId1 = CurrentUser.shared.objectId
        let userId2 = dbuser2.objectId

        let initials1 = FUser.initials()
        let initials2 = dbuser2.initials()

        let picture1 = CurrentUser.shared.picture
        let picture2 = dbuser2.picture
        let name1 = CurrentUser.shared.fullname
        let name2 = dbuser2.fullname
        let members: [String] = [userId1, userId2]
        let groupId: String! = self.groupId(members)
        Password.init(groupId)

        Recent.fetchMembers(groupId) { userIds in
            if userIds?.contains(userId1) == false {
                Recent.createPrivate(userId1, groupId: groupId!, initials: initials2, picture: picture2, description: name2, members: members)
            }
            if userIds?.contains(userId2) == false {
                Recent.createPrivate(userId2, groupId: groupId!, initials: initials1, picture: picture1, description: name1, members: members)
            }
        }
        return ["groupId": groupId, "members": members, "type": Constant.Chat.CHAT_PRIVATE]
    }

    // MARK: - Multiple Chat methods
    
    class func startMultiple(_ userIds: [String]) -> [String : Any]?  {
        var members = userIds
        members.append(CurrentUser.shared.objectId)
        let groupId: String! = self.groupId(members)
        Password.init(groupId)
        Recent.createMultiple(groupId, members: members)
        return ["groupId": groupId, "members": members, "type": Constant.Chat.CHAT_MULTIPLE]
    }

    // MARK: - Group Chat methods
//    class func startGroup1(_ group: FObject) -> [String : Any]?
//    {
//        let members = group.dictionary[FConst.FGroup.MEMBERS] as! [String]
//        let groupId = group.objectId()
//        let picture: String? = (group.dictionary[FConst.FGroup.PICTURE] != nil) ? group.dictionary[FConst.FGroup.PICTURE] as? String : CurrentUser.shared.picture
//
//        Password.init(groupId)
//
//        Recent.createGroup(groupId, picture: picture, description: group.dictionary[FConst.FGroup.NAME] as? String, members: members)
//
//        return ["groupId": groupId, "members": members, "type": Constant.Chat.CHAT_GROUP]
//    }
//
//    class func startGroup2(_ dbgroup: DBGroup) -> [String : Any]?
//    {
//        let members = dbgroup.members.components(separatedBy: ",")
//        let groupId = dbgroup.objectId
//
//        let picture: String = (dbgroup.picture.count != 0) ? dbgroup.picture : CurrentUser.shared.picture
//
//        Password.init(groupId)
//
//        Recent.createGroup(groupId, picture: picture, description: dbgroup.name, members: members)
//
//        return ["groupId": groupId , "members": members, "type": Constant.Chat.CHAT_GROUP]
//    }
    class func startGroup1(_ trip: FObject) -> [String : Any]?
    {
        let members = trip.dictionary[FConst.FTrip.MEMBERS] as! [String]
        let groupId = trip.objectId()
        let picture: String? = (trip.dictionary[FConst.FTrip.PICTURE] != nil) ? trip.dictionary[FConst.FTrip.PICTURE] as? String : CurrentUser.shared.picture
        
        Password.init(groupId)
        
        Recent.createGroup(groupId, picture: picture, description: trip.dictionary[FConst.FTrip.TITLE] as? String, members: members)
        
        return ["groupId": groupId, "members": members, "type": Constant.Chat.CHAT_GROUP]
    }
    
    class func startGroup2(_ dbtrip: DBTrip) -> [String : Any]?
    {
        let members = dbtrip.members.components(separatedBy: ",")
        let groupId = dbtrip.objectId
        
        let picture: String = (dbtrip.picture.count != 0) ? dbtrip.picture : CurrentUser.shared.picture
        
        Password.init(groupId)
        
        Recent.createGroup(groupId, picture: picture, description: dbtrip.title, members: members)
        
        return ["groupId": groupId , "members": members, "type": Constant.Chat.CHAT_GROUP]
    }
    // MARK: - Restart Recent Chat methods
    class func restart(_ dbrecent: DBRecent) -> [String : Any]?
    {
        let members = dbrecent.members.components(separatedBy: ",")
        return ["groupId": dbrecent.groupId, "members": members, "type": dbrecent.type]
    }
}
