//
//  Recent.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Realm
class Recent: NSObject {
    
    class func fetchRecents(_ groupId: String, completion: @escaping ([FObject]?) -> Void) {
        let firebase = Database.database().reference().child(FConst.FRecent.PATH)
        let query: DatabaseQuery = firebase.queryOrdered(byChild: FConst.FRecent.GROUPID).queryEqual(toValue: groupId)
        query.observeSingleEvent(of: .value) { (snapshot) in
            var recents: [FObject] = []
            if let array = snapshot.children.allObjects as? [DataSnapshot] {
                for dataSnapshot : DataSnapshot in array{
                    let dictionary : [String : Any] = dataSnapshot.value as! [String : Any]
                    let recent: FObject = FObject.object(path: FConst.FRecent.PATH, dictionary: dictionary)
                    recents.append(recent)
                }
                completion(recents)
            } else {
                completion([])
            }
        }
    }
    
    class func fetchMembers(_ groupId: String, completion: @escaping ([String]?) -> Void) {
        let firebase = Database.database().reference().child(FConst.FRecent.PATH)
        let query: DatabaseQuery? = firebase.queryOrdered(byChild: FConst.FRecent.GROUPID).queryEqual(toValue: groupId)
        query?.observeSingleEvent(of: .value) { snapshot in
            if let array = snapshot.children.allObjects as? [DataSnapshot] {
                let ids = array.map({ (snapshot) -> String in
                    return ((snapshot.value as? [String: Any])?[FConst.FRecent.USERID] as? String)!
                })
                completion(ids)
            } else {
                completion([])
            }
        }
    }
    
    // MARK: - Create methods
    
    class func createPrivate(_ userId: String, groupId: String, initials: String?, picture: String?, description: String?, members: [String]?) {
        self.createItem(userId: userId, groupId: groupId, initials: initials, picture: picture, description: description, members: members, type: Constant.Chat.CHAT_PRIVATE)
    }

    class func createMultiple(_ groupId: String, members: [String]?) {
        self.fetchMembers(groupId) { userIds in
            var createIds: [String]? = nil
            if let aMembers = members {
                createIds = aMembers
            }

            for userId: String? in userIds ?? [] {
                createIds?.removeAll(where: { element in element == userId })
            }

            for userId: String? in createIds ?? [] {
                let description = self.userNamesFor(members: members, userId: userId)
                self.createItem(userId: userId!, groupId: groupId, initials: FUser.initials(), picture: CurrentUser.shared.picture, description: description, members: members, type: Constant.Chat.CHAT_MULTIPLE)
            }
        }
    }

    class func createGroup(_ groupId: String, picture: String?, description: String?, members: [String]?) {
        self.fetchMembers(groupId) { userIds in
            var createIds: [String]? = nil
            if let aMembers = members {
                createIds = aMembers
            }

            for userId: String in userIds ?? [] {
                createIds?.removeAll(where: { element in element == userId })
            }

            for userId: String in createIds ?? [] {
                self.createItem(userId: userId, groupId: groupId, initials: FUser.initials(), picture: picture, description: description, members: members, type: Constant.Chat.CHAT_GROUP)
            }
        }
    }

    class func createItem(userId: String, groupId: String, initials: String?, picture: String?, description: String?, members: [String]?, type: String?)
    {
        let recent: FObject = FObject.object(path: FConst.FRecent.PATH)
    
        let temp = groupId + userId
        recent.dictionary[FConst.FRecent.OBJECTID] = Checksum.md5Hash(of: temp)

        recent.dictionary[FConst.FRecent.USERID] = userId;
        recent.dictionary[FConst.FRecent.GROUPID] = groupId;

        recent.dictionary[FConst.FRecent.INITIALS] = initials;
        recent.dictionary[FConst.FRecent.PICTURE] = (picture != nil) ? picture : "";
        recent.dictionary[FConst.FRecent.DESCRIPTION] = description;
        recent.dictionary[FConst.FRecent.MEMBERS] = members;
        recent.dictionary[FConst.FRecent.PASSWORD] = Password.get(groupId)
        recent.dictionary[FConst.FRecent.TYPE] = type;

        recent.dictionary[FConst.FRecent.COUNTER] = 0;
        recent.dictionary[FConst.FRecent.LASTMESSAGE] = "";
        recent.dictionary[FConst.FRecent.LASTMESSAGEDATE] = ServerValue.timestamp()
        
        recent.dictionary[FConst.FRecent.ISARCHIVED] = false
        recent.dictionary[FConst.FRecent.ISDELETED] = false
        recent.saveInBackground()

    }

    // MARK: - Update methods
    
    class func updateLastMessage(_ message: FObject) {
        self.fetchRecents(message.dictionary[FConst.FMessage.GROUPID] as! String) { recents in
            for recent: FObject in recents! {
                self.updateLastMessage(recent, lastMessage: message.dictionary[FConst.FMessage.TEXT] as! String)
            }
        }
    }

    class func updateLastMessage(_ recent: FObject, lastMessage: String) {
        var counter = recent.dictionary[FConst.FRecent.COUNTER] as! Int
        
        if (recent.dictionary[FConst.FRecent.USERID] as? String != FUser.currentId()){
            if (recent.dictionary[FConst.FRecent.TYPE] as? String == Constant.Chat.CHAT_PRIVATE) {
                recent.dictionary[FConst.FRecent.PICTURE] = CurrentUser.shared.thumbnail
            }
            counter += 1
        }
        recent.dictionary[FConst.FRecent.COUNTER] = counter
        recent.dictionary[FConst.FRecent.LASTMESSAGE] = lastMessage
        recent.dictionary[FConst.FRecent.LASTMESSAGEDATE] = ServerValue.timestamp()
        
        let activate: Bool = (recent.dictionary[FConst.FRecent.MEMBERS] as! [String]).contains(recent.dictionary[FConst.FRecent.USERID] as! String)
        if activate {
            recent.dictionary[FConst.FRecent.ISARCHIVED] = false
        }
        if activate {
            recent.dictionary[FConst.FRecent.ISDELETED] = false
        }
        recent.saveInBackground()
    }


    class func updateMembers(_ group: FObject)
    {
        fetchRecents(group.dictionary[FConst.FTrip.OBJECTID] as! String) { (recents) in
            for recent in recents!{
                let set1 = Set<String>((group.dictionary[FConst.FTrip.MEMBERS] as? [String])!)
                let set2 = Set<String>((recent.dictionary[FConst.FRecent.MEMBERS] as? [String])!)
                if set1.elementsEqual(set2) == false {
                    updateMembers(recent, members: group.dictionary[FConst.FTrip.MEMBERS] as! [String])
                }
            }
        }
    }

    class func updateMembers(_ recent: FObject?, members: [String]) {
        if members.contains(recent!.dictionary[FConst.FRecent.USERID] as! String) == false {
            recent?.dictionary[FConst.FRecent.ISDELETED] = true
        }
  
        
        recent?.dictionary[FConst.FRecent.MEMBERS] = members
        recent?.saveInBackground()
    }

    class func updateDescription(_ group: FObject) {
        self.fetchRecents(group.objectId()) { recents in
            for recent: FObject? in recents ?? [] {
                recent?.dictionary[FConst.FRecent.DESCRIPTION] = group.dictionary[FConst.FTrip.TITLE]
                recent?.saveInBackground()
            }
        }
    }

    class func updatePicture(_ group: FObject) {
        self.fetchRecents(group.objectId()) { recents in
            for recent: FObject? in recents ?? [] {
                recent?.dictionary[FConst.FRecent.PICTURE] = group.dictionary[FConst.FTrip.PICTURE]
                recent?.saveInBackground()
            }
        }
    }

    // MARK: - Delete/Archive methods
    
    class func deleteItem(_ objectId: String?) {
        let object = FObject.object(path: FConst.FRecent.PATH)
        object.dictionary[FConst.FRecent.OBJECTID] = objectId ?? ""
        object.dictionary[FConst.FRecent.ISDELETED] = true
        object.updateInBackground()
    }

    class func archiveItem(_ objectId: String?) {
        let object = FObject.object(path: FConst.FRecent.PATH)
        object.dictionary[FConst.FRecent.OBJECTID] = objectId ?? ""
        object.dictionary[FConst.FRecent.ISARCHIVED] = true
        object.updateInBackground()
    }

    class func unarchiveItem(_ objectId: String?) {
        let object = FObject.object(path: FConst.FRecent.PATH)
        object.dictionary[FConst.FRecent.OBJECTID] = objectId ?? ""
        object.dictionary[FConst.FRecent.ISARCHIVED] = false
        object.updateInBackground()
    }
    
    // MARK: - Clear methods
    
    class func clearCounter(_ groupId: String) {
        self.fetchRecents(groupId) { recents in
            for recent: FObject in recents! {
                if (recent.dictionary[FConst.FRecent.USERID] as? String == FUser.currentId()) {
                    recent.dictionary[FConst.FRecent.COUNTER] = 0
                    recent.saveInBackground()
                }
            }
        }
    }
    
    class func userNamesFor(members: [String]?, userId: String?) -> String? {
        var names: [String] = []
        let dbusers: RLMResults = DBUser.allObjects().sortedResults(usingKeyPath: FConst.FUser.FULLNAME, ascending: true)
        for i in 0..<dbusers.count {
            let dbuser : DBUser = dbusers[i] as! DBUser
            if members?.contains(dbuser.objectId) == true {
                if dbuser.objectId != userId {
                    names.append(dbuser.fullname)
                }
            }
          
        }
        return names.joined(separator: ", ")
    }
}
