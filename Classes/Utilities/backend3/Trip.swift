//
//  Trip.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
class Trip: NSObject {
    
    class func getObjectfromId(objectId:String) ->DBUser?{
        let predicate = NSPredicate(format: "objectId == %@", objectId)
        let dbuser = DBUser.objects(with: predicate).firstObject() as? DBUser
        return dbuser
    }
    
    class func addTripMembers(objectId: String, memberIds: [String], complete: @escaping(Error?) ->Void){
        let object = FObject.object(path: FConst.FTrip.PATH)
        object.dictionary[FConst.FTrip.OBJECTID] = objectId
        object.fetchInBackground(block: { (error) in
            if error == nil{
                var members = object.dictionary[FConst.FTrip.MEMBERS] as! [String]
                members += memberIds
                object.dictionary[FConst.FTrip.MEMBERS] = members
                object.saveInBackground(block: { (error) in
                    if error == nil{
                        Chat.startGroup1(object)
                        Recent.updateMembers(object)
                    }
                    complete(error)
                })
            }
            complete(error)
        })
    }
    
    class func delGroupMember(objectId: String, memberId: String, complete: @escaping(Error?) ->Void){
        let object = FObject.object(path: FConst.FTrip.PATH)
        object.dictionary[FConst.FTrip.OBJECTID] = objectId
        object.fetchInBackground(block: { (error) in
            if error == nil{
                var members = object.dictionary[FConst.FTrip.MEMBERS] as! [String]
                if let index = members.index(of:memberId){
                    members.remove(at: index)
                }
                object.dictionary[FConst.FTrip.MEMBERS] = members
                object.saveInBackground(block: { (error) in
                    if error == nil{
                        Recent.updateMembers(object)
                    }
                    complete(error)
                })
            }
            complete(error)
        })
    }
   

    class func achieveItem(_ tripId: String) {
        let object: FObject = FObject.object(path: FConst.FTrip.PATH)
        object.dictionary[FConst.FTrip.OBJECTID] = tripId
        object.dictionary[FConst.FTrip.ISACHIEVED] = true
        object.updateInBackground()
    }
    
    class func deleteItem(_ tripId: String) {
        let object: FObject = FObject.object(path: FConst.FTrip.PATH)
        object.dictionary[FConst.FTrip.OBJECTID] = tripId
        object.dictionary[FConst.FTrip.ISDELETED] = true
        object.updateInBackground()
    }
   
}
