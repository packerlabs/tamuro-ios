//
//  Message.swift
//  Tamuro
//
//  Created by Mobile on 10/31/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
class Message: NSObject {
    
    static func getObjectfromId(objectId:String) ->DBMessage?{
        let predicate = NSPredicate(format: "objectId == %@", objectId)
        let dbuser = DBMessage.objects(with: predicate).firstObject() as? DBMessage
        return dbuser
    }
    
    class func updateStatus(_ groupId: String, messageId: String) {
        let object: FObject = FObject.object(path: FConst.FMessage.PATH, subpath: groupId)
        object.dictionary[FConst.FMessage.OBJECTID] = messageId
        object.dictionary[FConst.FMessage.STATUS] = Constant.Chat.TEXT_READ
        object.updateInBackground()
    }
    
    class func deleteItem(_ groupId: String, messageId: String) {
        let object: FObject = FObject.object(path: FConst.FMessage.PATH, subpath: groupId)
        object.dictionary[FConst.FMessage.OBJECTID] = messageId
        object.dictionary[FConst.FMessage.ISDELETED] = true
        object.updateInBackground()
    }
    
    class func deleteItem(_ dbmessage: DBMessage) {
        if (dbmessage.status == Constant.Chat.TEXT_QUEUED) {
            let realm = RLMRealm.default()
            realm.beginWriteTransaction()
            realm.delete(dbmessage)
            do{
                try realm.commitWriteTransaction()
                NotificationCenter.default.post(name: Notification.Name(rawValue: Constant.NotifiKey.REFRESH_MESSAGES1), object: nil)
            }catch let error as NSError{
                print(error.description)
            }
        } else {
            self.deleteItem(dbmessage.groupId, messageId: dbmessage.objectId)
        }
    }
}
