
#import <Foundation/Foundation.h>

@interface NotificationCenter : NSObject

+ (void)addObserver:(id)target selector:(SEL)selector name:(NSString *)name;

+ (void)removeObserver:(id)target;

+ (void)post:(NSString *)notification;
+ (void)post:(NSString *)notification object:(id)object;
+ (void)post:(NSString *)notification afterDelay:(NSTimeInterval)delay;

@end

