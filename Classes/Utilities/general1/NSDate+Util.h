

#import <Foundation/Foundation.h>

@interface NSDate (Util)

- (long long)timestamp;

+ (NSDate *)dateWithTimestamp:(long long)timestamp;

+ (NSDate *)dateWithNumberTimestamp:(NSNumber *)number;

@end

