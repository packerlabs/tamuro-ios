
#import "NSDate+Util.h"

@implementation NSDate (Util)

- (long long)timestamp
{
	return [self timeIntervalSince1970] * 1000;
}

+ (NSDate *)dateWithTimestamp:(long long)timestamp
{
	NSTimeInterval interval = (NSTimeInterval) timestamp / 1000;
	return [NSDate dateWithTimeIntervalSince1970:interval];
}
+ (NSDate *)dateWithNumberTimestamp:(NSNumber *)number
{
	return [self dateWithTimestamp:[number longLongValue]];
}

@end

