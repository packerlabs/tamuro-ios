

#import "NotificationCenter.h"

@implementation NotificationCenter


+ (void)addObserver:(id)target selector:(SEL)selector name:(NSString *)name
{
	[[NSNotificationCenter defaultCenter] addObserver:target selector:selector name:name object:nil];
}


+ (void)removeObserver:(id)target

{
	[[NSNotificationCenter defaultCenter] removeObserver:target];
}


+ (void)post:(NSString *)notification

{
	[[NSNotificationCenter defaultCenter] postNotificationName:notification object:nil];
}
+ (void)post:(NSString *)notification object:(id)object

{
    [[NSNotificationCenter defaultCenter] postNotificationName:notification object:object];
}

+ (void)post:(NSString *)notification afterDelay:(NSTimeInterval)delay

{
	dispatch_time_t time = dispatch_time(DISPATCH_TIME_NOW, delay * NSEC_PER_SEC);
	dispatch_after(time, dispatch_get_main_queue(), ^(void){ [self post:notification]; });
}

@end

