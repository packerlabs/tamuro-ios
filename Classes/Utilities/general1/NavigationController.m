

#import "NavigationController.h"

@implementation NavigationController
- (void)viewDidLoad
{
	[super viewDidLoad];
	self.navigationBar.translucent = NO;
	self.navigationBar.barTintColor = COLOR_HOME_BG;
	self.navigationBar.tintColor = [UIColor darkGrayColor];
	self.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName:[UIColor blackColor]}; 
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
	return UIStatusBarStyleLightContent;
}
@end

