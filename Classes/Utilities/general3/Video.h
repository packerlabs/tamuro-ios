

#import <Foundation/Foundation.h>
 #import <UIKit/UIKit.h>

@interface Video : NSObject


+ (UIImage *)thumbnail:(NSString *)path;

+ (NSNumber *)duration:(NSString *)path;

@end

