

#import <Foundation/Foundation.h>
 

@interface File : NSObject


+ (NSString *)temp:(NSString *)ext;

+ (BOOL)exist:(NSString *)path;

+ (BOOL)remove:(NSString *)path;

+ (void)copy:(NSString *)src dest:(NSString *)dest overwrite:(BOOL)overwrite;

+ (NSDate *)created:(NSString *)path;

+ (NSDate *)modified:(NSString *)path;

+ (long long)size:(NSString *)path;

+ (long long)diskFree;

@end

