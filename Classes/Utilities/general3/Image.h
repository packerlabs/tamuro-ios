

#import <Foundation/Foundation.h>
 #import <UIKit/UIKit.h>

@interface Image : NSObject

+ (UIImage *)square:(UIImage *)image size:(CGFloat)size;

+ (UIImage *)resize:(UIImage *)image width:(CGFloat)width height:(CGFloat)height scale:(CGFloat)scale;

@end

