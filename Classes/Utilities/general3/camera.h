

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

BOOL			PresentPhotoCamera		(id target, BOOL canEdit);
BOOL			PresentVideoCamera		(id target, BOOL canEdit);
BOOL			PresentMultiCamera		(id target, BOOL canEdit);


BOOL			PresentPhotoLibrary		(id target, BOOL canEdit);
BOOL			PresentVideoLibrary		(id target, BOOL canEdit);

