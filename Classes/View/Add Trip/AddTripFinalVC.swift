//
//  AddTripFinalVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import ProgressHUD
import Firebase

class AddTripFinalVC: AddTripBaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()
        createTrip()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.createTrip()
        })
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionFacebook(_ sender: Any) {
    }
    @IBAction func actionTwitter(_ sender: Any) {
    }
    @IBAction func actionWhatsapp(_ sender: Any) {
    }
    
    
    func createTrip()
    {        
        trip?.dictionary[FConst.FTrip.ADDRESS] = ""
        trip?.dictionary[FConst.FTrip.ISACHIEVED] = false
        trip?.dictionary[FConst.FTrip.ISDELETED] = false
        trip?.dictionary[FConst.FTrip.ISPAID] = false
        trip?.dictionary[FConst.FTrip.ISPRIVATE] = false
        trip?.dictionary[FConst.FTrip.LATITUDE] = 0.0
        trip?.dictionary[FConst.FTrip.LONGITUDE] = 0.0  
        trip?.dictionary[FConst.FTrip.POPULARITY] = 0
        trip?.dictionary[FConst.FTrip.TRIPID] = ""
        trip?.dictionary[FConst.FTrip.RAISED] = 0
        trip?.dictionary[FConst.FTrip.MEMBERS] = [CurrentUser.shared.objectId]
        if (trip?.dictionary[FConst.FTrip.PAYMENTTYPE]) == nil {
            trip?.dictionary[FConst.FTrip.PAYMENTTYPE] = "paypal"
        }
        trip?.dictionary[FConst.FTrip.USERID] = CurrentUser.shared.objectId
        
        print("New Trip :%@",trip?.dictionary as Any)
        
        ProgressHUD.show("Creating...")
        self.uploadImage(image: trip?.dictionary[FConst.FTrip.PICTURE] as! UIImage, completion: { (pictureUrl, error) in
            if error == nil{
                self.trip?.dictionary[FConst.FTrip.PICTURE] = pictureUrl
                print("New Trip :%@",self.trip?.dictionary as Any)
                self.trip?.saveInBackground(block: { (error) in
                    if error == nil{
                        ProgressHUD.showSuccess("Created Successfully!")
                        CurrentUser.shared.trips.append(self.trip?.dictionary[FConst.FTrip.OBJECTID] as! String)
                        CurrentUser.saveUserInfoWithKey(key: FConst.FUser.TRIPS, value: CurrentUser.shared.trips)
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
//                            NotificationCenter.default.post(name: NSNotification.Name.CustomKeys.needToCloseTripPage, object: nil, userInfo: nil)
//                        })
                    }
                })
            }else{
                self.trip?.dictionary[FConst.FTrip.PICTURE] = ""
            }
        })
    }

    func uploadImage(image: UIImage, completion : @escaping ((String?, Error?) -> Void)){
        
        var pictureUrl: String? = ""
     
        let imagePicture : UIImage = Image.square(image, size: 140)
        let dataPicture : Data = imagePicture.jpegData(compressionQuality: 0.6)!       
        
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        
        let reference1 = Storage.storage().reference()
   
        let mediaPath1 = Global.filename(folderName: FConst.TRIP_STORAGE, type: nil, ext: "jpg")

        
        reference1.child(mediaPath1).putData(dataPicture, metadata: metaData, completion: { (metadata1, error) in
            if error == nil{
                Storage.storage().reference(withPath: mediaPath1).downloadURL(completion: { (downloadUrl1, error) in
                    if error == nil{
                        pictureUrl = downloadUrl1!.absoluteString
                        completion(pictureUrl,nil)
                    }else{
                        completion(nil,error)
                    }
                })
            }else{
                completion(nil,error)
            }
        })
    }
}
