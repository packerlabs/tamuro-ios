//
//  AddAboutVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class AddAboutVC: AddTripBaseVC {

    @IBOutlet weak var txtAbout: UITextView!
    @IBOutlet weak var lblWordCount: UILabel!
    @IBOutlet weak var btnPerson: UIButton!
    
    private let countWords = 80
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUI()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goNext", let vc = segue.destination as? AddTripBaseVC {
            trip?.dictionary[FConst.FTrip.ABOUT] = txtAbout.text
            print("New Trip :%@",trip?.dictionary as Any)
            vc.trip = self.trip
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func initUI() {
        txtAbout.updateSpacing(spacing: 30)
        btnPerson.setRadius(radius: 4)
    }

    @IBAction func actionPerson(_ sender: Any) {
    }
    
    

}
extension AddAboutVC : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        txtAbout.updateSpacing(spacing: 30)
        btnNext.isEnabled = txtAbout.text.count > 0
        lblWordCount.text = "\(80 - textView.text.words.count) words"
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let str = (textView.text as NSString).replacingCharacters(in: range, with: text)
        if str.words.count > countWords {
            return false
        }
        return true
    }
}
