//
//  AddPeopleMoneyVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class AddPeopleMoneyVC: AddTripBaseVC {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var btnPay: UIButton!
    
    var selectedIndex = -1
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUIs()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goNext", let vc = segue.destination as? AddTripBaseVC {
            trip?.dictionary[FConst.FTrip.COST] = 400
            print("New Trip :%@",trip?.dictionary as Any)
            vc.trip = self.trip
        }
    }
    func initUIs() {
        btnPay.setRadius(radius: btnPay.frame.size.height / 2)
        btnPay.setBorder(width: 1, color: UIColor(white: 74.0 / 255.0, alpha: 0.6))
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionPay(_ sender: Any) {
        performSegue(withIdentifier: "goSelectCard", sender: nil)
    }
}
extension AddPeopleMoneyVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let lblTitle = cell.viewWithTag(1) as? UILabel {
            lblTitle.text = "\(indexPath.row + 2) Person"
            if indexPath.row == selectedIndex {
                lblTitle.textColor = .white
            } else {
                lblTitle.textColor = UIColor(red: 66.0 / 255.0, green: 67.0 / 255.0, blue: 106.0 / 255.0, alpha: 1.0)
            }
        }
        if indexPath.row == selectedIndex {
            cell.backgroundColor = UIColor(red: 1.0, green: 41.0 / 255.0, blue: 103.0 / 255.0, alpha: 1.0)
        } else {
            cell.backgroundColor = .white
        }
        cell.setRadius(radius: 4)
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        selectedIndex = indexPath.row
        btnNext.isEnabled = true
        trip?.dictionary[FConst.FTrip.MAXMEMBERNUM] = indexPath.row + 2
        collectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width / 2 - 10, height: 50)
    }
}
