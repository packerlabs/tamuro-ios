//
//  AddInformationVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import DateTimePicker

class AddInformationVC: AddTripBaseVC {

    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var btnDate: UIButton!
    @IBOutlet weak var txtDays: UITextField!
    @IBOutlet weak var txtNights: UITextField!
    @IBOutlet weak var btnPhoto: UIButton!
    private let formatter = DateFormatter()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        trip = FObject.object(path: FConst.FTrip.PATH)
        btnNext.isEnabled = false
        initUIs()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        constraintHeightMainView.constant = 486
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
            self.viewBackground.alpha = 0.56
        }
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//
//    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goNext", let vc = segue.destination as? AddTripBaseVC {
            let dateInt = Int(txtDays.text!)!
            trip?.dictionary[FConst.FTrip.TITLE] = txtTitle.text
            trip?.dictionary[FConst.FTrip.DAY] = Int(txtDays.text!)
            trip?.dictionary[FConst.FTrip.NIGHT] = Int(txtNights.text!)
            
            let startDate: Date! = formatter.date(from: btnDate.titleLabel!.text!)
            let startDateStemp = startDate.timestamp()
            let endDate: Date = Date(timeInterval: TimeInterval(dateInt*86400), since: startDate)
            let endDateStemp = endDate.timestamp()
            
            trip?.dictionary[FConst.FTrip.STARTDATE] = btnDate.titleLabel?.text
            trip?.dictionary[FConst.FTrip.STARTDATESTAMP] = startDateStemp
            trip?.dictionary[FConst.FTrip.ENDDATESTAMP] = endDateStemp
            
            vc.trip = self.trip
        }
    }
    func initUIs() {
        formatter.dateFormat = "dd MMMM yyyy"
        btnDate.setTitle(formatter.string(from: Date()), for: .normal)
//        btnNext.isEnabled = true
    }
    func checkValidationNext() -> Bool {
        if txtTitle.text?.count == 0 {
            return false
        }
        if txtDays.text?.count == 0 {
            return false
        }
        if txtNights.text?.count == 0 {
            return false
        }
        if trip?.dictionary[FConst.FTrip.PICTURE] == nil {
            return false
        }
        return true
    }
    @IBAction func actionDate(_ sender: Any) {
        view.endEditing(true)
        let picker = DateTimePicker.create(minimumDate: Date(), maximumDate: nil)
        let date: Date = formatter.date(from: btnDate.titleLabel!.text!)!
        picker.selectedDate = date
        picker.delegate = self
        picker.isDatePickerOnly = true
        picker.show()
    }
    @IBAction func actionPhoto(_ sender: Any) {
        let alert = UIAlertController(title: "Choose photo", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Open camera", style: .default, handler: { (action) in
            PresentPhotoCamera(self, true);
        }))
        alert.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action) in
            PresentPhotoLibrary(self, true);
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation    

}

extension AddInformationVC: DateTimePickerDelegate {
    func dateTimePicker(_ picker: DateTimePicker, didSelectDate: Date) {
        btnDate.setTitle(formatter.string(from: didSelectDate), for: .normal)
        btnNext.isEnabled = checkValidationNext()
    }
}

extension AddInformationVC : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if str.count == 0 {
            btnNext.isEnabled = false
        } else {
            btnNext.isEnabled = checkValidationNext()
        }
        return true
    }
}

extension AddInformationVC : UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image:UIImage = info[.editedImage] as? UIImage else{
            return
        }
        trip?.dictionary[FConst.FTrip.PICTURE] = image
        btnPhoto.setImage(nil, for: .normal)
        btnPhoto.setBackgroundImage((trip?.dictionary[FConst.FTrip.PICTURE]) as? UIImage, for: .normal)
        btnNext.isEnabled = checkValidationNext()
    }

}
