//
//  SelectCardVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class SelectCardVC: AddTripBaseVC {

    @IBOutlet weak var btnVisa: UIButton!
    @IBOutlet weak var btnPaypal: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUIs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func initUIs() {
        btnVisa.setRadius(radius: 4)
        btnPaypal.setRadius(radius: 4)
        btnNext.isEnabled = true
    }
    @IBAction func actionVisa(_ sender: Any) {
        performSegue(withIdentifier: "cardDetail", sender: "visa")
    }
    @IBAction func actionPaypal(_ sender: Any) {
        performSegue(withIdentifier: "cardDetail", sender: "paypal")
    }
    

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "cardDetail" {
            if let strSender = sender as? String, let vc = segue.destination as? AddTripBaseVC {
                if strSender == "visa" {
                    trip?.dictionary[FConst.FTrip.PAYMENTTYPE] = "visa"
                } else {
                    trip?.dictionary[FConst.FTrip.PAYMENTTYPE] = "paypal"
                }
                print("New Trip :%@",trip?.dictionary as Any)
                vc.trip = self.trip
            }
        }
    }
    

}
