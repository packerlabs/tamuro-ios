//
//  AddCardDetailsVC.swift
//  Tamuro
//
//  Created by Mobile on 9/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class AddCardDetailsVC: AddTripBaseVC {

    @IBOutlet weak var txtPrice: UITextField!
    @IBOutlet weak var txtCardNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func checkValidationNext() -> Bool {
        if txtPrice.text?.count == 0 {
            return false
        }
        if txtCardNumber.text?.count == 0 {
            return false
        }
        if txtPassword.text?.count == 0 {
            return false
        }
        return true
    }
    

}
extension AddCardDetailsVC: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if str.count == 0 {
            btnNext.isEnabled = false
        } else {
            btnNext.isEnabled = checkValidationNext()
        }
        return true
    }
}
