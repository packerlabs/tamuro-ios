//
//  AddItemVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import SDWebImage

class AddItemVC: AddTripBaseVC {

    @IBOutlet weak var btnAdd: UIButton!
    @IBOutlet weak var txtSearch: UITextField!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var lblItems: UILabel!
    
    var isSearch  = false
    var dbtripcontents: RLMResults<DBTripcontent>?
    var arrayItems = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initUI()
        // Do any additional setup after loading the view.
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if segue.identifier == "goNext", let vc = segue.destination as? AddTripBaseVC {
            trip?.dictionary[FConst.FTrip.TRIPCONTENTS] = arrayItems
            print("New Trip :%@",trip?.dictionary as Any)
            vc.trip = self.trip
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func initUI() {
        btnAdd.setRadius(radius: 4)
        lblItems.text = "No Items Added"
    }
    func loadTripContents(strSearch: String) {
//        let strSearch: String = txtSearch.text!
        let predicate = NSPredicate(format: "name CONTAINS[c] %@ AND isDeleted == NO", strSearch)
        dbtripcontents = DBTripcontent.objects(with: predicate).sortedResults(usingKeyPath: FConst.FMessage.CREATEDAT, ascending: true) as? RLMResults<DBTripcontent>
        print("Searched Trip Count: %d", dbtripcontents?.count ?? 0)
//        if strSearch.count == 0 {
            collectionView.reloadData()
//            return
//        }
    }
    func updateUIs() {
        if Int(dbtripcontents!.count) == 0 {
            lblItems.text = "No Items Added"
        } else if Int(dbtripcontents!.count)  == 1 {
            lblItems.text = "1 Item Added"
        } else {
            lblItems.text = "\(Int(dbtripcontents!.count)) Items Added"
        }
        btnNext.isEnabled = Int(dbtripcontents!.count) > 0
    }
    @IBAction func actionAdd(_ sender: Any) {
        if isSearch, Int(dbtripcontents!.count) > 0 {
            arrayItems.append(dbtripcontents!.firstObject()!.objectId)
            updateUIs()
            isSearch = false
            collectionView.reloadData()
        }
    }  
}
extension AddItemVC : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        isSearch = true
        collectionView.reloadData()
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        isSearch = false
        txtSearch.text = ""
        collectionView.reloadData()
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        loadTripContents(strSearch: str)
        return true
    }
}
extension AddItemVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if isSearch{
            return Int(dbtripcontents?.count ?? 0)
        }
        return arrayItems.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var strCell = "cell"
        var dbtripcontent : DBTripcontent?
        if isSearch {
            strCell = "cell_list"
            dbtripcontent = dbtripcontents![UInt(indexPath.row)]
        } else {
            let objectId = arrayItems[indexPath.row]
            dbtripcontent = TripContents.getObjectfromId(objectId: objectId)
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: strCell, for: indexPath)
        cell.setRadius(radius: 4)
        if let imgView = cell.viewWithTag(1) as? UIImageView {
            imgView.sd_setImage(with: URL(string: (dbtripcontent?.picture)!), placeholderImage: nil)
        }
        if let lblTitle = cell.viewWithTag(2) as? UILabel {
            lblTitle.text = dbtripcontent?.name
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        if isSearch, Int(dbtripcontents!.count) > indexPath.row {
            let dbtripcontent = dbtripcontents![UInt(indexPath.row)]
            arrayItems.append(dbtripcontent.objectId)
            updateUIs()
            txtSearch.text = ""
            txtSearch.resignFirstResponder()
            isSearch = false
            collectionView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if isSearch {
            return CGSize(width: collectionView.frame.size.width, height: 48)
        }
        return CGSize(width: collectionView.frame.size.width / 4 - 10, height: 80)
    }
}
