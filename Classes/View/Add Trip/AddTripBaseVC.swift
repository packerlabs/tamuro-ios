//
//  AddTripBaseVC.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class AddTripBaseVC: UIViewController {

    @IBOutlet weak var viewBackground: UIView!
    @IBOutlet weak var viewProgressBack: UIView!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var viewMain: UIView!
    @IBOutlet weak var constraintHeightMainView: NSLayoutConstraint! // 486
    @IBOutlet weak var btnNext: UIButton!
    
    var trip : FObject?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewMain.setRadius(radius: 22)
        viewMain.dropShadowEffectOnly()
        if viewProgressBack != nil {
            viewProgressBack.setRadius(radius: viewProgressBack.frame.size.height / 2)
        }
        if viewProgress != nil {
            viewProgress.setRadius(radius: viewProgressBack.frame.size.height / 2)
        }
        // Do any additional setup after loading the view.
        
        
//        if btnNext != nil {
//            btnNext.isEnabled = true
//        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(needToClose), name: NSNotification.Name.CustomKeys.needToCloseTripPage, object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.CustomKeys.needToCloseTripPage, object: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func needToClose() {
        constraintHeightMainView.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.viewBackground.alpha = 0
        }) { (done) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.count > 0 {
            let touch = touches.first
            if (touch?.view?.isEqual(view))! || (touch?.view?.isEqual(viewBackground))! {
                NotificationCenter.default.post(name: NSNotification.Name.CustomKeys.needToRotateTrip, object: nil, userInfo: nil)
                needToClose()
            }
        }
    }
    @IBAction func actionNext(_ sender: Any) {
        performSegue(withIdentifier: "goNext", sender: nil)
    }
    
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destinationViewController.
//        // Pass the selected object to the new view controller.
//        if segue.identifier == "goNext", let vc = segue.destination as? AddTripBaseVC {
//            vc.trip = self.trip
//        }
//    }
//    

}
