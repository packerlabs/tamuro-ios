//
//  EmailVC.swift
//  Tamuro
//
//  Created by Mobile on 11/20/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseStorage
import ProgressHUD
import ACFloatingTextfield_Swift

class EmailVC: UIViewController {

    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.txtEmail.errorText = "Please input valid email."
        self.txtPassword.errorText = "Password should not less than 6 characters."
        self.txtPassword.isSecureTextEntry = true
    }
    
    
    @IBAction func actionSignIn(_ sender: Any) {
        let email = self.txtEmail.text!
        let password = self.txtPassword.text!

        if Global.validEmail(email: email) == false {
            self.txtEmail.showError()
            return;
        }
        if password.count < 6 {
            self.txtPassword.showError()
            return
        }
        ProgressHUD.show("Login...", interaction: false)
        FUser.signIn(email: email, password: password) { (fuser, error) in
            UI {
                if error == nil{
                    if fuser != nil{
                        let loading = Loading.shared
                        loading.fromVC = self
                        loading.initialize()
                    }else{
                        ProgressHUD.dismiss()
                    }
                }else{
                    ProgressHUD.showError(error!.localizedDescription, interaction: false)
                }
            }
        }
    }
    @IBAction func actionClose(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }

}
