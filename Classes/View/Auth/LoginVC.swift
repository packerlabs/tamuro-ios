//
//  LoginVC.swift
//  Tamuro
//
//  Created by Mobile on 9/6/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD
class LoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    //MARK:- Actions

    @IBAction func actionFacebooklogin(_ sender: Any) {
        FUser.signInWithFacebook(viewController: self) { (fuser, error) in
            UI {
                if error == nil{
                    if fuser != nil{
                        let loading = Loading.shared
                        loading.fromVC = self
                        loading.initialize()
                    }else{
                        ProgressHUD.dismiss()
                    }
                }else{
                    ProgressHUD.showError(error!.localizedDescription, interaction: false)
                }
            }
        }
    }
}
