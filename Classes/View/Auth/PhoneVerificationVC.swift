//
//  PhoneVerificationVC.swift
//  Tamuro
//
//  Created by Mobile on 9/7/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseAuth
import ProgressHUD
class PhoneVerificationVC: UIViewController {

    @IBOutlet weak var lblverification: UILabel!
    @IBOutlet weak var btnCode: UIButton!
    @IBOutlet weak var btnResend: UIButton!
    @IBOutlet weak var lblSMS: UILabel!
    @IBOutlet weak var viewPhoneCode: UIView!
    @IBOutlet weak var txtPhoneNum: UITextField!
    @IBOutlet weak var btnBack: UIButton!
    
    var phoneNumber = ""
    var verificationId: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUIs()
    }

    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    func configureUIs(){
        viewPhoneCode.dropShadowEffect()
        btnBack.dropShadowEffect()
    }
    
    // MARK: - Actions
    @IBAction func actionResend(_ sender: Any) {
        ProgressHUD.show()
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationId, error) in
            UI {
                ProgressHUD.dismiss()
                if error != nil{
                    ProgressHUD.showError(error?.localizedDescription)
                    return
                }
                self.verificationId = verificationId
                ProgressHUD.showSuccess("Phone verification code sent successfully!")
            }
        }
    }
    
    @IBAction func actionNext(_ sender: Any) {
        if (txtPhoneNum.text?.count)! != 6 {
            ProgressHUD.showError("Please enter the valid verify code.")
            return
        }
        ProgressHUD.show()
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: phoneNumber, verificationCode: txtPhoneNum.text!)
        Auth.auth().signInAndRetrieveData(with: credential) { (authData, error) in
            UI {
                ProgressHUD.dismiss()
                if let error = error {
                    ProgressHUD.showError(error.localizedDescription)
                    return
                }
                let loading = Loading.shared
                loading.fromVC = self
                loading.initialize()
            }
        }
    }
    @IBAction func actionPre(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    func sendCode(verificationCode:String){
        
    }
}
