//
//  RegisterVC.swift
//  Tamuro
//
//  Created by Mobile on 9/6/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseStorage
import ProgressHUD
import ACFloatingTextfield_Swift
import APAvatarImageView
class RegisterVC: UIViewController{
    @IBOutlet weak var ivUser: APAvatarImageView!
    @IBOutlet weak var txtUserName: ACFloatingTextfield!
    @IBOutlet weak var txtEmail: ACFloatingTextfield!
    @IBOutlet weak var txtPassword: ACFloatingTextfield!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtUserName.errorText = "Please input valid name."
        self.txtEmail.errorText = "Please input valid email."
        self.txtPassword.errorText = "Password should not less than 6 characters."
        self.txtPassword.isSecureTextEntry = true
    }
    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    // MARK: - Action

    @IBAction func actionCamera(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle:UIAlertController.Style.actionSheet)

        let action1 = UIAlertAction.init(title: "Open camera", style: UIAlertAction.Style.default) { (action) in
            PresentPhotoCamera(self, true);
        }
        let action2 = UIAlertAction.init(title: "Photo library", style: UIAlertAction.Style.default) { (action) in
            PresentPhotoLibrary(self, true);
        }
        let action3 = UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.default) { (action) in
            
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func actionClose(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionUpdateProfile(_ sender: Any) {
        
        let fullname = self.txtUserName.text!
        let email = self.txtEmail.text!
        let password = self.txtPassword.text!
        if (fullname.count < 1) {
            self.txtUserName.showError()
            return;
        }
        if Global.validEmail(email: email) == false {
            self.txtEmail.showError()
            return;
        }
        if password.count < 6 {
            self.txtPassword.showError()
            return
        }
        ProgressHUD.show("Creating new account...", interaction: false)
        CurrentUser.shared.registerModel = [:]
        let names : [String] = fullname.components(separatedBy:" ")
        if names.count > 1 {
            CurrentUser.shared.registerModel![FConst.FUser.FIRSTNAME] = names[0]
            CurrentUser.shared.registerModel![FConst.FUser.LASTNAME] = names[1]
        }else{
            CurrentUser.shared.registerModel![FConst.FUser.FIRSTNAME] = names[0]
            CurrentUser.shared.registerModel![FConst.FUser.LASTNAME] = ""
        }
        
        CurrentUser.shared.registerModel![FConst.FUser.FULLNAME] = fullname
        CurrentUser.shared.registerModel![FConst.FUser.EMAIL] = email
        
        ProgressHUD.show("Sign Up...")
        FUser.createUser(email: email, password: password) { (fuser, error) in
            if error == nil{
                    let loading = Loading.shared
                    loading.fromVC = self
                    loading.initialize()                
            }else{
                ProgressHUD.showError(error?.localizedDescription, interaction: false)
            }
        }
    }
}
extension RegisterVC : UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image:UIImage = info[.editedImage] as? UIImage else{
            return
        }
        ivUser.image = image
        CurrentUser.shared.userImage = image
        
    }
}
