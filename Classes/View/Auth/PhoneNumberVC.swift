//
//  PhoneNumberVC.swift
//  Tamuro
//
//  Created by Mobile on 9/11/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import PhoneNumberKit
import FirebaseAuth
import ProgressHUD

class PhoneNumberVC: UIViewController {
    @IBOutlet weak var btnPhoneNum: UIButton!
    @IBOutlet weak var viewPhoneNum: UIView!
    @IBOutlet weak var txtPhoneNum: PhoneNumberTextField!
    @IBOutlet weak var btnBack: UIButton!
    
    private var verificationId : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        configureUIs()
        Auth.auth().settings?.isAppVerificationDisabledForTesting = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func configureUIs(){
        viewPhoneNum.dropShadowEffect()
        btnBack.dropShadowEffect()
    }

    
    @IBAction func actionBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender: Any) {
        if (txtPhoneNum.text?.count)! < 8 {
            showErrorAlert(message: "Please enter the valid phone number")
            return
        }
//        ProgressHUD.show()
        PhoneAuthProvider.provider().verifyPhoneNumber("+\((txtPhoneNum.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())!)", uiDelegate: nil) { (verificationId, error) in
            UI {
//                ProgressHUD.dismiss()
                if error != nil{
                    ProgressHUD.showError(error?.localizedDescription)
                    return
                }
                self.verificationId = verificationId
                self.performSegue(withIdentifier: "segueCode", sender: nil)
            }
        }
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueCode" {
            if let vc = segue.destination as? PhoneVerificationVC {
                vc.verificationId = verificationId
                vc.phoneNumber = (txtPhoneNum.text?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined())!
            }
        }
    }
    

}
