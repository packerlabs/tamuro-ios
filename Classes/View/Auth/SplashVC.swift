//
//  SplashVC.swift
//  Tamuro
//
//  Created by Mobile on 9/6/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import ProgressHUD
class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ProgressHUD.show("Loading...", interaction: true)
        let loading = Loading.shared
        loading.fromVC = self
        loading.initialize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
