//
//  Loading.swift
//  WeddingProject
//
//  Created by Mobile on 8/31/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit
import ProgressHUD
import Realm

class Loading: NSObject {
    var fromVC: UIViewController?
    static let shared = Loading()
    func initialize() {
        print("Loading Intialize")
        let user: FUser = FUser.currentUser()!
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constant.NotifiKey.USER_LOGGED_IN), object: nil)
        user.fetchInBackground { (error) in
            if error != nil {
                ProgressHUD.showError(error?.localizedDescription)
            } else {
                self.updateOnesignalId()
                self.perform(#selector(self.goMainVC), with: nil, afterDelay: 0.0)
            }
        }
    }
    
    // MARK: - GOTOMAIN SCREEN
    @objc func goMainVC() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateInitialViewController()
        UIApplication.shared.keyWindow?.rootViewController = vc        
//        let navigationController = UINavigationController(rootViewController: vc!)
//        self.fromVC?.present(navigationController, animated: true, completion: nil)
    }
    func updateOnesignalId() {
        let user = FUser.currentUser()!
        let onesignalId = UserDefaults.standard.object(forKey: FConst.FUser.ONESIGNALID) as? String
        if onesignalId != nil {
            user.dictionary[FConst.FUser.ONESIGNALID] = onesignalId
            user.saveInBackground { (error) in
                if error != nil {
                    ProgressHUD.showError("Network error.")
                }
            }
        }
    }
}
