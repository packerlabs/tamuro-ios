//
//  ProfileVC.swift
//  Tamuro
//
//  Created by Mobile on 9/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import ProgressHUD
import SDWebImage

class ProfileVC: UIViewController {
    @IBOutlet weak var imgProfile: UIImageView!
    @IBOutlet weak var viewProfile: UIView!
    @IBOutlet weak var lblEmail: UILabel!
    
    @IBOutlet weak var txtName: UITextField!
    private var lblTitleNav : UILabel?
    private let arrayCellTitles = ["Offers", "Account Settings", "Payment Profile", "Help & Support", "Logout"]
    private let arrayCellImages = [#imageLiteral(resourceName: "offers"), #imageLiteral(resourceName: "account_settings"), #imageLiteral(resourceName: "payment_profile"), #imageLiteral(resourceName: "help_support"), #imageLiteral(resourceName: "logout")]
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUIs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func initUIs() {
        imgProfile.setRadius(radius: imgProfile.frame.width / 2)
        imgProfile.setBorder(width: 2, color: UIColor(white: 232.0 / 255.0, alpha: 1.0))
        viewProfile.setBorder(width: 1, color: UIColor(white: 231.0 / 255.0, alpha: 1.0))

        let (viewRightItem, lblTitleNav) = Global.getNavigationView(selector: #selector(actionCountry))
        self.lblTitleNav = lblTitleNav
        print(CurrentUser.shared.picture)
        imgProfile.sd_setImage(with: URL(string: CurrentUser.shared.picture),placeholderImage:nil, options: SDWebImageOptions.delayPlaceholder)
        
        txtName.isEnabled = false
        txtName.text = CurrentUser.shared.fullname
        txtName.inputAccessoryView = UIView()
        lblEmail.text = CurrentUser.shared.email
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: viewRightItem)

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    @IBAction func actionCamera(_ sender: Any) {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle:UIAlertController.Style.actionSheet)
        
        let action1 = UIAlertAction.init(title: "Open camera", style: UIAlertAction.Style.default) { (action) in
            PresentPhotoCamera(self, true);
        }
        let action2 = UIAlertAction.init(title: "Photo library", style: UIAlertAction.Style.default) { (action) in
            PresentPhotoLibrary(self, true);
        }
        let action3 = UIAlertAction.init(title: "Cancel", style: UIAlertAction.Style.default) { (action) in
            
        }
        alertController.addAction(action1)
        alertController.addAction(action2)
        alertController.addAction(action3)
        self.present(alertController, animated: true, completion: nil)
    }

    
    @IBAction func actionMenu(_ sender: Any) {
    }
    @IBAction func actionEdit(_ sender: Any) {
        txtName.isEnabled = true
        txtName.becomeFirstResponder()
    }
    @objc func actionCountry() {
        
    }
    

    func selectItemWithIndex(value: Int) {
        if let tabarController: UITabBarController = Global.shared.mainVC as? HomeTabbarVC, let allTripsVC = UIStoryboard.init(name: "AllTrip", bundle: nil).instantiateViewController(withIdentifier: "AllTripsVC") as? AllTripsVC{
            allTripsVC.isSaved = true
            tabarController.selectedIndex = value;            
        }
    }

}
extension ProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayCellTitles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? ProfileTableViewCell
        cell?.lblTitle.text = arrayCellTitles[indexPath.row]
        cell?.imgView.image = arrayCellImages[indexPath.row]
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        switch indexPath.row {
        case 0:
            break
        case 1:
            break
        case 2:
            break
        case 3:
            break
        case 4:
            let alert = UIAlertController(title: "", message: "Are you sure you want to logout?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                if FUser.logOut() {
                    let storyboard = UIStoryboard(name: "Auth", bundle: nil)
                    let vc = storyboard.instantiateInitialViewController()
                    UIApplication.shared.keyWindow?.rootViewController = vc
                    NotificationCenter.default.post(name: NSNotification.Name(Constant.NotifiKey.USER_LOGGED_OUT), object: nil)
                } else {
                    
                }
            }))
            alert.addAction(UIAlertAction(title: "No", style: .default, handler: { (action) in
            }))
            present(alert, animated: true, completion: nil)
            break      
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}

extension ProfileVC : UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let userName = textField.text!
        
        if userName.count < 1 {
            ProgressHUD.showError("Invalid Name!")
            txtName.isEnabled = true
            txtName.becomeFirstResponder()
            return false
        }
        
        var firstname = ""
        var lastname = ""
        
        var array: [String] = []
        let userName_ = userName.replacingOccurrences(of: "  ", with: " ")
        array = userName_.components(separatedBy: " ")
        firstname = array[0]
        if array.count > 1 {
            lastname = array[1]
        }
        
        let fuser: FUser! = FUser.currentUser()
        fuser.dictionary[FConst.FUser.FULLNAME] = userName_
        fuser.dictionary[FConst.FUser.FIRSTNAME] = firstname
        fuser.dictionary[FConst.FUser.LASTNAME] = lastname
        fuser.updateInBackground()
        
        txtName.isEnabled = false
        txtName.resignFirstResponder()
        
        return true
    }

}

extension ProfileVC : UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image:UIImage = info[.editedImage] as? UIImage else{
            return
        }
        imgProfile.image = image
        CurrentUser.shared.userImage = image
        
        DispatchQueue.main.async() {
            ProgressHUD.show("Uploading...")
            let user: FUser! = FUser.currentUser()
            self.uploadImage(image: image, completion: { (pictureUrl, thumbUrl, error) in
                CurrentUser.shared.userImage = nil

                if error == nil{
                    user.dictionary[FConst.FUser.PICTURE] = pictureUrl
                    user.dictionary[FConst.FUser.THUMBNAIL] = thumbUrl
                    user.saveInBackground()
                    ProgressHUD.showSuccess("Uploaded Successfully!")
                }else{
                    ProgressHUD.showError(error?.localizedDescription)
                }

            })
        }
    }

    func uploadImage(image: UIImage, completion : @escaping ((String?, String?, Error?) -> Void)){
        
        var pictureUrl: String? = ""
        var thumbUrl: String? = ""
        let imagePicture : UIImage = Image.square(image, size: 140)
        let imageThumbnail : UIImage = Image.square(image, size: 60)
        
        let dataPicture : Data = imagePicture.jpegData(compressionQuality: 0.6)!
        let dataThumbnail : Data = imageThumbnail.jpegData(compressionQuality: 0.6)!
        
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        
        let reference1 = Storage.storage().reference()
        let reference2 = Storage.storage().reference()
        let mediaPath1 = Global.filename(folderName: FConst.USERPIC_STORAGE, type: "profile_picture", ext: "jpg")
        let mediaPath2 = Global.filename(folderName: FConst.USERPIC_STORAGE, type: "profile_thumbnail", ext: "jpg")
        
        reference1.child(mediaPath1).putData(dataPicture, metadata: metaData, completion: { (metadata1, error) in
            if error == nil{
                reference2.child(mediaPath2).putData(dataThumbnail, metadata: metaData, completion: { (metadata2, error) in
                    if error == nil{
                        Storage.storage().reference(withPath: mediaPath1).downloadURL(completion: { (downloadUrl1, error) in
                            if error == nil{
                                pictureUrl = downloadUrl1!.absoluteString
                                Storage.storage().reference(withPath: mediaPath2).downloadURL(completion: { (downloadUrl2, error) in
                                    if error == nil{
                                        thumbUrl = downloadUrl2!.absoluteString
                                        completion(pictureUrl, thumbUrl,nil)
                                    }else{
                                        completion(nil,nil,error)
                                    }
                                })
                            }else{
                                completion(nil,nil,error)
                            }
                        })
                    }else{
                        completion(nil,nil,error)
                    }
                })
            }else{
                completion(nil,nil,error)
            }
        })
    }
}
