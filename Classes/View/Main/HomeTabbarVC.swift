//
//  HomeTabbarVC.swift
//  Tamuro
//
//  Created by Mobile on 9/11/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Firebase
import ProgressHUD
class HomeTabbarVC: UITabBarController {
    var btnAddTrip: UIButton?
    var imgTabbarback: UIImageView?
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        addTabbarBackground()
        addTripButton()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(needToRotate), name: NSNotification.Name.CustomKeys.needToRotateTrip, object: nil)
        Global.shared.mainVC = self
        if CurrentUser.shared.userImage != nil{
            updateUserProfileImage()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.CustomKeys.needToRotateTrip, object: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        var tabFrame = tabBar.frame
        tabFrame.size.height = 73
        tabFrame.origin.y = view.frame.size.height - 73
        tabBar.frame = tabFrame
    }
    @objc func needToRotate() {
        UIView.animate(withDuration: 0.25, animations: {
            self.btnAddTrip?.transform = CGAffineTransform(rotationAngle: 0)
        }) { (done) in
            self.btnAddTrip?.isSelected = false
        }
    }
    func addTabbarBackground() {
        let image: UIImage = UIImage.init(imageLiteralResourceName: "tabbarbackground2.png")
        let rect: CGRect = CGRect.init(x: 0, y: 0, width: view.frame.width, height: 80)
        
        UIGraphicsBeginImageContext( rect.size );
        image.draw(in: rect)
        let picture1: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        let imageData: NSData = picture1.pngData()! as NSData
        let img:UIImage = UIImage.init(data: imageData as Data)!
        tabBar.backgroundImage = img
        tabBar.layoutIfNeeded()
    }
    func addTripButton() {
        btnAddTrip = UIButton(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        btnAddTrip?.setImage(#imageLiteral(resourceName: "add_trip"), for: .normal)
        btnAddTrip?.addTarget(self, action: #selector(actionAddTrip(button:)), for: .touchUpInside)
        btnAddTrip?.center = CGPoint(x: view.frame.size.width / 2, y: view.frame.size.height - 40)
        view.addSubview(btnAddTrip!)
        view.layoutIfNeeded()
    }
    @objc func actionAddTrip(button: UIButton) {
        UIView.animate(withDuration: 0.25, animations: {
            if button.isSelected {
                button.transform = CGAffineTransform(rotationAngle: 0)
            } else {
                button.transform = CGAffineTransform(rotationAngle: CGFloat.pi * 0.25)
            }
        }) { (done) in
            button.isSelected = !button.isSelected
        }
        if button.isSelected {
            NotificationCenter.default.post(name: NSNotification.Name.CustomKeys.needToCloseTripPage, object: nil, userInfo: nil)
        } else {
            let storyboard = UIStoryboard(name: "AddTrip", bundle: nil)
            let vc = storyboard.instantiateInitialViewController()
            selectedViewController?.present(vc!, animated: false, completion: nil)
        }
    }
   
}

//  MARK: - Update UserProfile Image
extension HomeTabbarVC{
    
    func updateUserProfileImage(){
        let user: FUser = FUser.currentUser()!
        ProgressHUD.show("Loading...")
        if let imageUrl = CurrentUser.shared.userImage as? String{
            downloadImage(url: URL(string: imageUrl), completion: { (pictureUrl, thumbUrl, error) in
                ProgressHUD.dismiss()
                CurrentUser.shared.userImage = nil
                if error == nil{
                    user.dictionary[FConst.FUser.PICTURE] = pictureUrl
                    user.dictionary[FConst.FUser.THUMBNAIL] = thumbUrl
                }else{
                    user.dictionary[FConst.FUser.PICTURE] = ""
                    user.dictionary[FConst.FUser.THUMBNAIL] = ""
                }
                user.saveInBackground()
            })
        }else if let image = CurrentUser.shared.userImage as? UIImage{
            DispatchQueue.main.async() {
                self.uploadImage(image: image, completion: { (pictureUrl, thumbUrl, error) in
                    CurrentUser.shared.userImage = nil
                    ProgressHUD.dismiss()
                    if error == nil{
                        user.dictionary[FConst.FUser.PICTURE] = pictureUrl
                        user.dictionary[FConst.FUser.THUMBNAIL] = thumbUrl
                    }else{
                        user.dictionary[FConst.FUser.PICTURE] = ""
                        user.dictionary[FConst.FUser.THUMBNAIL] = ""
                    }
                    user.saveInBackground()
                })
            }
        }
    }
    
    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    
    func downloadImage(url: URL?, completion : @escaping (String?, String?, Error?) -> ()){
        print("Download Started")
        if url == nil {
            completion("", "", nil)
        }else{
            getData(from: url!) { data, response, error in
                guard let data = data, error == nil else { return }
                print(response?.suggestedFilename ?? url!.lastPathComponent)
                print("Download Finished")
                
                let image = UIImage(data: data)
                DispatchQueue.main.async() {
                    self.uploadImage(image: image!, completion: { (pictureUrl, thumbUrl, error) in
                        if error == nil{
                            completion(pictureUrl, thumbUrl, nil)
                        }else{
                            completion(nil, nil, error)
                        }
                    })
                }
                
            }
        }
    }
    func uploadImage(image:UIImage, completion : @escaping ((String?, String?, Error?) -> Void)){
        
        var pictureUrl: String? = ""
        var thumbUrl: String? = ""
        let imagePicture : UIImage = Image.square(image, size: 140)
        let imageThumbnail : UIImage = Image.square(image, size: 60)
        
        let dataPicture : Data = imagePicture.jpegData(compressionQuality: 0.6)!
        let dataThumbnail : Data = imageThumbnail.jpegData(compressionQuality: 0.6)!
        
        let metaData = StorageMetadata()
        metaData.contentType = "image/png"
        
        let reference1 = Storage.storage().reference()
        let reference2 = Storage.storage().reference()
        let mediaPath1 = Global.filename(folderName: FConst.USERPIC_STORAGE, type: "profile_picture", ext: "jpg")
        let mediaPath2 = Global.filename(folderName: FConst.USERPIC_STORAGE, type: "profile_thumbnail", ext: "jpg")
        
        reference1.child(mediaPath1).putData(dataPicture, metadata: metaData, completion: { (metadata1, error) in
            if error == nil{
                reference2.child(mediaPath2).putData(dataThumbnail, metadata: metaData, completion: { (metadata2, error) in
                    if error == nil{
                        Storage.storage().reference(withPath: mediaPath1).downloadURL(completion: { (downloadUrl1, error) in
                            if error == nil{
                                pictureUrl = downloadUrl1!.absoluteString
                                Storage.storage().reference(withPath: mediaPath2).downloadURL(completion: { (downloadUrl2, error) in
                                    if error == nil{
                                        thumbUrl = downloadUrl2!.absoluteString
                                        completion(pictureUrl, thumbUrl,nil)
                                    }else{
                                        completion(nil,nil,error)
                                    }
                                })
                            }else{
                                completion(nil,nil,error)
                            }
                        })
                    }else{
                        completion(nil,nil,error)
                    }
                })
            }else{
                completion(nil,nil,error)
            }
        })
    }
}
