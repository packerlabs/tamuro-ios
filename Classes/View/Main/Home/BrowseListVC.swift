//
//  BrowseListVC.swift
//  Tamuro
//
//  Created by Mobile on 9/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Realm
import ProgressHUD
class BrowseListVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var typeBrowseBy = BrowseBy.endDate
    var lblTitleNav : UILabel?
    var dbTrips: RLMResults<DBTrip>?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadTrips()
        initUIs()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadTrips() {
        let predicate = NSPredicate(format: "isDeleted == NO")
        switch typeBrowseBy {
            case BrowseBy.endDate:
                dbTrips = DBTrip.objects(with: predicate).sortedResults(usingKeyPath: FConst.FTrip.ENDDATESTAMP, ascending: true) as? RLMResults<DBTrip>
                break
            case BrowseBy.newest:
                dbTrips = DBTrip.objects(with: predicate).sortedResults(usingKeyPath: FConst.FTrip.CREATEDAT, ascending: true) as? RLMResults<DBTrip>
                break
            case BrowseBy.popularity:
                dbTrips = DBTrip.objects(with: predicate).sortedResults(usingKeyPath: FConst.FTrip.POPULARITY, ascending: false) as? RLMResults<DBTrip>
                break
            case BrowseBy.mostFunded:
                dbTrips = DBTrip.objects(with: predicate).sortedResults(usingKeyPath: FConst.FTrip.COST, ascending: false) as? RLMResults<DBTrip>
                break
            default:
                break
        }
        tableView.reloadData()
    }
    func initUIs() {
        title = typeBrowseBy.rawValue
        let (viewRightItem, lblTitleNav) = Global.getNavigationView(selector: #selector(actionCountry))
        self.lblTitleNav = lblTitleNav
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: viewRightItem)
    }
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func actionCountry() {
        
    }
}
extension BrowseListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(dbTrips?.count ?? 0)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TripListTableViewCell
        let dbtrip = dbTrips?[UInt(indexPath.row)]
        cell?.configureCell(dbtrip: dbtrip!)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        let dbtrip = dbTrips![UInt(indexPath.row)]
        if let vc = storyboard.instantiateViewController(withIdentifier: "TripDetailVC") as? TripDetailVC {
            vc.dbtrip = dbtrip
            presentWithPushAnimation(destinationVC: vc)
        }

    }
}
