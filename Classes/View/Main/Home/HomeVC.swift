//
//  MainVC.swift
//  Tamuro
//
//  Created by Mobile on 9/6/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import ProgressHUD
class HomeVC: UIViewController {
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var btnSearchTrip: UIButton!
    @IBOutlet weak var btnPrice: UIButton!
    @IBOutlet weak var btnEndDate: UIButton!
    @IBOutlet weak var btnPopularity: UIButton!
    @IBOutlet weak var btnNewest: UIButton!
    @IBOutlet weak var btnMostFunded: UIButton!
    @IBOutlet var btnStart: [UIButton]!
    var lblTitleNav : UILabel?
    @IBOutlet weak var viewHowitworks: UIView!
    @IBOutlet weak var lblHowItWorks: UILabel!
    @IBOutlet weak var btnHowItWorks: UIButton!
    @IBOutlet weak var btnHideHowitWorks: UIButton!
    
    
    @IBOutlet weak var constraintHeightHowItWorksView: NSLayoutConstraint! // 0 <-> 141
    
    override func viewDidLoad() {
        super.viewDidLoad()
       configureUIs()
    }
    override func viewWillAppear(_ animated: Bool) {
        viewHowitworks.layoutIfNeeded()
        constraintHeightHowItWorksView.constant = 0
    }

    func configureUIs(){
        btnSearchTrip.dropShadowEffect()
        btnPrice.dropShadowEffect()
        btnEndDate.setRadius(radius: btnEndDate.frame.size.height / 2.0)
        btnPopularity.setRadius(radius: btnPopularity.frame.size.height / 2.0)
        btnNewest.setRadius(radius: btnNewest.frame.size.height / 2.0)
        btnMostFunded.setRadius(radius: btnMostFunded.frame.size.height / 2.0)
        for theButton in btnStart.enumerated() {
            theButton.element.setRadius(radius: 3)
        }
        let (viewRightItem, lblTitleNav) = Global.getNavigationView(selector: #selector(actionCountry))
        self.lblTitleNav = lblTitleNav
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: viewRightItem)

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    @IBAction func actionSearchTrip(_ sender: Any) {
    }
    @IBAction func actionPrice(_ sender: Any) {
    }
    @IBAction func actionViewMore(_ sender: Any) {
        performSegue(withIdentifier: "goBrowseBy", sender: BrowseBy.recommended)
    }
    @IBAction func actionBrowseBy(_ sender: Any) {
        var type = BrowseBy.endDate
        switch (sender as! UIButton).tag {
        case 1:
            type = BrowseBy.endDate
            break
            
        case 2:
            type = BrowseBy.popularity
            break
            
        case 3:
            type = BrowseBy.newest
            break
            
        case 4:
            type = BrowseBy.mostFunded
            break
            
        default:
            break
        }
        performSegue(withIdentifier: "goBrowseBy", sender: type)
    }
    @IBAction func actionMenu(_ sender: Any) {
        
    }
    @objc func actionCountry() {
        
    }
    
    @IBAction func actionShowHowitworks(_ sender: Any) {
        constraintHeightHowItWorksView.constant = 141
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.lblHowItWorks.isHidden = false
            self.btnHideHowitWorks.isHidden = false
            self.btnHowItWorks.isHidden = true
        }) { (complete) in
            let bottomOffset: CGPoint = CGPoint.init(x: 0, y: self.scrollView.contentSize.height - self.scrollView.bounds.size.height + self.scrollView.contentInset.bottom)
            self.scrollView.setContentOffset(bottomOffset, animated: true)
        }
    }
    @IBAction func actionHideHowitworks(_ sender: Any) {

        constraintHeightHowItWorksView.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.view.layoutIfNeeded()
            self.lblHowItWorks.isHidden = true
            self.btnHideHowitWorks.isHidden = true
            self.btnHowItWorks.isHidden = false
        }) { (complete) in

        }
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "goBrowseBy", let type = sender as? BrowseBy, let vc = segue.destination as? BrowseListVC {
            vc.typeBrowseBy = type
        }
    }
}
