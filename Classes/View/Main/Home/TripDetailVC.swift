//
//  TripDetailVC.swift
//  Tamuro
//
//  Created by Mobile on 9/14/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import ProgressHUD

class TripDetailVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
//    @IBOutlet weak var headerView: UIView!
    
    @IBOutlet weak var imgViewTrip: UIImageView!
    @IBOutlet weak var lblAbout: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnSaved: UIButton!
    @IBOutlet weak var lblPrice: UILabel!
    
    @IBOutlet weak var lblStartDate: UILabel!
    @IBOutlet weak var lblPriceRised: UILabel!
    @IBOutlet weak var lblPriceGoal: UILabel!
    @IBOutlet weak var viewProgressBack: UIView!
    @IBOutlet weak var viewProgress: UIView!
    @IBOutlet weak var lblMaxMemberCount: UILabel!
    
    
    @IBOutlet weak var constraintWidthProgress: NSLayoutConstraint!
    @IBOutlet weak var constraintHeightPriceView: NSLayoutConstraint! // 84 <-> 0
    @IBOutlet weak var constraintHeightCollectionViewItems: NSLayoutConstraint! // 80 <-> 170
    @IBOutlet weak var lblNightDays: UILabel!
    @IBOutlet weak var collectionViewItems: UICollectionView!
    @IBOutlet weak var collectionViewPeoples: UICollectionView!
    
    var dbtrip = DBTrip()
    var dbtripContents : RLMResults<DBTripcontent>?
    var userIds = [String]()
    var maxMemberCount: Int = 0
    var memberCount: Int = 0
    
    let formatter = DateFormatter()
    let formatter1 = DateFormatter()
    var isOwner = false
    var isMember = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initialize()
        initUIs()
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func initialize(){
        formatter.dateFormat = "dd MMMM yyyy"
        formatter1.dateFormat = "dd MMM yyyy"
        if dbtrip.userId == CurrentUser.shared.objectId{
            isOwner = true
        }
        userIds = dbtrip.members.components(separatedBy: ",")

        if let index = userIds.index(of: dbtrip.userId){
            userIds.remove(at: index)
            userIds.insert(dbtrip.userId, at: 0)
        }
        if !isOwner, let index = userIds.index(of: CurrentUser.shared.objectId){
            isMember = true
            userIds.remove(at: index)
            userIds.insert(CurrentUser.shared.objectId, at: 1)
        }
        
        maxMemberCount = dbtrip.maxMemberNum
        memberCount = userIds.count
        
        let tripContents = dbtrip.tripContents.components(separatedBy: ",")
        let predicate = NSPredicate(format: "objectId IN %@", tripContents)
        dbtripContents = DBTripcontent.objects(with: predicate).sortedResults(usingKeyPath: FConst.FTripContent.NAME, ascending: true) as? RLMResults<DBTripcontent>
    }
    func initUIs() {
        lblPrice.setRadius(radius: 3)
        lblNightDays.setRadius(radius: 3)
        viewProgressBack.setRadius(radius: viewProgressBack.frame.size.height / 2)
        viewProgress.setRadius(radius: viewProgress.frame.size.height / 2)
        lblTitle.text = dbtrip.title
        lblPrice.text = "$\(dbtrip.cost)"
        imgViewTrip.sd_setImage(with: URL(string: dbtrip.picture), placeholderImage: nil, options: .refreshCached)
        lblAbout.text = dbtrip.about
        lblNightDays.text = "\(dbtrip.day)D, " + "\(dbtrip.night)N Trip"
        lblMaxMemberCount.text = "Max \(maxMemberCount) people"
        
        let startDate = formatter.date(from: dbtrip.startDate)
        let startDateStr = formatter1.string(from: startDate!)
        lblStartDate.text = startDateStr
        lblPriceRised.text = "$\(dbtrip.raised)"
        lblPriceGoal.text = "$\(dbtrip.cost)"
        
        if !isOwner {
            constraintHeightPriceView.constant = 0
        }
        
        btnSaved.isSelected = CurrentUser.shared.savedtrips.contains(dbtrip.objectId)
        collectionViewItems.reloadData()
        collectionViewPeoples.reloadData()
        
        if Int(dbtripContents?.count ?? 0) > 4{
            constraintHeightCollectionViewItems.constant = 170
        }
        
        if let headerView = tableView.tableHeaderView {
            print("tableHeaderView")
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame

            //Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
            }
        }
        view.layoutIfNeeded()
    }
    
    @IBAction func actionBack(_ sender: Any) {
        dismissWithPushAnimation()
    }
    
    @IBAction func actionSave(_ sender: Any) {
        if btnSaved.isSelected{
            if let index = CurrentUser.shared.savedtrips.index(of: dbtrip.objectId){
                CurrentUser.shared.savedtrips.remove(at: index)
            }
        }else{
            CurrentUser.shared.savedtrips.append(dbtrip.objectId);
        }
        CurrentUser.saveUserInfoWithKey(key: FConst.FUser.SAVEDTRIPS, value: CurrentUser.shared.savedtrips)
        btnSaved.isSelected = !btnSaved.isSelected
    }
    
    @IBAction func actionLetsGO(_ sender: Any) {
        
    }
    @objc func actionMore(){
        if isOwner {
            actionAddNewMember()
        }else{
            let alert = UIAlertController(title: "", message: "Are you sure you want to attend the trip?", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action) in
            }))
            alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action) in
                Trip.addTripMembers(objectId: self.dbtrip.objectId, memberIds: [CurrentUser.shared.objectId], complete: { (error) in
                    if error == nil{
                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                            ProgressHUD.showSuccess("Added you Successfully!")
                             self.updateUserTrips()
                            self.initialize()
                            self.initUIs()
                        })
                    }
                })
//                self.addNewMembers(memberIds: [CurrentUser.shared.objectId])
            }))
            present(alert, animated: true, completion: nil)
        }
    }
    
//    func addNewMembers(memberIds: [String]){
//        let object = FObject.objectWithPath(path: FConst.FTrip.PATH)
//        object.dictionary[FConst.FTrip.OBJECTID] = self.dbtrip.objectId
//
//        ProgressHUD.show("adding...")
//        object.fetchInBackground(block: { (error) in
//            if error == nil{
//                var members = object.dictionary[FConst.FTrip.MEMBERS] as! [String]
//                members += memberIds
//                object.dictionary[FConst.FTrip.MEMBERS] = members
//                object.saveInBackground(block: { (eror) in
//                    if error == nil{
//                        Chat.startGroup1(object)
//                        Recent.updateMembers(object)
//
//
//
//                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
//                            ProgressHUD.showSuccess("Added you Successfully!")
//                            self.initialize()
//                            self.initUIs()
//                        })
//                    }
//                })
//            }
//        })
//    }


    func actionAddNewMember(){
        let vc: SelectMultipleView = SelectMultipleView()
        let navigationcontroller = NavigationController.init(rootViewController: vc)
        vc.delegate = self
        vc.userIds = self.userIds
        
        present(navigationcontroller, animated: true, completion: {

        })
    }
    
    func updateUserTrips(){
        CurrentUser.shared.trips.append(self.dbtrip.objectId)
        CurrentUser.saveUserInfoWithKey(key: FConst.FUser.TRIPS, value: CurrentUser.shared.trips)
    }
}

extension TripDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView.tag == 0{
            return Int(dbtripContents?.count ?? 0)
        }else{
            if memberCount < maxMemberCount{
                if isMember{
                    return memberCount
                }
                return memberCount + 1
            }else{
                return maxMemberCount
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView.tag == 0{
            let strCell = "cell"
            let dbtripcontent = dbtripContents![UInt(indexPath.row)]
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: strCell, for: indexPath)
            cell.setRadius(radius: 4)
            if let imgView = cell.viewWithTag(1) as? UIImageView {
                imgView.sd_setImage(with: URL(string: (dbtripcontent.picture)), placeholderImage: nil)
            }
            if let lblTitle = cell.viewWithTag(2) as? UILabel {
                lblTitle.text = dbtripcontent.name
            }
            return cell
            
        }
        else{
            var strCell = ""          
            if indexPath.row < memberCount{
                strCell = "cell"
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: strCell, for: indexPath)
                let imgView = cell.viewWithTag(1) as! UIImageView
                imgView.setRadius(radius: imgView.frame.width / 2)
                imgView.setBorder(width: 2, color: UIColor(white: 232.0 / 255.0, alpha: 1.0))
                if userIds[indexPath.row] == CurrentUser.shared.objectId{
                    imgView.sd_setImage(with: URL(string: (CurrentUser.shared.thumbnail)), placeholderImage: nil)
                }else{
                    let dbuser = Users.getObjectfromId(objectId: userIds[indexPath.row])!
                    imgView.sd_setImage(with: URL(string: (dbuser.thumbnail)), placeholderImage: nil)
                }
                return cell
            }else{
                strCell = "cell_more"
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: strCell, for: indexPath)
                if let btnMore = cell.viewWithTag(1) as? UIButton {
                    btnMore.addTarget(self, action: #selector(actionMore), for: .touchUpInside)
                }
                return cell
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 {
            return CGSize(width: collectionView.frame.size.width / 4 - 10, height: 80)
        }else{
            return CGSize(width: collectionView.frame.size.width / 5 - 20, height: 50)
        }
    }
}
extension TripDetailVC: SelectMultipleDelegate{
    func didSelectMultipleUsers(selects: [String]) {
        Trip.addTripMembers(objectId: dbtrip.objectId, memberIds: selects) { (error) in
            if error == nil{
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                    ProgressHUD.showSuccess("Added you Successfully!")
                     self.updateUserTrips()
                    self.initialize()
                    self.initUIs()
                })
            }
        }
//        addNewMembers(memberIds: selects)
    }
}
