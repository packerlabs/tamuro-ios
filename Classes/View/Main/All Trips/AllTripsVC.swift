//
//  AllTripsVC.swift
//  Tamuro
//
//  Created by Mobile on 9/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import Firebase

class AllTripsVC: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnUpcoming: UIButton!
    @IBOutlet weak var btnSaved: UIButton!
    var dbTrips: RLMResults<DBTrip>?
    var isSaved = false
    var lblTitleNav : UILabel?
     override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        initUIs()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func loadTrips(){
        var predicate = NSPredicate()
        if isSaved{
            predicate = NSPredicate(format: "isDeleted == NO AND objectId IN %@", CurrentUser.shared.savedtrips)
        }else{
            print("Firebase TimeStemp: %@",ServerValue.timestamp())
            print("Date TimeStemp: %ld",Date().timestamp())
            
//            predicate = NSPredicate(format: "isDeleted == NO AND startDateStamp > %ld AND objectId IN %@", Date().timestamp(), CurrentUser.shared.trips)
            predicate = NSPredicate(format: "isDeleted == NO AND objectId IN %@", CurrentUser.shared.trips)
        }
        dbTrips = DBTrip.objects(with: predicate).sortedResults(usingKeyPath: FConst.FTrip.ENDDATESTAMP, ascending: true) as? RLMResults<DBTrip>
    }
    func initUIs() {
        let (viewRightItem, lblTitleNav) = Global.getNavigationView(selector: #selector(actionCountry))
        self.lblTitleNav = lblTitleNav
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: viewRightItem)

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        updateUIs()
    }
    func updateUIs() {
        btnSaved.isSelected = isSaved
        btnUpcoming.isSelected = !isSaved
        loadTrips()
        tableView.reloadData()
    }
    @IBAction func actionUpcoming(_ sender: Any) {
        isSaved = false
        updateUIs()
    }
    @IBAction func actionSaved(_ sender: Any) {
        isSaved = true
        updateUIs()
    }
    @IBAction func actionMenu(_ sender: Any) {
        
    }
    @objc func actionCountry() {
        
    }
}
extension AllTripsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(dbTrips!.count)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? TripListTableViewCell
        let dbtrip = dbTrips?[UInt(indexPath.row)]
        cell?.configureCell(dbtrip: dbtrip!)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dbtrip = dbTrips![UInt(indexPath.row)]
        let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: "TripDetailVC") as? TripDetailVC {
            vc.dbtrip = dbtrip
            presentWithPushAnimation(destinationVC: vc)
        }
    }
}
