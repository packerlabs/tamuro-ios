

#import "AllMediaCell.h"
#import "DownloadManager.h"
#import "Image.h"
#import "Video.h"
@interface AllMediaCell()

@property (strong, nonatomic) IBOutlet UIImageView *imageItem;
@property (strong, nonatomic) IBOutlet UIImageView *imageVideo;

@end


@implementation AllMediaCell

@synthesize imageItem, imageVideo;


- (void)bindData:(DBMessage *)dbmessage

{
	imageItem.image = [UIImage imageNamed:@"allmedia_blank"];
	imageVideo.hidden = [dbmessage.type isEqualToString:MESSAGE_PICTURE];
	
	if ([dbmessage.type isEqualToString:MESSAGE_PICTURE])
	{
		[self bindPicture:dbmessage];
	}
	
	if ([dbmessage.type isEqualToString:MESSAGE_VIDEO])
	{
		[self bindVideo:dbmessage];
	}
}


- (void)bindPicture:(DBMessage *)dbmessage

{
	NSString *path = [DownloadManager pathImage:dbmessage.picture];
	
	if (path != nil)
	{
		UIImage *image = [[UIImage alloc] initWithContentsOfFile:path];
		imageItem.image = [Image square:image size:320];
	}
}


- (void)bindVideo:(DBMessage *)dbmessage

{
	NSString *path = [DownloadManager pathVideo:dbmessage.video];
	
	if (path != nil)
	{
		UIImage *image = [Video thumbnail:path];
		imageItem.image = [Image square:image size:320];
	}
}

@end

