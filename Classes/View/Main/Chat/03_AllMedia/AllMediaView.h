

#import "Tamuro-Swift.h"
#import <UIKit/UIKit.h>
#import "AppConstant.h"

@interface AllMediaView : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate>


- (id)initWith:(NSString *)groupId;

@end

