

//#import "Tamuro-Swift.h"

#import "StickersView.h"
#import <Realm/Realm.h>
//#import "Realm.h"
#import <JSQMessagesViewController/JSQMessages.h>
#import <RNGridMenu/RNGridMenu.h>
#import <IQAudioRecorderController/IQAudioRecorderViewController.h>
@interface ChatView : JSQMessagesViewController <UIGestureRecognizerDelegate, RNGridMenuDelegate, UIImagePickerControllerDelegate, IQAudioRecorderViewControllerDelegate, StickersDelegate>


- (id)initWith:(NSDictionary *)dictionary;
@property UIImageView *backgroundImage;
@end

