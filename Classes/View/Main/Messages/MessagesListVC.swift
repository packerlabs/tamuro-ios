//
//  MessagesListVC.swift
//  Tamuro
//
//  Created by Mobile on 9/13/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class MessagesListVC: UIViewController {
    var lblTitleNav : UILabel?
    var dbtrips : RLMResults<DBTrip>?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        initUIs()
        loadTrips()
    }
    func initUIs() {
        let (viewRightItem, lblTitleNav) = Global.getNavigationView(selector: #selector(actionCountry))
        self.lblTitleNav = lblTitleNav
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: viewRightItem)

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {super.didReceiveMemoryWarning()}
    
    func loadTrips(){
        let predicate = NSPredicate(format: "objectId IN %@", CurrentUser.shared.trips)
        dbtrips = DBTrip.objects(with: predicate) as? RLMResults<DBTrip>
    }
    
    @IBAction func actionMenu(_ sender: Any) {
    }
    
    @objc func actionCountry() {
        
    }
    
    func actionTripChat(dbtrip: DBTrip){
        let dictionary = Chat.startGroup2(dbtrip)
        let storyboard = UIStoryboard.init(name: "Messages", bundle: nil)
        let vc: ChatVC = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        let navigationcontroller = NavigationController(rootViewController: vc)
        vc.dictionary = dictionary
        vc.title = dbtrip.title
        present(navigationcontroller, animated: true, completion: nil)
    }
    
    func actionChat(){
        var objectId = ""
        if CurrentUser.shared.objectId == "Aru4KktZYqgVgIvc3scuMx22i5B2" {
            objectId = "PEQWaBun02R56e4L9oOaTz173hv1"
        }else{
            objectId = "Aru4KktZYqgVgIvc3scuMx22i5B2"
        }
        let dbuser : DBUser = Users.getObjectfromId(objectId: objectId)!
        let dictionary = Chat.startPrivate(dbuser)
        let storyboard = UIStoryboard.init(name: "Messages", bundle: nil)
        let vc : ChatVC = storyboard.instantiateViewController(withIdentifier: "ChatVC") as! ChatVC
        vc.dictionary = dictionary
        let navigationController = UINavigationController(rootViewController: vc)
  
//        presentWithPushAnimation(destinationVC: navigationController)
        Global.shared.mainVC!.presentWithPushAnimation(destinationVC: navigationController)
    }
}
extension MessagesListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Int(dbtrips?.count ?? 0)
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as? MessageListTableViewCell
        let dbtrip = dbtrips![UInt(indexPath.row)]
        cell?.configureCell(dbtrip: dbtrip)
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let dbtrip = dbtrips![UInt(indexPath.row)]
        actionTripChat(dbtrip: dbtrip)
//        self.actionChat()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
