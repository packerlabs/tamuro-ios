//
//  ChatVC.swift
//  Tamuro
//
//  Created by Mobile on 11/1/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import JSQMessagesViewController
import FirebaseDatabase
import Firebase
import Realm

class ChatVC: JSQMessagesViewController ,UIGestureRecognizerDelegate{

    var groupId = ""
    var members: [String] = []
    var type = ""
    var typingCounter: Int = 0
    var insertCounter: Int = 0
    var messages: Messages?
    var firebase: DatabaseReference?
    var dbmessages: RLMResults<DBMessage>?
    var jsqmessages: [String : Any] = [:]
    var avatarIds: [String] = []
    var avatars: [String : Any] = [:]
    var initials: [String : Any] = [:]
    var bubbleImageOutgoing: JSQMessagesBubbleImage?
    var bubbleImageIncoming: JSQMessagesBubbleImage?
    var otherUser: DBUser?
    var dictionary: [String : Any]?
    var backgroundImage: UIImageView?
    func initiWith(_ dictionary: [String : Any]) {
        groupId = dictionary["groupId"] as! String
        members = dictionary["members"] as! [String]
        type = dictionary["type"] as! String
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initiWith(dictionary!)
       
        inputToolbar.contentView.backgroundColor = Constant.COLOR_INCOMING
        inputToolbar.contentView.leftBarButtonItem.setImage(UIImage(named: "attach"), for: .normal)
        inputToolbar.contentView.textView.placeHolder = "Type your message here..."
        inputToolbar.contentView.rightBarButtonItem.setTitle("Send", for: .normal)
        inputToolbar.contentView.rightBarButtonItem.setTitleColor(UIColor.white, for: .normal)
        inputToolbar.contentView.leftBarButtonItem = nil
        NotificationCenter.default.addObserver(self, selector: #selector(actionCleanup), name: NSNotification.Name.CustomKeys.CLEANUP_CHATVIEW, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshCollectionView1), name: NSNotification.Name.CustomKeys.REFRESH_MESSAGES1, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(refreshCollectionView2), name: NSNotification.Name.CustomKeys.REFRESH_MESSAGES2, object: nil)

        insertCounter = Constant.Chat.INSERT_MESSAGES;

        jsqmessages = [String : Any]()

        avatarIds = [String]()
        avatars = [String : Any]()
        initials = [String : Any]()

        let bubbleFactory = JSQMessagesBubbleImageFactory()!
        bubbleImageOutgoing = bubbleFactory.outgoingMessagesBubbleImage(with: Constant.COLOR_OUTGOING)
        bubbleImageIncoming = bubbleFactory.incomingMessagesBubbleImage(with: Constant.COLOR_INCOMING)

        JSQMessagesCollectionViewCell.registerMenuAction(#selector(self.actionCopy(_:)))
        JSQMessagesCollectionViewCell.registerMenuAction(#selector(self.actionDelete(_:)))
        JSQMessagesCollectionViewCell.registerMenuAction(#selector(self.actionSave(_:)))

        let menuItemCopy = UIMenuItem(title: "Copy", action: #selector(self.actionCopy(_:)))
        let menuItemDelete = UIMenuItem(title: "Delete", action: #selector(self.actionDelete(_:)))
        let menuItemSave = UIMenuItem(title: "Save", action: #selector(self.actionSave(_:)))
        UIMenuController.shared.menuItems = [menuItemCopy, menuItemDelete, menuItemSave]

        Recent.clearCounter(groupId)
        messages = Messages(groupId)
//        firebase = Database.database().reference(withPath: FConst.FTYPING_PATH).child(groupId)
        firebase = Database.database().reference().child(FConst.FTYPING_PATH).child(groupId)
        loadMessages()
        createTypingObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController!.navigationBar.barTintColor = UIColor.white;
//        navigationItem.title = "Chat";
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "nav_back"), style: .plain, target: self, action: #selector(self.actionBack))
        navigationController!.navigationBar.tintColor = UIColor.darkGray
        let buttonRight1 = UIBarButtonItem(image: UIImage(named: "chat_callaudio"), style: .plain, target: self, action: #selector(self.actionCallAudio))
        let buttonRight2 = UIBarButtonItem(image: UIImage(named: "chat_callvideo"), style: .plain, target: self, action: #selector(self.actionCallVideo))

        if (type == Constant.Chat.CHAT_PRIVATE) {
            navigationItem.rightBarButtonItems = [buttonRight1, buttonRight2]
        }
        navigationController?.interactivePopGestureRecognizer?.delegate = self
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        collectionView.collectionViewLayout.springinessEnabled = false
    }
}
// MARK: - Custom menu actions for cells
extension ChatVC{
    override func didReceiveMenuWillShow(_ notification: Notification?) {
        let menu = notification?.object as? UIMenuController
        let menuItemCopy = UIMenuItem(title: "Copy", action: #selector(self.actionCopy(_:)))
        let menuItemDelete = UIMenuItem(title: "Delete", action: #selector(self.actionDelete(_:)))
        let menuItemSave = UIMenuItem(title: "Save", action: #selector(self.actionSave(_:)))
        menu?.menuItems = [menuItemCopy, menuItemDelete, menuItemSave]
    }
}
// MARK: - Realm Methods
extension ChatVC{
    func loadMessages() {
        automaticallyScrollsToMostRecentMessage = true
        
        let predicate = NSPredicate(format: "groupId == %@ AND isDeleted == NO", groupId)
        dbmessages = DBMessage.objects(with: predicate).sortedResults(usingKeyPath: FConst.FMessage.CREATEDAT, ascending: true) as? RLMResults<DBMessage>
        refreshCollectionView2()
    }
    
    func insertMessages() {
        insertCounter += Constant.Chat.INSERT_MESSAGES
        refreshCollectionView2()
    }
    @objc func actionCleanup() {
        NotificationCenter.default.removeObserver(self)
        messages?.actionCleanup()
        messages = nil
        firebase?.removeAllObservers()
        firebase = nil
    }
}

// MARK: - Refresh methods
extension ChatVC{
    @objc func refreshCollectionView1() {
        refreshCollectionView2()
        finishReceivingMessage()
    }
    
    @objc func refreshCollectionView2() {
        showLoadEarlierMessagesHeader = insertCounter < Int((dbmessages == nil) ? 0: dbmessages!.count)
        collectionView.reloadData()
    }
}

// MARK: - Backend methods (avatar)
extension ChatVC{
    func loadAvatar(_ userId: String) {
        if avatarIds.contains(userId) {
            return
        } else {
            avatarIds.append(userId)
        }
        var imagePath: String?
        let dbuser: DBUser? = Users.getObjectfromId(objectId: userId)
        
        if userId == CurrentUser.shared.objectId{
            imagePath = CurrentUser.shared.thumbnail
        }else{
            imagePath = dbuser?.thumbnail
        }
        
        DownloadManager.image(imagePath) { path, error, network in
            if error == nil {
                let image = UIImage(contentsOfFile: path ?? "")
                self.avatars[userId] = JSQMessagesAvatarImageFactory.avatarImage(with: image, diameter: 30)
                self.perform(#selector(self.delayedReload), with: nil, afterDelay: 0.1)
            } else if (error as NSError?)?.code != 100 {
                self.avatarIds.removeAll(where: { element in element == userId })
            }
        }
    }
    
    @objc func delayedReload() {
        collectionView.reloadData()
    }

}
// MARK: - Message sendig methods
extension ChatVC{
    func messageSend(_ text: String?, video: URL?, picture: UIImage?, audio: String?) {
        if Connection.isReachable() {
            let messageSend1 = MessageSend1(groupId, view: navigationController!.view)
            messageSend1.send(text, video: video, picture: picture, audio: audio)
        } else {
            
        }
        JSQSystemSoundPlayer.jsq_playMessageSentSound()
        finishSendingMessage()
    }
    func messageDelete(_ indexPath: IndexPath) {
        let dbmessage: DBMessage = self.dbmessage(indexPath)
        Message.deleteItem(dbmessage)
    }
}
// MARK: - Typing indicator methods
extension ChatVC {
    func createTypingObservers() {
        firebase?.observe(.childChanged) { (snapshot) in
            if (snapshot.key == FUser.currentId()) == false {
                let typing: Bool = snapshot.value as! Bool
                self.showTypingIndicator = typing
                if typing {
                    self.scrollToBottom(animated: true)
                }
            }
        }
    }
    
    func typingIndicatorStart() {
        typingCounter += 1
        typingIndicatorSave(true)
        perform(#selector(self.typingIndicatorStop), with: nil, afterDelay: 2.0)
    }
    
    @objc func typingIndicatorStop() {
        typingCounter -= 1
        if typingCounter == 0 {
            typingIndicatorSave(false)
        }
    }
    
    func typingIndicatorSave(_ typing: Bool) {
        var dictionary : [String : Any] = [:]
        dictionary[CurrentUser.shared.objectId] = typing
        firebase?.updateChildValues(dictionary)
    }
    
    override func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        typingIndicatorStart()
        return true
    }
}

// MARK: - JSQMessagesViewController method overrides
extension ChatVC{
    override func didPressSend(_ button: UIButton!, withMessageText text: String!, senderId: String!, senderDisplayName: String!, date: Date!) {
        messageSend(text, video: nil, picture: nil, audio: nil)
    }
    override func didPressAccessoryButton(_ sender: UIButton!) {
//        actionAttach()
    }
}

// MARK: - JSQMessages CollectionView DataSource
extension ChatVC{
    override func senderId() -> String? {
        return FUser.currentId()
    }
    
    override func senderDisplayName() -> String? {
        return CurrentUser.shared.fullname
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, messageDataForItemAt indexPath: IndexPath?) -> JSQMessageData? {
        let dbmessage: DBMessage = self.dbmessage(indexPath)
        let messageId = dbmessage.objectId
        
        if jsqmessages[messageId] == nil {
            let incoming = Incoming(dbmessage, collectionView: self.collectionView)
            jsqmessages[messageId] = incoming.createMessage()
        }
        return jsqmessages[messageId] as? JSQMessageData
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, messageBubbleImageDataForItemAt indexPath: IndexPath?) -> JSQMessageBubbleImageDataSource? {
        if outgoing(indexPath) {
            return bubbleImageOutgoing
        } else {
            return bubbleImageIncoming
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, avatarImageDataForItemAt indexPath: IndexPath?) -> JSQMessageAvatarImageDataSource? {
        let dbmessage: DBMessage = self.dbmessage(indexPath)
        let senderId = dbmessage.senderId
        
        if avatars[senderId] == nil {
            loadAvatar(senderId)
        }
        
        if initials[senderId] == nil {
            let avatar : JSQMessagesAvatarImage = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: dbmessage.senderInitials, backgroundColor: UIColor.init(rgb: 0xE4E4E4FF), textColor: UIColor.white, font: UIFont.systemFont(ofSize: 14), diameter: 30)
            initials[senderId] = avatar
        }
        return (avatars[senderId] != nil) ? avatars[senderId] as! JSQMessageAvatarImageDataSource : initials[senderId] as! JSQMessageAvatarImageDataSource
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForCellTopLabelAt indexPath: IndexPath?) -> NSAttributedString? {
        if (indexPath?.item ?? 0) % 3 == 0 {
            let jsqmessage: JSQMessage = self.jsqmessage(indexPath)
            return JSQMessagesTimestampFormatter.shared().attributedTimestamp(for: jsqmessage.date)
        } else {
            return nil
        }
    }
    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForMessageBubbleTopLabelAt indexPath: IndexPath?) -> NSAttributedString? {
        if incoming(indexPath) {
            let dbmessage: DBMessage? = self.dbmessage(indexPath)
            if (indexPath?.item ?? 0) > 0 {
                let dbprevious: DBMessage? = self.dbmessage(IndexPath(item: (indexPath?.item ?? 0) - 1, section: indexPath?.section ?? 0))
                if (dbprevious?.senderId == dbmessage?.senderId) {
                    return nil
                }
            }
            return NSAttributedString(string: dbmessage?.senderName ?? "")
        } else {
            return nil
        }
    }

    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, attributedTextForCellBottomLabelAt indexPath: IndexPath?) -> NSAttributedString? {
        if outgoing(indexPath) {
            let dbmessage: DBMessage? = self.dbmessage(indexPath)
            return NSAttributedString(string: dbmessage?.status ?? "")
        } else {
            return nil
        }
    }
}
// MARK: - UICollectionView DataSource & Delegate
extension ChatVC{
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return min(insertCounter, Int(dbmessages?.count ?? 0))
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let color = outgoing(indexPath) ? UIColor.black : UIColor.white
        
        let cell = super.collectionView(collectionView, cellForItemAt: indexPath) as! JSQMessagesCollectionViewCell
        cell.textView.textColor = color
        cell.textView.linkTextAttributes = [NSAttributedString.Key.foregroundColor: color]

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, shouldShowMenuForItemAt indexPath: IndexPath) -> Bool {
        super.collectionView(collectionView, shouldShowMenuForItemAt: indexPath)
        return true
    }
    
    override func collectionView(_ collectionView: UICollectionView, canPerformAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) -> Bool {
        let dbmessage: DBMessage? = self.dbmessage(indexPath)
        
        if action == #selector(self.actionCopy(_:)) {
            if (dbmessage?.type == Constant.Chat.MESSAGE_TEXT) {
                return true
            }
            if (dbmessage?.type == Constant.Chat.MESSAGE_EMOJI) {
                return true
            }
        }
        
        if action == #selector(self.actionDelete(_:)) {
            if outgoing(indexPath) {
                return true
            }
        }
        
        if action == #selector(self.actionSave(_:)) {
            if (dbmessage?.type == Constant.Chat.MESSAGE_PICTURE) {
                return true
            }
            if (dbmessage?.type == Constant.Chat.MESSAGE_VIDEO) {
                return true
            }
            if (dbmessage?.type == Constant.Chat.MESSAGE_AUDIO) {
                return true
            }
        }
        return false
    }
    
    override func collectionView(_ collectionView: UICollectionView, performAction action: Selector, forItemAt indexPath: IndexPath, withSender sender: Any?) {
        if action == #selector(self.actionCopy(_:)) {
            actionCopy(indexPath)
        }
        if action == #selector(self.actionDelete(_:)) {
            actionDelete(indexPath)
        }
        if action == #selector(self.actionSave(_:)) {
            actionSave(indexPath)
        }
    }

}
// MARK: - JSQMessages collection view flow layout delegate
extension ChatVC{
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout?, heightForCellTopLabelAt indexPath: IndexPath?) -> CGFloat {
        if (indexPath?.item ?? 0) % 3 == 0 {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        } else {
            return 0
        }
    }

    
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout?, heightForMessageBubbleTopLabelAt indexPath: IndexPath?) -> CGFloat {
        if incoming(indexPath) {
            let dbmessage: DBMessage? = self.dbmessage(indexPath)
            if (indexPath?.item ?? 0) > 0 {
                let dbprevious: DBMessage? = self.dbmessage(IndexPath(item: (indexPath?.item ?? 0) - 1, section: indexPath?.section ?? 0))
                if (dbprevious?.senderId == dbmessage?.senderId) {
                    return 0
                }
            }
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        } else {
            return 0
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, layout collectionViewLayout: JSQMessagesCollectionViewFlowLayout?, heightForCellBottomLabelAt indexPath: IndexPath?) -> CGFloat {
        if outgoing(indexPath) {
            return kJSQMessagesCollectionViewCellLabelHeightDefault
        } else {
            return 0
        }
    }
}
// MARK: - Responding to collection view tap events
extension ChatVC{
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, header headerView: JSQMessagesLoadEarlierHeaderView?, didTapLoadEarlierMessagesButton sender: UIButton?) {
        insertMessages()
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, didTapAvatarImageView avatarImageView: UIImageView?, at indexPath: IndexPath?) {
        let dbmessage: DBMessage? = self.dbmessage(indexPath)
        let userId = dbmessage?.senderId
        
        if (userId == FUser.currentId()) == false {
//            let profileView = ProfileView(userId, chat: false)
//            navigationController?.pushViewController(profileView, animated: true)
        }
    }
    
    override func collectionView(_ collectionView: JSQMessagesCollectionView?, didTapMessageBubbleAt indexPath: IndexPath?) {
        let dbmessage: DBMessage = self.dbmessage(indexPath)
        let jsqmessage: JSQMessage = self.jsqmessage(indexPath)
        
        if (dbmessage.type == Constant.Chat.MESSAGE_PICTURE) {
            let mediaItem = jsqmessage.media as! PhotoMediaItem
            if mediaItem.status == Constant.Chat.STATUS_MANUAL {
                MediaManager.loadPictureManual(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
                collectionView?.reloadData()
            }
            if mediaItem.status == Constant.Chat.STATUS_SUCCEED {
                let pictureView = PictureView(mediaItem.image)
                present(pictureView!, animated: true)
            }
        }

        if (dbmessage.type == Constant.Chat.MESSAGE_VIDEO) {
            let mediaItem = jsqmessage.media as! VideoMediaItem
            if mediaItem.status == Constant.Chat.STATUS_MANUAL {
                MediaManager.loadVideoManual(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
                collectionView?.reloadData()
            }
            if mediaItem.status == Constant.Chat.STATUS_SUCCEED {
                let videoView = VideoView(mediaItem.fileURL)
                present(videoView!, animated: true)
            }
        }

        if (dbmessage.type == Constant.Chat.MESSAGE_AUDIO) {
            let mediaItem = jsqmessage.media as! AudioMediaItem
            if mediaItem.status == Constant.Chat.STATUS_MANUAL {
                MediaManager.loadAudioManual(mediaItem, dbmessage: dbmessage, collectionView: collectionView)
                collectionView?.reloadData()
            }
        }

        if (dbmessage.type == Constant.Chat.MESSAGE_LOCATION) {
            let mediaItem = jsqmessage.media as! JSQLocationMediaItem
            let mapView = MapView(mediaItem.location)
            let navController = NavigationController(rootViewController: mapView!)
            present(navController, animated: true)
        }

    }

    override func collectionView(_ collectionView: JSQMessagesCollectionView?, didTapCellAt indexPath: IndexPath?, touchLocation: CGPoint) {
    }

}

// MARK: - User actions
extension ChatVC{
    @objc func actionBack() {
        for index in 0..<Int(dbmessages!.count){
            let dbmessage: DBMessage = dbmessages![UInt(index)]
            if (dbmessage.type == Constant.Chat.MESSAGE_AUDIO) {
                let jsqmessage: JSQMessage = jsqmessages[dbmessage.objectId] as! JSQMessage
                let mediaItem = jsqmessage.media as? AudioMediaItem
                mediaItem?.stopAudioPlayer()
            }
        }
        
        actionCleanup()
        Recent.clearCounter(groupId)
//        dismissWithPushAnimation()
//        navigationController?.popViewController(animated: true)
        navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func actionDetails(_ sender: Any) {
//        if ([type isEqualToString:CHAT_PRIVATE])
//        {
//            NSString *userId = [self otherUserId];
//            ProfileView *profileView = [[ProfileView alloc] initWith:userId Chat:NO];
//            [self.navigationController pushViewController:profileView animated:YES];
//        }
//
//        if ([type isEqualToString:CHAT_MULTIPLE])
//        {
//            MultipleView *multipleView = [[MultipleView alloc] initWith:groupId members:members];
//            [self.navigationController pushViewController:multipleView animated:YES];
//        }
//
//        if ([type isEqualToString:CHAT_GROUP])
//        {
//            GroupView *groupView = [[GroupView alloc] initWith:groupId];
//            [self.navigationController pushViewController:groupView animated:YES];
//        }
    }
    
    @objc func actionCallAudio() {
//        ActionPremium()
    }
    
    @objc func actionCallVideo() {
//        ActionPremium()
    }
    
    @objc func actionDelete(_ indexPath: IndexPath?) {
//        messageDelete(indexPath)
    }
    
    @objc func actionCopy(_ indexPath: IndexPath?) {
//        DBMessage *dbmessage = [self dbmessage:indexPath];
//        [[UIPasteboard generalPasteboard] setString:dbmessage.text];
    }
    
    @objc func actionSave(_ indexPath: IndexPath?) {
//        DBMessage *dbmessage = [self dbmessage:indexPath];
//        JSQMessage *jsqmessage = [self jsqmessage:indexPath];
//
//        if ([dbmessage.type isEqualToString:MESSAGE_PICTURE])
//        {
//            PhotoMediaItem *mediaItem = (PhotoMediaItem *)jsqmessage.media;
//            if (mediaItem.status == STATUS_SUCCEED)
//            UIImageWriteToSavedPhotosAlbum(mediaItem.image, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
//        }
//
//        if ([dbmessage.type isEqualToString:MESSAGE_VIDEO])
//        {
//            VideoMediaItem *mediaItem = (VideoMediaItem *)jsqmessage.media;
//            if (mediaItem.status == STATUS_SUCCEED)
//            UISaveVideoAtPathToSavedPhotosAlbum(mediaItem.fileURL.path, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
//        }
//
//        if ([dbmessage.type isEqualToString:MESSAGE_AUDIO])
//        {
//            AudioMediaItem *mediaItem = (AudioMediaItem *)jsqmessage.media;
//            if (mediaItem.status == STATUS_SUCCEED)
//            {
//                NSString *path = [File temp:@"mp4"];
//                [mediaItem.audioData writeToFile:path atomically:NO];
//                UISaveVideoAtPathToSavedPhotosAlbum(path, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
//            }
//        }
    }
    
    func video(_ videoPath: String?, didFinishSavingWithError error: Error?, contextInfo: UnsafeMutableRawPointer?) {
//        if (error == nil)
//        {
//            [ProgressHUD showSuccess:@"Successfully saved."];
//        }
//        else [ProgressHUD showError:@"Save failed."];
    }
    
    func actionAttach() {
//        [self.view endEditing:YES];
//        NSArray *menuItems = @[[[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat_camera"] title:@"Camera"],
//        [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat_audio"] title:@"Audio"],
//        [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat_picture"] title:@"Picture"],
//        [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat_video"] title:@"Video"],
//        [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat_location"] title:@"Location"],
//        [[RNGridMenuItem alloc] initWithImage:[UIImage imageNamed:@"chat_sticker"] title:@"Sticker"]];
//        RNGridMenu *gridMenu = [[RNGridMenu alloc] initWithItems:menuItems];
//        gridMenu.delegate = self;
//        [gridMenu showInViewController:self.navigationController center:CGPointMake(self.view.bounds.size.width/2, self.view.bounds.size.height/2)];
    }
    
    func actionStickers() {
//        StickersView *stickersView = [[StickersView alloc] init];
//        stickersView.delegate = self;
//        NavigationController *navController = [[NavigationController alloc] initWithRootViewController:stickersView];
//        [self presentViewController:navController animated:YES completion:nil];
    }
}

// MARK: - Helper methods
extension ChatVC{
    
    func index(_ indexPath: IndexPath?) -> Int {
        let count = min(insertCounter, Int(dbmessages!.count))
        let offset: Int = Int(dbmessages!.count) - count
        return (indexPath?.item ?? 0) + offset
    }
    
    func dbmessage(_ indexPath: IndexPath?) -> DBMessage {
        let index: Int = self.index(indexPath)
        return dbmessages![UInt(index)]
    }
    
    func jsqmessage(_ indexPath: IndexPath?) -> JSQMessage {
        let dbmessage: DBMessage = self.dbmessage(indexPath)
        return jsqmessages[dbmessage.objectId] as! JSQMessage
    }
    
    func incoming(_ indexPath: IndexPath?) -> Bool {
        let dbmessage: DBMessage? = self.dbmessage(indexPath)
        return (dbmessage?.senderId == FUser.currentId()) == false
    }
    
    func outgoing(_ indexPath: IndexPath?) -> Bool {
        let dbmessage: DBMessage? = self.dbmessage(indexPath)
        return (dbmessage?.senderId == FUser.currentId()) == true
    }
}
