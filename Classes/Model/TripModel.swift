//
//  TripModel.swift
//  Tamuro
//
//  Created by Mobile on 9/12/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class TripModel: NSObject {
    var dateStartTrip: Date?
    var descriptionTrip: String?
    var title:String?
    var days: Int = 0
    var night: Int = 0
    var imgTrip: UIImage?
}
