//
//  SelectMultipleView.swift
//  Tamuro
//
//  Created by Mobile on 11/29/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit
import ProgressHUD
import Toast

protocol SelectMultipleDelegate {
    func didSelectMultipleUsers(selects: [String]);
}
class SelectMultipleView: UIViewController {
    
    var delegate: SelectMultipleDelegate? = nil
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var userIds = [String]()
    var selection = [String]()
    var sections: [[DBUser]] = [[]]
    var dbusers: RLMResults<DBUser>?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Select Multiple";
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(actionCancel))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(actionDone))
        
        tableView.tableFooterView = UIView()
        
        loadUsers()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        dismissKeyboard()
    }
    func dismissKeyboard() {
        view.endEditing(true)
    }
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        dismissKeyboard()
    }

    
    @objc func actionCancel(){
        navigationController?.dismiss(animated: true, completion: nil)
    }
    @objc func actionDone(){
        if selection.count == 0 {
            ProgressHUD.showError("Please select some users.")
            return
        }
        
        dismiss(animated: true) {
            if self.delegate != nil {
                self.delegate!.didSelectMultipleUsers(selects: self.selection)
            }
        }

    }
    func loadUsers(){
        let text: String = searchBar.text!
       
        if text.count>0 {
            let predicate = NSPredicate(format: "fullname CONTAINS[c] %@ AND objectId !IN %@", text,userIds)
            dbusers = DBUser.objects(with: predicate).sortedResults(usingKeyPath: FConst.FUser.FULLNAME, ascending: true) as? RLMResults<DBUser>
        }else{
            dbusers = DBUser.allObjects().sortedResults(usingKeyPath: FConst.FUser.FULLNAME, ascending: true) as? RLMResults<DBUser>
        }
        refreshTableView()
    }
    
    func refreshTableView(){
        setObjects()
        tableView.reloadData()
    }
    
    func setObjects(){
        if sections.count > 0{
            sections = [[]]
        }
        let sectionTitlesCount = UILocalizedIndexedCollation.current().sectionIndexTitles.count
        for _ in 1..<sectionTitlesCount{
            sections.append([DBUser]())
        }
        for i in 0..<Int(dbusers?.count ?? 0){
            let dbuser = dbusers![UInt(i)]
            let section = UILocalizedIndexedCollation.current().section(for: dbuser, collationStringSelector: #selector(getter: dbuser.fullname))
            sections[section].append(dbuser)
        }
    }
}
extension SelectMultipleView: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if sections[section].count != 0 {
            return UILocalizedIndexedCollation.current().sectionTitles[section]
        } else {
            return nil
        }
    }
    
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        return UILocalizedIndexedCollation.current().sectionIndexTitles
    }
    
    
    func tableView(_ tableView: UITableView, sectionForSectionIndexTitle title: String, at index: Int) -> Int {
        return UILocalizedIndexedCollation.current().section(forSectionIndexTitle: index)
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "cell")
        if cell == nil {
            cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        }
        
        let dbusers_section = sections[indexPath.section]
        let dbuser = dbusers_section[indexPath.row]
        
        cell?.textLabel?.text = dbuser.fullname
        cell?.accessoryType = selection.contains(dbuser.objectId) ? .checkmark : .none
        
        return cell!
    }
    
    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let dbusers_section = sections[indexPath.section]
        let dbuser = dbusers_section[indexPath.row]
   
        if userIds.contains(dbuser.objectId) {
            view.makeToast("You have added this user already!")
            return
        }
        if selection.contains(dbuser.objectId) {
            selection.removeAll(where: { element in element == dbuser.objectId })
        } else {
            selection.append(dbuser.objectId)
        }
        
        let cell: UITableViewCell? = tableView.cellForRow(at: indexPath)
        cell?.accessoryType = selection.contains(dbuser.objectId) ? .checkmark : .none
    }
}

extension SelectMultipleView: UISearchBarDelegate{
    // MARK: - UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        loadUsers()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar_: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBarTextDidEndEditing(_ searchBar_: UISearchBar) {
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar_: UISearchBar) {
        searchBar.text = ""
        searchBar.resignFirstResponder()
        loadUsers()
    }
    
    func searchBarSearchButtonClicked(_ searchBar_: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}
