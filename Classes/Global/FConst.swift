//
//  AppFConstant.swift
//  WeddingProject
//
//  Created by Mobile on 8/29/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit

struct FConst{
    //MARK: - FUser
    static let FIREBASE_STORAGE     = "gs://tamuro-2397a.appspot.com"
    static let USERPIC_STORAGE      = "UserProfilePhoto"
    static let TRIP_STORAGE         = "Trip"
    struct FUser
    {
        static let PATH             = "User"                    //    Path name
        static let OBJECTID         = "objectId"                //    String
        static let FIRSTNAME        = "firstname"               //    String
        static let LASTNAME         = "lastname"                //    String
        static let FULLNAME         = "fullname"                //    String
        static let PICTURE          = "picture"                 //    String
        static let THUMBNAIL        = "thumbnail"               //    String
        static let ONESIGNALID      = "oneSignalId"             //    String
        static let EMAIL            = "email"                   //    String
        static let PHONE            = "phone"                   //    String
        static let TRIPS            = "trips"                   //    String
        static let SAVEDTRIPS       = "savedtrips"              //    Array
        static let CREATEDAT        = "createdAt"               //    String
        static let UPDATEDAT        = "updatedAt"               //    String
    }
    
    //MARK: - FMESSAGE
    struct FMessage
    {
        static let PATH                           = "Message"              //    Path name
        static let OBJECTID                       = "objectId"             //    String
        
        static let GROUPID                        = "groupId"              //    String
        static let SENDERID                       = "senderId"             //    String
        static let SENDERNAME                     = "senderName"           //    String
        static let SENDERINITIALS                 = "senderInitials"       //    String
        
        static let TYPE                           = "type"                 //    String
        static let TEXT                           = "text"                 //    String
        
        static let PICTURE                        = "picture"              //    String
        static let PICTURE_WIDTH                  = "picture_width"        //    Number
        static let PICTURE_HEIGHT                 = "picture_height"       //    Number
        static let PICTURE_MD5                    = "picture_md5"          //    String
        
        static let VIDEO                          = "video"                //    String
        static let VIDEO_DURATION                 = "video_duration"       //    Number
        static let VIDEO_MD5                      = "video_md5"            //    String
        
        static let AUDIO                          = "audio"                //    String
        static let AUDIO_DURATION                 = "audio_duration"       //    Number
        static let AUDIO_MD5                      = "audio_md5"            //    String
        
        static let LATITUDE                       = "latitude"             //    Number
        static let LONGITUDE                      = "longitude"            //    Number
        
        static let STATUS                         = "status"               //    String
        static let ISDELETED                      = "isDeleted"            //    Boolean
        
        static let CREATEDAT                      = "createdAt"            //    Timestamp
        static let UPDATEDAT                      = "updatedAt"            //    Timestamp
    }
    
    //MARK: - FRECENT
    struct FRecent
    {
        static let PATH                            = "Recent"               //    Path name
        static let OBJECTID                        = "objectId"             //    String
        
        static let USERID                          = "userId"               //    String
        static let GROUPID                         = "groupId"              //    String
        
        static let INITIALS                        = "initials"             //    String
        static let PICTURE                         = "picture"              //    String
        static let DESCRIPTION                     = "desc"                 //    String
        static let MEMBERS                         = "members"              //    Array
        static let PASSWORD                        = "password"             //    String
        static let TYPE                            = "type"                 //    String
        
        static let COUNTER                         = "counter"              //    Number
        static let LASTMESSAGE                     = "lastMessage"          //    String
        static let LASTMESSAGEDATE                 = "lastMessageDate"      //    Timestamp
        
        static let ISARCHIVED                      = "isArchived"           //    Boolean
        static let ISDELETED                       = "isDeleted"            //    Boolean
        
        static let CREATEDAT                       = "createdAt"            //    Timestamp
        static let UPDATEDAT                       = "updatedAt"            //    Timestamp
    }
    //MARK: - FGROUP
//    struct FGroup {
//        static let PATH                            = "Group"                //    Path name
//        static let OBJECTID                        = "objectId"             //    String
//
//        static let USERID                          = "userId"               //    String
//        static let NAME                            = "name"                 //    String
//        static let PICTURE                         = "picture"              //    String
//        static let MEMBERS                         = "members"              //    Array
//
//        static let ISDELETED                       = "isDeleted"            //    Boolean
//
//        static let CREATEDAT                       = "createdAt"            //    Timestamp
//        static let UPDATEDAT                       = "updatedAt"            //    Timestamp
//    }
    
    static let FTYPING_PATH                        = "Typing"               //    Path name
    
    struct FTrip {
        static let PATH                             = "Trip"                //    Path name
        static let OBJECTID                         = "objectId"            //    String
        static let TRIPID                           = "tripId"              //    String
        static let TITLE                            = "title"               //    String
        static let ABOUT                            = "about"               //    String
        static let PICTURE                          = "picture"             //    String
        static let USERID                           = "userId"              //    String
        
        static let COST                             = "cost"                //    int
        static let RAISED                           = "raised"              //    int
        
        static let TRIPCONTENTS                     = "tripContents"        //    Array
        static let MEMBERS                          = "members"             //    Array
        
        static let STARTDATE                        = "startDate"           //    String
        static let MAXMEMBERNUM                     = "maxMemberNum"        //    int
        static let DAY                              = "day"                 //    int
        static let NIGHT                            = "night"               //    int
        static let POPULARITY                       = "popularity"          //    int
        
        static let ISPRIVATE                        = "isPrivate"           //    Boolean
        static let ISPAID                           = "isPaid"              //    Boolean
        static let ISDELETED                        = "isDeleted"           //    Boolean
        static let ISACHIEVED                       = "isAchieved"          //    Boolean
        
        static let ADDRESS                          = "address"             //    String
        static let LATITUDE                         = "latitude"            //    double
        static let LONGITUDE                        = "longitude"           //    double
        
        static let FOLLOWERS                        = "followers"           //    Array
               
        static let PAYMENTTYPE                      = "paymentType"         //    String
        
        static let STARTDATESTAMP                   = "startDateStamp"      //    Timestamp
        static let ENDDATESTAMP                     = "endDateStamp"        //    Timestamp
        static let CREATEDAT                        = "createdAt"           //    Timestamp
        static let UPDATEDAT                        = "updatedAt"           //    Timestamp
    }
    //MARK: - FGROUP
    struct FTripContent {
        static let PATH                            = "TripContent"          //    Path name
        static let OBJECTID                        = "objectId"             //    String
        static let NAME                            = "name"                 //    String
        static let PICTURE                         = "picture"              //    String
        
        static let ISDELETED                       = "isDeleted"            //    Boolean
        
        static let CREATEDAT                       = "createdAt"            //    Timestamp
        static let UPDATEDAT                       = "updatedAt"            //    Timestamp
    }
}

