//
//  Extensions.swift
//  Tamuro
//
//  Created by Mobile on 9/11/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

extension UIView {
    func dropShadowEffect(shadowColor: UIColor = .black, shadowOffset : CGSize = CGSize(width: 0, height: 2), shadowRadius: CGFloat = 6, shadowOpacity: Float = 0.15) {
        layer.shadowOffset = shadowOffset
        layer.shadowColor = shadowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.cornerRadius = shadowRadius
        layer.shouldRasterize = true
    }
    func dropShadowEffectOnly(shadowColor: UIColor = .black, shadowOffset : CGSize = CGSize(width: 0, height: 2), shadowRadius: CGFloat = 6, shadowOpacity: Float = 0.15) {
        layer.shadowOffset = shadowOffset
        layer.shadowColor = shadowColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shouldRasterize = true
    }
    func setRadius(radius: CGFloat = 6) {
        layer.masksToBounds = true
        layer.cornerRadius = radius
    }
    func setBorder(width: CGFloat = 1, color: UIColor = .black) {
        layer.masksToBounds = true
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
}
extension UIViewController {
    func showErrorAlert(title: String? = nil, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
extension Notification.Name {
    public struct CustomKeys {
        public static let needToCloseTripPage = Notification.Name("needToCloseTripPage")
        public static let needToRotateTrip = Notification.Name("needToRotateTrip")
        public static let CLEANUP_CHATVIEW = Notification.Name("NotificationCleanupChatView")
        public static let REFRESH_MESSAGES1 = Notification.Name("NotificationRefreshMessages1")
        public static let REFRESH_MESSAGES2 = Notification.Name("NotificationRefreshMessages2")
    }
}
extension UITextView {
    func updateSpacing(spacing: CGFloat) {
        let attrStr = NSMutableAttributedString(string: text)
        let paragraph = NSMutableParagraphStyle()
        paragraph.lineSpacing = spacing
        attrStr.addAttribute(.paragraphStyle, value: paragraph, range: NSMakeRange(0, text.count))
        attrStr.addAttribute(.font, value: font!, range: NSMakeRange(0, text.count))
        attrStr.addAttribute(.foregroundColor, value: textColor!, range: NSMakeRange(0, text.count))
        attributedText = attrStr
    }
}

extension String {
    var words: [String] {
        var words: [String] = []
        enumerateSubstrings(in: startIndex..<endIndex, options: .byWords) { (word, _, _, _) in
            guard let word = word else {return}
            words.append(word)
        }
        return words
    }
}
extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}
extension UIViewController {
    func presentWithPushAnimation(destinationVC: UIViewController, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromRight
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        present(destinationVC, animated: false, completion: completion)
    }
    func dismissWithPushAnimation(completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.3
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromLeft
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        view.window?.layer.add(transition, forKey: kCATransition)
        dismiss(animated: false, completion: completion)
    }
}
extension Date {
    func timestamp() -> Int64 {
        return Int64(timeIntervalSince1970 * 1000)
    }
    
    static func dateWithTimestamp(timestamp: Int64) -> Date {
        let interval = TimeInterval(TimeInterval(timestamp) / 1000)
        return Date(timeIntervalSince1970: interval)
    }

    static func dateWithNumberTimestamp(number: Int64) -> Date {
        return dateWithTimestamp(timestamp: number)
    }
}

@IBDesignable
class DesignableView: UIView {
}

@IBDesignable
class DesignableButton: UIButton {
}

@IBDesignable
class DesignableLabel: UILabel {
}

extension UIView {
    
    @IBInspectable
    var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable
    var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable
    var borderColor: UIColor? {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.borderColor = color.cgColor
            } else {
                layer.borderColor = nil
            }
        }
    }
    
    @IBInspectable
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
    
    @IBInspectable
    var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable
    var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable
    var shadowColor: UIColor? {
        get {
            if let color = layer.shadowColor {
                return UIColor(cgColor: color)
            }
            return nil
        }
        set {
            if let color = newValue {
                layer.shadowColor = color.cgColor
            } else {
                layer.shadowColor = nil
            }
        }
    }
}
extension UITableView{
    


}
