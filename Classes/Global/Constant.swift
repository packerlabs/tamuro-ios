//
//  Constant.swift
//  WeddingProject
//
//  Created by Mobile on 8/31/18.
//  Copyright © 2018 clipbox. All rights reserved.
//

import UIKit

class Constant: NSObject {
    
    static let COLOR_OUTGOING           = UIColor.init(red: 230/255.0, green: 230/255.0, blue: 230/255.0, alpha: 1.0)
    static let COLOR_INCOMING           = UIColor.init(red: 20/255.0, green: 118/255.0, blue: 209/255.0, alpha: 1.0)
    static let LINK_PREMIUM             = "http://www.relatedcode.com/premium"
    
    static let SCREEN_WIDTH             = UIScreen.main.bounds.size.width
    static let SCREEN_HEIGHT            = UIScreen.main.bounds.size.height
    
    static let TEXT_INVITE_SMS          = "Check out PremiumChat for your smartphone. Download it today."
    
    
    static let ONESIGNALID              = "OneSignalId"
    static let USER_ACCOUNTS            = "UserAccounts"
    static let REACHABILITY_CHANGED     = "ReachabilityChanged"
    struct AppKey {
        static let ONESIGNAL = "36e5d83a-01be-4152-9f89-7ac4b6240ca0"
    }
    struct NotifiKey{
        static let APP_STARTED              = "NotificationAppStarted"
        static let USER_LOGGED_IN           = "NotificationUserLoggedIn"
        static let USER_LOGGED_OUT          = "NotificationUserLoggedOut"
        static let CHANGE_SHOOTCODE         = "NotificationChangeShootCode"
        static let MEONLINE                 = "meonline"
        static let MEOFFLINE                = "meoffline"
        static let APP_EXIT                 = "appexit"
        static let APP_TIMERESET            = "ApptimeReset"
        static let REFRESH_CALLHISTORIES    = "NotificationRefreshCallHistories"
        static let REFRESH_CONTACTS         = "NotificationRefreshContacts"
        static let REFRESH_GROUPS           = "NotificationRefreshGroups"
        static let REFRESH_MESSAGES1        = "NotificationRefreshMessages1"
        static let REFRESH_MESSAGES2        = "NotificationRefreshMessages2"
        static let REFRESH_RECENTS          = "NotificationRefreshRecents"
        static let REFRESH_MEDIAS           = "NotificationRefreshMedias"
        static let REFRESH_USERS            = "NotificationRefreshUsers"
        static let REFRESH_TRIPS            = "NotificationRefreshTrips"
        static let REFRESH_SHOOTCODES       = "NotificationRefreshShootCodes"
        static let UPDATE_USER_STATUS       = "updateuserstatus"
    }
    struct Chat {
        static let DEFAULT_TAB              = 0
        static let VIDEO_LENGTH             = 5
        static let AUDIO_LENGTH             = 5
        static let INSERT_MESSAGES          = 10
        static let PASSWORD_LENGTH          = 20
        static let DOWNLOAD_TIMEOUT         = 300
        
        static let STATUS_LOADING           = 1
        static let STATUS_SUCCEED           = 2
        static let STATUS_MANUAL            = 3
        
        static let MEDIA_IMAGE              = 1
        static let MEDIA_VIDEO              = 2
        static let MEDIA_AUDIO              = 3
        
        static let NETWORK_MANUAL           = 1
        static let NETWORK_WIFI             = 2
        static let NETWORK_ALL              = 3
        
        static let KEEPMEDIA_WEEK           = 1
        static let KEEPMEDIA_MONTH          = 2
        static let KEEPMEDIA_FOREVER        = 3
        
        static let DEL_ACCOUNT_NONE         = 1
        static let DEL_ACCOUNT_ONE          = 2
        static let DEL_ACCOUNT_ALL          = 3
        
        static let CHAT_PRIVATE             = "private"
        static let CHAT_MULTIPLE            = "multiple"
        static let CHAT_GROUP               = "group"
        
        static let CALLHISTORY_AUDIO        = "audio"
        static let CALLHISTORY_VIDEO        = "video"
        
        static let MESSAGE_TEXT             = "text"
        static let MESSAGE_EMOJI            = "emoji"
        static let MESSAGE_PICTURE          = "picture"
        static let MESSAGE_VIDEO            = "video"
        static let MESSAGE_AUDIO            = "audio"
        static let MESSAGE_LOCATION         = "location"
        
        static let LOGIN_EMAIL              = "Email"
        static let LOGIN_FACEBOOK           = "Facebook"
        static let LOGIN_GOOGLE             = "Google"
        
        
        
        static let TEXT_QUEUED              = "Queued"
        static let TEXT_SENT                = "Sent"
        static let TEXT_READ                = "Read"
        
        static let PHOTOS_ALBUM_TITLE       = "Chat"
    }
}
