//
//  Global.swift
//  Tamuro
//
//  Created by Mobile on 9/6/18.
//  Copyright © 2018 Mobile. All rights reserved.
//

import UIKit

class Global: NSObject {
    
    static let shared = Global()
    var mainVC : UIViewController?
    static var newTrip : FObject?
    static func validEmail(email: String) -> Bool{
        let array = email.components(separatedBy: "@")
        if array.count == 2 {
            let array = array[1] .components(separatedBy: ".")
            if array.count > 1 {
                return true
            }
            return false
        }
        return false
    }
    
    static func filename(folderName:String,type:String?,ext:String) ->String
    {
        let timestamp = NSDate().timestamp()
        if type == nil {
            return folderName + "/" + "\(timestamp)" + "." + ext
        }
        return folderName + "/" + CurrentUser.shared.objectId + "/" + type! + "/" + "\(timestamp)" + "." + ext
    }
    static func getNavigationView(selector: Selector) -> (UIView, UILabel) {
        let viewRightItem = UIView(frame: CGRect(x: 0, y: 0, width: 76, height: 44))
        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0, width: 68, height: 44))
        lblTitle.text = "Ohio, US"
        lblTitle.font = UIFont(name: "Avenir-Heavy", size: 14)
        lblTitle.textColor = UIColor(red: 1.0, green: 41.0 / 255.0, blue: 103.0 / 255.0, alpha: 1.0)
        viewRightItem.addSubview(lblTitle)
        let imgArrow = UIImageView(frame: CGRect(x: 61, y: 0, width: 7, height: 44))
        imgArrow.image = #imageLiteral(resourceName: "bottom_nav")
        imgArrow.contentMode = .scaleAspectFit
        viewRightItem.addSubview(imgArrow)
        let btnRight = UIButton(frame: viewRightItem.bounds)
        btnRight.addTarget(self, action: selector, for: .touchUpInside)
        viewRightItem.addSubview(btnRight)
        return (viewRightItem, lblTitle)
    }
}
/// Runs the closure in the UI thread asynchronously
func UI(_ f: @escaping () -> Void) {
    DispatchQueue.main.async(execute: f)
}
/// Runs the closure in the UI thread synchronously
func UISync(_ f: @escaping () -> Void) {
    DispatchQueue.main.sync(execute: f)
}
public enum BrowseBy: String {
    case endDate = "End Date"
    case popularity = "Popularity"
    case newest = "Newest"
    case mostFunded = "Most Funded"
    case recommended = "Recommended"
}
